package mra.http;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class HttpRequest {

	private static final String HTTP_VERSION = "HTTP/1.1";
	public static final int DEFAULT_BUFFER_SIZE = 1024 * 1024 * 1;
	public static final int RESPONSE_CODE_CONTINUE = 100;
	public static final int RESPONSE_CODE_SWITCHING_PROTOCOLS = 101;
	public static final int RESPONSE_CODE_PROCESSING = 102;
	public static final int RESPONSE_CODE_OK = 200;
	public static final int RESPONSE_CODE_CREATED = 201;
	public static final int RESPONSE_CODE_ACCEPTED = 202;
	public static final int RESPONSE_CODE_NON_AUTHORITATIVE_INFORMATION = 203;
	public static final int RESPONSE_CODE_NO_CONTENT = 204;
	public static final int RESPONSE_CODE_RESET_CONTENT = 205;
	public static final int RESPONSE_CODE_PARTIAL_CONTENT = 206;
	public static final int RESPONSE_CODE_MULTI_STATUS = 207;
	public static final int RESPONSE_CODE_MULTIPLE_CHOICES = 300;
	public static final int RESPONSE_CODE_MOVED_PERMANENTLY = 301;
	public static final int RESPONSE_CODE_FOUND = 302;
	public static final int RESPONSE_CODE_SEE_OTHER = 303;
	public static final int RESPONSE_CODE_NOT_MODIFIED = 304;
	public static final int RESPONSE_CODE_USE_PROXY = 305;
	public static final int RESPONSE_CODE_SWITCH_PROXY = 306;
	public static final int RESPONSE_CODE_TEMPORARY_REDIRECT = 307;
	public static final int RESPONSE_CODE_BAD_REQUEST = 400;
	public static final int RESPONSE_CODE_UNAUTHORIZED = 401;
	public static final int RESPONSE_CODE_PAYMENT_REQUIRED = 402;
	public static final int RESPONSE_CODE_FORBIDDEN_ = 403;
	public static final int RESPONSE_CODE_NOT_FOUND = 404;
	public static final int RESPONSE_CODE_METHOD_NOT_ALLOWED = 405;
	public static final int RESPONSE_CODE_NOT_ACCEPTABLE = 406;
	public static final int RESPONSE_CODE_PROXY_AUTHENTICATION_REQUIRED = 407;
	public static final int RESPONSE_CODE_REQUEST_TIMEOUT = 408;
	public static final int RESPONSE_CODE_CONFLICT = 409;
	public static final int RESPONSE_CODE_GONE = 410;
	public static final int RESPONSE_CODE_LENGTH_REQUIRED = 411;
	public static final int RESPONSE_CODE_PRECONDITION_FAILED = 412;
	public static final int RESPONSE_CODE_REQUEST_ENTITY_TOO_LARGE = 413;
	public static final int RESPONSE_CODE_REQUEST_URI_TOO_LONG = 414;
	public static final int RESPONSE_CODE_UNSUPPORTED_MEDIA_TYPE = 415;
	public static final int RESPONSE_CODE_REQUESTED_RANGE_NOT_SATISFIABLE = 416;
	public static final int RESPONSE_CODE_I_M_A_TEAPOT = 417;
	public static final int RESPONSE_CODE_THERE_ARE_TOO_MANY_CONNECTIONS_FROM_YOUR_INTERNET_ADDRESS = 421;
	public static final int RESPONSE_CODE_UNPROCESSABLE_ENTITY = 422;
	public static final int RESPONSE_CODE_LOCKED_ = 423;
	public static final int RESPONSE_CODE_FAILED_DEPENDENCY = 424;
	public static final int RESPONSE_CODE_UNORDERED_COLLECTION = 425;
	public static final int RESPONSE_CODE_UPGRADE_REQUIRED = 426;
	public static final int RESPONSE_CODE_RETRY_WITH = 449;
	public static final int RESPONSE_CODE_UNAVAILABLE_FOR_LEGAL_REASONS = 451;
	public static final int RESPONSE_CODE_INTERNAL_SERVER_ERROR = 500;
	public static final int RESPONSE_CODE_NOT_IMPLEMENTED = 501;
	public static final int RESPONSE_CODE_BAD_GATEWAY = 502;
	public static final int RESPONSE_CODE_SERVICE_UNAVAILABLE_ = 503;
	public static final int RESPONSE_CODE_GATEWAY_TIMEOUT = 504;
	public static final int RESPONSE_CODE_HTTP_VERSION_NOT_SUPPORTED = 505;
	public static final int RESPONSE_CODE_VARIANT_ALSO_NEGOTIATES = 506;
	public static final int RESPONSE_CODE_INSUFFICIENT_STORAGE = 507;
	public static final int RESPONSE_CODE_BANDWIDTH_LIMIT_EXCEEDED = 509;
	public static final int RESPONSE_CODE_NOT_EXTENDED = 510;
	public static final String METHOD_GET = "GET";
	public static final String METHOD_POST = "POST";
	public static final String METHOD_PUT = "PUT";
	public static final String METHOD_DELETE = "DELETE";
	public static final String CHARSET_UTF8 = "UTF-8";
	public static final String CONTENT_TYPE_MULTIPART_RELATED = "multipart/related; boundary=\"END_OF_PART\"";
	public static final String CONTENT_TYPE_WWW_FORM = "application/x-www-form-urlencoded";
	public static final String CONTENT_TYPE_ATOM_XML = "application/atom+xml";
	public static final String CONTENT_TYPE_JSON = "application/json";
	//
	private final URL url;
	private String method = HttpRequest.METHOD_GET;
	private Map<String, String> requestHeaders;
	private InputStream requestData;
	private long responseLength;
	private int responseCode;
	private String responseMessage;
	private Map<String, String> responseHeaders;
	private InputStream responseData;
	private String responseString;

	public HttpRequest(URL url) {
		this.url = url;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public void setRequestHeaders(Map<String, String> requestHeaders) {
		this.requestHeaders = requestHeaders;
	}

	public void setRequestData(Map<String, String> requestData) throws UnsupportedEncodingException {
		StringBuilder sb = new StringBuilder();
		for (String key : requestData.keySet()) {
			for (String value : requestData.get(key).split("\n")) {
				sb.append("&");
				sb.append(key);
				sb.append("=");
				sb.append(value);
			}
		}
		sb.delete(0, 1);
		this.setRequestData(sb.toString());
	}

	public void setRequestData(String requestData) throws UnsupportedEncodingException {
		this.setRequestData(requestData.getBytes(HttpRequest.CHARSET_UTF8));
	}

	public void setRequestData(byte[] requestData) {
		this.setRequestData(new ByteArrayInputStream(requestData));
	}

	public void setRequestData(InputStream requestData) {
		this.requestData = requestData;
	}

	public long getResponseLength() {
		return this.responseLength;
	}

	public int getResponseCode() {
		return this.responseCode;
	}

	public String getResponseMessage() {
		return this.responseMessage;
	}

	public Map<String, String> getResponseHeaders() {
		return this.responseHeaders;
	}

	public InputStream getResponseData() {
		return this.responseData;
	}

	public String getResponseString() {
		if (this.responseString == null) {
			InputStream is = this.getResponseData();
			StringBuilder sb = new StringBuilder();
			try {
				byte[] buffer = new byte[HttpRequest.DEFAULT_BUFFER_SIZE];
				for (int length; (length = is.read(buffer)) > 0;) {
					sb.append(new String(buffer, 0, length));
				}
				is.close();
			} catch (IOException ex) {
			}
			this.responseString = sb.toString();
		}
		return this.responseString;
	}

	public void send() throws IOException {
		this.resetResponse();
		HttpURLConnection connection = (HttpURLConnection) this.url.openConnection();
		connection.setDoInput(true);
		connection.setDoOutput(true);
		connection.setRequestMethod(this.method);
		if (this.requestHeaders != null) {
			for (String key : this.requestHeaders.keySet()) {
				for (String value : this.requestHeaders.get(key).split("\n")) {
					connection.addRequestProperty(key, value);
				}
			}
		}
		if (!this.method.equals(HttpRequest.METHOD_GET)) {
			OutputStream os = connection.getOutputStream();
			byte[] buffer = new byte[HttpRequest.DEFAULT_BUFFER_SIZE];
			for (int length; (length = this.requestData.read(buffer)) > 0;) {
				os.write(buffer, 0, length);
			}
			os.flush();
			os.close();
		}
		connection.disconnect();
		try {
			this.responseLength = connection.getContentLengthLong();
			this.responseCode = connection.getResponseCode();
			this.responseMessage = connection.getResponseMessage();
			Map<String, List<String>> responseHeaders = connection.getHeaderFields();
			this.responseHeaders = new LinkedHashMap<>();
			for (String key : responseHeaders.keySet()) {
				for (String value : responseHeaders.get(key)) {
					if (key != null) {
						if (this.responseHeaders.containsKey(key)) {
							value = this.responseHeaders.get(key) + "\n" + value;
						}
						this.responseHeaders.put(key, value);
					}
				}
			}
			this.responseData = connection.getInputStream();
		} catch (FileNotFoundException ex) {
			this.responseData = connection.getErrorStream();
		}
	}

	private void resetResponse() {
		this.responseLength = -1;
		this.responseCode = -1;
		this.responseMessage = null;
		this.responseHeaders = null;
		this.responseData = null;
		this.responseString = null;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.method);
		sb.append(" ");
		String path = this.url.getPath();
		if (path.length() == 0) {
			path = "/";
		}
		sb.append(path);
		String query = this.url.getQuery();
		if (query != null) {
			sb.append("?");
			sb.append(query);
		}
		sb.append(" ");
		sb.append(HttpRequest.HTTP_VERSION);
		sb.append("\nHost: ");
		sb.append(this.url.getHost());
		int port = this.url.getPort();
		if (port > -1) {
			sb.append(":");
			sb.append(port);
		}
		if (this.requestHeaders != null) {
			for (String key : this.requestHeaders.keySet()) {
				for (String value : requestHeaders.get(key).split("\n")) {
					if (key != null) {
						sb.append("\n");
						sb.append(key);
						sb.append(": ");
						sb.append(value);
					}
				}
			}
		}
		if (!this.method.equals(HttpRequest.METHOD_GET)) {
			try {
				this.requestData.reset();
				sb.append("\n\n");
				byte[] buffer = new byte[HttpRequest.DEFAULT_BUFFER_SIZE];
				for (int length; (length = this.requestData.read(buffer)) > 0;) {
					sb.append(new String(buffer, 0, length));
				}
				this.requestData.close();
			} catch (IOException ex) {
			}
		}
		return sb.toString();
	}
}
