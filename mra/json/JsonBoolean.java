package mra.json;

import mra.xml.XmlObject;
import mra.xml.XmlTag;

public class JsonBoolean extends JsonValue<Boolean> {

	public static JsonBoolean parse(String json) throws JsonException {
		json = json.trim();
		switch (json) {
			case "null":
				return new JsonNull();
			case "true":
				return new JsonTrue();
			case "false":
				return new JsonFalse();
			default:
				throw new JsonException("not a JSON boolean");
		}
	}

	public JsonBoolean(Boolean value) {
		super(value);
	}

	private String toDataString() {
		Boolean value = this.getValue();
		return (value == null ? "null" : value ? "true" : "false");
	}

	@Override
	String toString(boolean formatted, int tabs, boolean isFirst) {
		StringBuilder sb = new StringBuilder();
		if (!isFirst && formatted) {
			sb.append("\n");
			for (int j = 0; j < tabs; j++) {
				sb.append(JsonString.CHAR_TAB);
			}
		}
		sb.append(this.toDataString());
		return sb.toString();
	}

	@Override
	public XmlObject toXmlObject() {
		return new XmlTag(this.toDataString());
	}
}
