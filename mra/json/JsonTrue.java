package mra.json;

public class JsonTrue extends JsonBoolean {

	public static JsonTrue parse(String json) throws JsonException {
		JsonBoolean jsonBoolean = JsonBoolean.parse(json);
		if (jsonBoolean instanceof JsonTrue) {
			return (JsonTrue) jsonBoolean;
		} else {
			throw new JsonException("not a JSON true object");
		}
	}

	public JsonTrue() {
		super(true);
	}
}
