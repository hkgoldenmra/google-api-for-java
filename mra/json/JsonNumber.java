package mra.json;

import mra.xml.XmlObject;
import mra.xml.XmlString;
import mra.xml.XmlTag;

public class JsonNumber extends JsonValue<Number> {

	public static JsonNumber parse(String json) throws JsonException {
		json = json.trim();
		try {
			return new JsonNumber(Double.parseDouble(json));
		} catch (NumberFormatException ex) {
			throw new JsonException("not a JSON number");
		}
	}

	public JsonNumber(Number value) {
		super(value);
	}

	@Override
	public Number getValue() {
		Number number = super.getValue();
		double value = number.doubleValue();
		if (value % 1.0 == 0.0) {
			return number.longValue();
		} else {
			return value;
		}
	}

	@Override
	String toString(boolean formatted, int tabs, boolean isFirst) {
		StringBuilder sb = new StringBuilder();
		if (!isFirst && formatted) {
			sb.append("\n");
			for (int j = 0; j < tabs; j++) {
				sb.append(JsonString.CHAR_TAB);
			}
		}
		sb.append(this.getValue());
		return sb.toString();
	}

	@Override
	public XmlObject toXmlObject() {
		return new XmlTag("number", new XmlObject[]{new XmlString(this.getValue().toString())});
	}
}
