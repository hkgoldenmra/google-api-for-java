package mra.google.ingress;

public class IngressResonatorL3 extends IngressResonators {

	IngressResonatorL3(String guid, IngressAgent owner) {
		this(guid, owner, -1, -1);
	}

	IngressResonatorL3(String guid, IngressAgent owner, int remainedEnergy, int distance) {
		super(guid, owner, 3, 2000, remainedEnergy, distance);
	}
}
