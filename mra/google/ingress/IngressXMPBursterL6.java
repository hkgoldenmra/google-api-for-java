package mra.google.ingress;

public class IngressXMPBursterL6 extends IngressXMPBursters {

	IngressXMPBursterL6(String guid, IngressAgent owner) {
		super(guid, owner, 6, 1500, 112);
	}
}
