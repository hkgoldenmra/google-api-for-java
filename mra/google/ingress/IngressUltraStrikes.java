package mra.google.ingress;

public abstract class IngressUltraStrikes extends IngressWeapons {

	static IngressUltraStrikes createIngressUltraStrikes(String guid, IngressAgent owner, int level) {
		switch (level) {
			case 1: {
				return new IngressUltraStrikeL1(guid, owner);
			}
			case 2: {
				return new IngressUltraStrikeL2(guid, owner);
			}
			case 3: {
				return new IngressUltraStrikeL3(guid, owner);
			}
			case 4: {
				return new IngressUltraStrikeL4(guid, owner);
			}
			case 5: {
				return new IngressUltraStrikeL5(guid, owner);
			}
			case 6: {
				return new IngressUltraStrikeL6(guid, owner);
			}
			case 7: {
				return new IngressUltraStrikeL7(guid, owner);
			}
			case 8: {
				return new IngressUltraStrikeL8(guid, owner);
			}
		}
		return null;
	}

	IngressUltraStrikes(String guid, IngressAgent owner, int level, int damage, int radius, int cost) {
		super(guid, owner, "Ultra Strike", level, damage, radius, cost);
	}
}
