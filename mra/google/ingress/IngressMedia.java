package mra.google.ingress;

import java.net.URL;

public class IngressMedia extends IngressStorableInventory {

	private final String title;
	private final URL reference;

	IngressMedia(String guid, IngressAgent owner, String title, URL reference) {
		super(guid, owner, 20, "Media", "Very Common");
		this.title = title;
		this.reference = reference;
	}

	public String getTitle() {
		return this.title;
	}

	public URL getReference() {
		return this.reference;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("\nTitle = ");
		sb.append(this.getTitle());
		sb.append("\nReference = ");
		sb.append(this.getReference() == null ? "<null>" : this.getReference().toString());
		return sb.toString();
	}
}
