package mra.google.ingress;

public class IngressPowerCubeL2 extends IngressPowerCubes {

	IngressPowerCubeL2(String guid, IngressAgent owner) {
		super(guid, owner, 2);
	}
}
