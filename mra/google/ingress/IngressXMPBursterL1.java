package mra.google.ingress;

public class IngressXMPBursterL1 extends IngressXMPBursters {

	IngressXMPBursterL1(String guid, IngressAgent owner) {
		super(guid, owner, 1, 150, 42);
	}
}
