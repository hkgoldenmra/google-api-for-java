package mra.google.ingress;

import java.util.ArrayList;

public abstract class IngressCapsules extends IngressInventory {

	private final IngressGenericContainer<IngressStorableInventory> ingressInventoryContainer = new IngressGenericContainer<>();

	IngressCapsules(String guid, IngressAgent owner, int recycle, String name, String rarity) {
		super(guid, owner, recycle, name, rarity);
	}

	void addIngressInventory(IngressStorableInventory ingressStorableInventory) {
		this.ingressInventoryContainer.add(ingressStorableInventory);
	}

	public IngressStorableInventory[] getAll() {
		ArrayList<IngressStorableInventory> ingressStorableInventories = this.ingressInventoryContainer.getAll();
		return ingressStorableInventories.toArray(new IngressStorableInventory[ingressStorableInventories.size()]);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("\nContainer = {");
		sb.append(this.ingressInventoryContainer.toString());
		sb.append("\n}");
		return sb.toString();
	}
}
