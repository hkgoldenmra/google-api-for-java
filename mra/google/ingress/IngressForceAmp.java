package mra.google.ingress;

public class IngressForceAmp extends IngressMods {

	private final int forceAmplifier;

	IngressForceAmp(String guid, IngressAgent owner, int removalStickiness) {
		super(guid, owner, 80, "Force Amp", "Rare", 800, removalStickiness);
		this.forceAmplifier = 2000;
	}

	public int getForceAmplifier() {
		return this.forceAmplifier;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("\nForce Amplifier = ");
		sb.append(this.getForceAmplifier());
		return sb.toString();
	}
}
