package mra.google.ingress;

public class IngressUltraStrikeL2 extends IngressUltraStrikes {

	IngressUltraStrikeL2(String guid, IngressAgent owner) {
		super(guid, owner, 2, 300, 13, 20);
	}
}
