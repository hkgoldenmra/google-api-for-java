package mra.google.ingress;

public class IngressRecycleResponse {

	private final String[] guids;

	IngressRecycleResponse(String[] guids) {
		this.guids = guids;
	}

	public String[] getGuids() {
		return this.guids;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("GUIDs = {\n");
		for (String guid : this.getGuids()) {
			sb.append(guid);
			sb.append("\n");
		}
		sb.append("}");
		return sb.toString();
	}
}
