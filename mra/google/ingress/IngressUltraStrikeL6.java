package mra.google.ingress;

public class IngressUltraStrikeL6 extends IngressUltraStrikes {

	IngressUltraStrikeL6(String guid, IngressAgent owner) {
		super(guid, owner, 6, 1500, 24, 360);
	}
}
