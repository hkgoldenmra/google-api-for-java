package mra.google.ingress;

public class IngressUltraStrikeL7 extends IngressUltraStrikes {

	IngressUltraStrikeL7(String guid, IngressAgent owner) {
		super(guid, owner, 7, 1800, 27, 490);
	}
}
