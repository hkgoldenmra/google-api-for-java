package mra.google.ingress;

import java.net.URL;

public class IngressMission extends IngressNonAgentObject {

	private final String title;
	private final String description;
	private final String type;
	private final URL logoURL;
	private final URL achievementURL;
	private final IngressWaypoint[] ingressWaypoints;

	IngressMission(String guid, IngressAgent owner, String title, String description, String type, URL logoURL, URL achievementURL, IngressWaypoint[] ingressWaypoints) {
		super(guid, owner);
		this.title = title;
		this.description = description;
		this.type = type;
		this.logoURL = logoURL;
		this.achievementURL = achievementURL;
		this.ingressWaypoints = ingressWaypoints;
	}

	public String getTitle() {
		return this.title;
	}

	public String getDescription() {
		return this.description;
	}

	public String getType() {
		return this.type;
	}

	public URL getLogoURL() {
		return this.logoURL;
	}

	public URL getAchievementURL() {
		return this.achievementURL;
	}

	public IngressWaypoint[] getIngressWaypoints() {
		return this.ingressWaypoints;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("\nTitle = ");
		sb.append(this.getTitle());
		sb.append("\nDescription = ");
		sb.append(this.getDescription());
		sb.append("\nType = ");
		sb.append(this.getType());
		sb.append("\nLogo URL = ");
		sb.append(this.getLogoURL() == null ? "<null>" : this.getLogoURL().toString());
		sb.append("\nAchievement URL = ");
		sb.append(this.getAchievementURL() == null ? "<null>" : this.getAchievementURL().toString());
		sb.append("\nIngress Waypoints = {");
		if (this.ingressWaypoints != null) {
			for (int i = 0; i < this.ingressWaypoints.length; i++) {
				if (i > 0) {
					sb.append(",");
				}
				sb.append("\n{\n");
				sb.append(this.ingressWaypoints[i].toString());
				sb.append("\n}");
			}
		}
		sb.append("\n}");
		return sb.toString();
	}

}
