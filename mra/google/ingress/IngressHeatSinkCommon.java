package mra.google.ingress;

public class IngressHeatSinkCommon extends IngressHeatSinks {

	IngressHeatSinkCommon(String guid, IngressAgent owner, int removalStickiness) {
		super(guid, owner, 40, "Heat Sink", "Common", 400, removalStickiness, 200000);
	}
}
