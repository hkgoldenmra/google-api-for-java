package mra.google.ingress;

public class IngressXMPBursterL3 extends IngressXMPBursters {

	IngressXMPBursterL3(String guid, IngressAgent owner) {
		super(guid, owner, 3, 500, 58);
	}
}
