package mra.google.ingress;

public class IngressPortalKey extends IngressStorableInventory {

	private final String ingressPortalGuid;
	private final long latitude;
	private final long longitude;
	private final String title;
	private final String address;
	private final String coverUrl;

	IngressPortalKey(String guid, IngressAgent owner, String ingressPortalGuid, long latitude, long longitude, String title, String address, String coverUrl) {
		super(guid, owner, 20, "Portal Key", "Very Common");
		this.ingressPortalGuid = ingressPortalGuid;
		this.latitude = latitude;
		this.longitude = longitude;
		this.title = title;
		this.address = address;
		this.coverUrl = coverUrl;
	}

	public String getIngressPortalGuid() {
		return this.ingressPortalGuid;
	}

	public long getLatitude() {
		return this.latitude;
	}

	public long getLongitude() {
		return this.longitude;
	}

	public String getAddress() {
		return this.address;
	}

	public String getTitle() {
		return this.title;
	}

	public String getCoverUrl() {
		return this.coverUrl;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("\nIngress Portal GUID = ");
		sb.append(this.getIngressPortalGuid());
		sb.append("\nLatitude = ");
		sb.append(this.getLatitude());
		sb.append("\nLongitude = ");
		sb.append(this.getLongitude());
		sb.append("\nTitle = ");
		sb.append(this.getTitle());
		sb.append("\nCover URL = ");
		sb.append(this.getCoverUrl());
		sb.append("\nAddress = ");
		sb.append(this.getAddress());
		return sb.toString();
	}
}
