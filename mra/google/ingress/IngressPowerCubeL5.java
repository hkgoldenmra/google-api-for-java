package mra.google.ingress;

public class IngressPowerCubeL5 extends IngressPowerCubes {

	IngressPowerCubeL5(String guid, IngressAgent owner) {
		super(guid, owner, 5);
	}
}
