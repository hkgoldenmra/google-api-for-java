package mra.google.ingress;

public class IngressResonatorL8 extends IngressResonators {

	IngressResonatorL8(String guid, IngressAgent owner) {
		this(guid, owner, -1, -1);
	}

	IngressResonatorL8(String guid, IngressAgent owner, int remainedEnergy, int distance) {
		super(guid, owner, 8, 6000, remainedEnergy, distance);
	}
}
