package mra.google.ingress;

import java.util.Date;
import mra.google.Google;

public class IngressLog extends IngressObject {

	private final Date date;
	private final boolean secure;
	private final String agentCode;
	private final String agentFaction;
	private final String agentAction;
	private final String sentToAgentCode;
	private final String sentToAgentFaction;
	private final String message;
	private final String portalName;
	private final String portalAddress;
	private final long portalLatitude;
	private final long portalLongitude;
	private final String linkedToPortalName;
	private final String linkedToPortalAddress;
	private final long linkedToPortalLatitude;
	private final long linkedToPortalLongitude;
	private final long mus;

	public IngressLog(String guid, Date date, boolean secure, String agentCode, String agentFaction, String agentAction, String sentToAgentCode, String sentToAgentFaction, String message, String portalName, String portalAddress, long portalLatitude, long portalLongitude, String linkedToPortalName, String linkedToPortalAddress, long linkedToPortalLatitude, long linkedToPortalLongitude, long mus) {
		super(guid);
		this.date = date;
		this.secure = secure;
		this.agentCode = agentCode;
		this.agentFaction = agentFaction;
		this.agentAction = agentAction;
		this.sentToAgentCode = sentToAgentCode;
		this.sentToAgentFaction = sentToAgentFaction;
		this.message = message;
		this.portalName = portalName;
		this.portalAddress = portalAddress;
		this.portalLatitude = portalLatitude;
		this.portalLongitude = portalLongitude;
		this.linkedToPortalName = linkedToPortalName;
		this.linkedToPortalAddress = linkedToPortalAddress;
		this.linkedToPortalLatitude = linkedToPortalLatitude;
		this.linkedToPortalLongitude = linkedToPortalLongitude;
		this.mus = mus;
	}

	public Date getDate() {
		return this.date;
	}

	public boolean isSecure() {
		return this.secure;
	}

	public String getAgentCode() {
		return this.agentCode;
	}

	public String getAgentFaction() {
		return this.agentFaction;
	}

	public String getAgentAction() {
		return this.agentAction;
	}

	public String getSentToAgentCode() {
		return this.sentToAgentCode;
	}

	public String getSentToAgentFaction() {
		return this.sentToAgentFaction;
	}

	public String getMessage() {
		return this.message;
	}

	public String getPortalName() {
		return this.portalName;
	}

	public String getPortalAddress() {
		return this.portalAddress;
	}

	public long getPortalLatitude() {
		return this.portalLatitude;
	}

	public long getPortalLongitude() {
		return this.portalLongitude;
	}

	public String getLinkedToPortalName() {
		return this.linkedToPortalName;
	}

	public String getLinkedToPortalAddress() {
		return this.linkedToPortalAddress;
	}

	public long getLinkedToPortalLatitude() {
		return this.linkedToPortalLatitude;
	}

	public long getLinkedToPortalLongitude() {
		return this.linkedToPortalLongitude;
	}

	public long getMus() {
		return this.mus;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(Google.toHumanDateTimeFormat(this.getDate()));
		sb.append("] - [");
		sb.append(this.getAgentCode());
		sb.append("] [");
		sb.append(this.getAgentFaction());
		sb.append("]");
		String agentAction = this.getAgentAction();
		sb.append(agentAction);
		if (agentAction.equals("")) {
			sb.append(" say [");
			sb.append(this.getMessage());
			sb.append("]");
		} else if (agentAction.equals("captured")
			|| agentAction.equals("deployed a Resonator on")
			|| agentAction.equals("destroyed a Resonator on")
			|| agentAction.equals("linked")
			|| agentAction.equals("created a Control Field @")
			|| agentAction.equals("destroyed a Control Field @")) {
			sb.append(this.getPortalName());
			sb.append(" ([");
			sb.append(this.getPortalAddress());
			sb.append("] - [");
			sb.append(this.getPortalLatitude());
			sb.append("],[");
			sb.append(this.getPortalLongitude());
			sb.append("])");
			if (agentAction.equals("linked")) {
				sb.append(" to [");
				sb.append(this.getLinkedToPortalName());
				sb.append("] (");
				sb.append(this.getLinkedToPortalAddress());
				sb.append(" - [");
				sb.append(this.getLinkedToPortalLatitude());
				sb.append("],[");
				sb.append(this.getLinkedToPortalLongitude());
				sb.append("])");
			} else if (agentAction.equals("created a Control Field @")) {
				sb.append(" +");
				sb.append(this.getMus());
				sb.append(" MUs");
			}
		}
		return sb.toString();
	}
}
