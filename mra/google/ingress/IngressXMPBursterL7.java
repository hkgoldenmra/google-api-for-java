package mra.google.ingress;

public class IngressXMPBursterL7 extends IngressXMPBursters {

	IngressXMPBursterL7(String guid, IngressAgent owner) {
		super(guid, owner, 7, 1800, 138);
	}
}
