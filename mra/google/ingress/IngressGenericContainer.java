package mra.google.ingress;

import java.util.ArrayList;

class IngressGenericContainer<T extends IngressInventory> {

	private final ArrayList<T> ts = new ArrayList<>();

	IngressGenericContainer() {
	}

	void add(T t) {
		this.ts.add(t);
	}

	ArrayList<T> getAll() {
		return this.ts;
	}

	int count() {
		return this.ts.size();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < this.count(); i++) {
			if (i > 0) {
				sb.append(",");
			}
			sb.append("\n{\n");
			sb.append(this.ts.get(i).toString());
			sb.append("\n}");
		}
		return sb.toString();
	}
}
