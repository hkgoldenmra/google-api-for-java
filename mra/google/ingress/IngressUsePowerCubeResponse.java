package mra.google.ingress;

public class IngressUsePowerCubeResponse {

	private final String guid;
	private final int xm;

	IngressUsePowerCubeResponse(String guid, int xm) {
		this.guid = guid;
		this.xm = xm;
	}

	public String getGuid() {
		return this.guid;
	}

	public int getXM() {
		return this.xm;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("GUID = ");
		sb.append(this.getGuid());
		sb.append(", XM = ");
		sb.append(this.getXM());
		return sb.toString();
	}
}
