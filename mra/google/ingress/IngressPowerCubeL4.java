package mra.google.ingress;

public class IngressPowerCubeL4 extends IngressPowerCubes {

	IngressPowerCubeL4(String guid, IngressAgent owner) {
		super(guid, owner, 4);
	}
}
