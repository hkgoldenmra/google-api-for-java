package mra.google.ingress;

public class IngressMUFGCapsule extends IngressCapsules {

	IngressMUFGCapsule(String guid, IngressAgent owner) {
		super(guid, owner, 100, "MUFG Capsule", "Very Rare");
	}
}
