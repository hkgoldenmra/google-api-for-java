package mra.google.ingress;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import mra.http.HttpRequest;
import mra.json.JsonArray;
import mra.json.JsonNull;
import mra.json.JsonNumber;
import mra.json.JsonObject;
import mra.json.JsonParser;
import mra.json.JsonString;
import mra.json.JsonTrue;
import mra.json.JsonValue;

public class IngressIntel {

	private static final String PROTOCOL = "https";
	private static final String HOST = "www.ingress.com";

	////////// static method start //////////
	static String getFactionByChar(char c) {
		String faction = "Neutral";
		switch (c) {
			case 'R':
			case 'r': {
				faction = "Resistance";
			}
			break;
			case 'E':
			case 'e': {
				faction = "Enlightened";
			}
		}
		return faction;
	}

	static Map<String, String> createCsrfTokenHeaders(String sacSid, String token) {
		Map<String, String> requestHeaders = new LinkedHashMap<>();
		requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_JSON + "; charset=" + HttpRequest.CHARSET_UTF8);
		requestHeaders.put("Cookie", String.format("csrftoken=%s; SACSID=%s", token, sacSid));
		requestHeaders.put("X-CsrfToken", token);
		requestHeaders.put("Referer", "https://www.ingress.com/intel");
		return requestHeaders;
	}

	static JsonValue createGetIngressMissionsJSON(long minLatitude, long minLongitude, long maxLatitude, long maxLongitude, String v) {
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("minLatE6", new JsonNumber(minLatitude));
		jsonObject.setValue("minLngE6", new JsonNumber(minLongitude));
		jsonObject.setValue("maxLatE6", new JsonNumber(maxLatitude));
		jsonObject.setValue("maxLngE6", new JsonNumber(maxLongitude));
		jsonObject.setValue("v", new JsonString(v));
		return jsonObject;
	}

	static JsonValue createGetIngressPortalJSON(String guid, String v) {
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("guid", new JsonString(guid));
		jsonObject.setValue("v", new JsonString(v));
		return jsonObject;
	}

	static JsonValue createGetIngressScoreJSON(Long latitude, Long longitude, String v) {
		JsonObject jsonObject = new JsonObject();
		if (latitude != null && longitude != null) {
			jsonObject.setValue("latE6", new JsonNumber(latitude));
			jsonObject.setValue("lngE6", new JsonNumber(longitude));
		}
		jsonObject.setValue("v", new JsonString(v));
		return jsonObject;
	}

	static JsonValue createGetIngressMissionJSON(String guid, String v) {
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("guid", new JsonString(guid));
		jsonObject.setValue("v", new JsonString(v));
		return jsonObject;
	}

	static JsonValue createGetIngressLogsJSON(long minLatitude, long minLongitude, long maxLatitude, long maxLongitude, String v) {
		Date maxDate = new Date();
		Date minDate = new Date(maxDate.getTime() - 1000 * 60);
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("ascendingTimestampOrder", new JsonTrue());
		jsonObject.setValue("minLatE6", new JsonNumber(minLatitude));
		jsonObject.setValue("minLngE6", new JsonNumber(minLongitude));
		jsonObject.setValue("maxLatE6", new JsonNumber(maxLatitude));
		jsonObject.setValue("maxLngE6", new JsonNumber(maxLongitude));
		jsonObject.setValue("minTimestampMs", new JsonNumber(minDate.getTime()));
		jsonObject.setValue("maxTimestampMs", new JsonNumber(maxDate.getTime()));
		jsonObject.setValue("tab", new JsonString("all"));
		jsonObject.setValue("v", new JsonString(v));
		return jsonObject;
	}

	private final String sacSid;
	private final String token;
	private final String v;

	public IngressIntel(String sacSid, String token, String v) {
		this.sacSid = sacSid;
		this.token = token;
		this.v = v;
	}

	String getSacSid() {
		return this.sacSid;
	}

	String getToken() {
		return this.token;
	}

	String getV() {
		return this.v;
	}

	public IngressMission[] getIngressMissions(long minLatitude, long minLongitude, long maxLatitude, long maxLongitude) throws IOException {
		URL url = new URL(String.format("%s://%s/r/getTopMissionsInBounds", IngressIntel.PROTOCOL, IngressIntel.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressIntel.createCsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressIntel.createGetIngressMissionsJSON(minLatitude, minLongitude, maxLatitude, maxLongitude, this.getV()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		ArrayList<IngressMission> ingressMissions = new ArrayList<>();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = ((JsonObject) jsonValue);
		JsonValue jvResult = jsonObject.getValue("result");
		JsonArray jaResult = ((JsonArray) jvResult);
		for (JsonValue jvMission : jaResult.getValue()) {
			JsonArray jaMission = ((JsonArray) jvMission);
			String guid = ((JsonString) jaMission.getValue(0)).getValue();
			String title = ((JsonString) jaMission.getValue(1)).getValue();
			String path = ((JsonString) jaMission.getValue(2)).getValue();
			URL logo = new URL(path);
			ingressMissions.add(new IngressMission(guid, null, title, null, null, logo, logo, null));
		}
		return ingressMissions.toArray(new IngressMission[ingressMissions.size()]);
	}

	public IngressPortal getIngressPortal(String guid) throws IOException {
		URL url = new URL(String.format("%s://%s/r/getPortalDetails", IngressIntel.PROTOCOL, IngressIntel.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createGetIngressPortalJSON(guid, this.getV()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = ((JsonObject) jsonValue);
		JsonValue jvResult = jsonObject.getValue("result");
		JsonArray jaResult = ((JsonArray) jvResult);
		String faction = ((JsonString) jaResult.getValue(1)).getValue();
		faction = IngressIntel.getFactionByChar(faction.charAt(0));
		long latitude = ((JsonNumber) jaResult.getValue(2)).getValue().longValue();
		long longitude = ((JsonNumber) jaResult.getValue(3)).getValue().longValue();
		String path = ((JsonString) jaResult.getValue(7)).getValue();
		IngressImage cover = new IngressImage(null, null, new URL(path));
		String title = ((JsonString) jaResult.getValue(8)).getValue();
		JsonValue jvMods = jaResult.getValue(14);
		JsonArray jaMods = ((JsonArray) jvMods);
		IngressMods[] ingressMods = new IngressMods[4];
		for (int i = 0; i < ingressMods.length; i++) {
			JsonValue jvMod = jaMods.getValue(i);
			if (!(jvMod instanceof JsonNull)) {
				JsonObject joMod = ((JsonObject) jvMod);
				ingressMods[i] = IngressInventoryContainer.parseIngressMods(guid, null, joMod);
			}
		}
		JsonValue jvResonators = jaResult.getValue(15);
		JsonArray jaResonators = ((JsonArray) jvResonators);
		IngressResonators[] ingressResonatorses = new IngressResonators[8];
		for (int i = 0; i < ingressResonatorses.length; i++) {
			JsonValue jvResonator = jaResonators.getValue(i);
			if (!(jvResonator instanceof JsonNull)) {
				JsonArray jaResonator = ((JsonArray) jvResonator);
				int level = ((JsonNumber) jaResonator.getValue(1)).getValue().intValue();
				int energy = ((JsonNumber) jaResonator.getValue(2)).getValue().intValue();
				ingressResonatorses[i] = IngressResonators.createIngressResonators(guid, null, level, energy, -1);
			}
		}
		return new IngressPortal(guid, null, latitude, longitude, null, title, null, -1, -1, faction, null, cover, ingressResonatorses, ingressMods, null, null);
	}

	public IngressScore getIngressScore() throws IOException {
		URL url = new URL(String.format("%s://%s/r/getGameScore", IngressIntel.PROTOCOL, IngressIntel.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressIntel.createCsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressIntel.createGetIngressScoreJSON(null, null, this.getV()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = ((JsonObject) jsonValue);
		JsonValue jvResult = jsonObject.getValue("result");
		JsonArray jaResult = ((JsonArray) jvResult);
		JsonValue jvEnlightenedScore = jaResult.getValue(0);
		long enlightenedScore = Long.parseLong(((JsonString) jvEnlightenedScore).getValue());
		JsonValue jvResistanceScore = jaResult.getValue(1);
		long resistanceScore = Long.parseLong(((JsonString) jvResistanceScore).getValue());
		return new IngressScore(null, enlightenedScore, resistanceScore);
	}

	public IngressScore getIngressScore(long latitude, long longitude) throws IOException {
		URL url = new URL(String.format("%s://%s/r/getRegionScoreDetails", IngressIntel.PROTOCOL, IngressIntel.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressIntel.createCsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressIntel.createGetIngressScoreJSON(latitude, longitude, this.getV()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = ((JsonObject) jsonValue);
		JsonValue jvResult = jsonObject.getValue("result");
		JsonObject joResult = ((JsonObject) jvResult);
		JsonValue jvGameScore = joResult.getValue("gameScore");
		JsonArray jaGameScore = ((JsonArray) jvGameScore);
		JsonValue jvEnlightenedScore = jaGameScore.getValue(0);
		long enlightenedScore = Long.parseLong(((JsonString) jvEnlightenedScore).getValue());
		JsonValue jvResistanceScore = jaGameScore.getValue(1);
		long resistanceScore = Long.parseLong(((JsonString) jvResistanceScore).getValue());
		String regionName = ((JsonString) joResult.getValue("regionName")).getValue();
		return new IngressScore(regionName, enlightenedScore, resistanceScore);
	}

	public IngressMission getIngressMission(String guid) throws IOException {
		URL url = new URL(String.format("%s://%s/r/getMissionDetails", IngressIntel.PROTOCOL, IngressIntel.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressIntel.createCsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressIntel.createGetIngressMissionJSON(guid, this.getV()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = ((JsonObject) jsonValue);
		JsonValue jvResult = jsonObject.getValue("result");
		JsonArray jaResult = ((JsonArray) jvResult);
		String title = ((JsonString) jaResult.getValue(1)).getValue();
		String description = ((JsonString) jaResult.getValue(2)).getValue();
		String owner = ((JsonString) jaResult.getValue(3)).getValue();
		String faction = ((JsonString) jaResult.getValue(4)).getValue();
		faction = IngressIntel.getFactionByChar(faction.charAt(0));
		IngressAgent agent = new IngressAgent(null, owner, faction, -1, -1, null, null, null, null);
		String path = ((JsonString) jaResult.getValue(10)).getValue();
		URL logoURL = new URL(path);
		JsonValue jvWaypoints = jaResult.getValue(9);
		JsonArray jaWaypoints = ((JsonArray) jvWaypoints);
		ArrayList<IngressWaypoint> ingressWaypoints = new ArrayList<>();
		for (JsonValue jvWaypoint : jaWaypoints.getValue()) {
			JsonArray jaWaypoint = (JsonArray) jvWaypoint;
			String $guid = ((JsonString) jaWaypoint.getValue(1)).getValue();
			String $title = ((JsonString) jaWaypoint.getValue(2)).getValue();
			int $typeId = ((JsonNumber) jaWaypoint.getValue(4)).getValue().intValue();
			String $type = IngressWaypoint.WAYPOINY_TYPES_MAP.get($typeId);
			long $latitude = 0;
			long $longitude = 0;
			int $level = -1;
			int $energy = -1;
			String $faction = "Neutral";
			IngressImage cover = null;
			JsonValue jvInfo = jaWaypoint.getValue(5);
			if (jvInfo instanceof JsonArray) {
				JsonArray jaInfo = (JsonArray) jvInfo;
				if ($typeId != IngressWaypoint.$WAYPOINT_TYPE_VIEW) {
					$latitude = ((JsonNumber) jaInfo.getValue(2)).getValue().longValue();
					$longitude = ((JsonNumber) jaInfo.getValue(3)).getValue().longValue();
					$level = ((JsonNumber) jaInfo.getValue(4)).getValue().intValue();
					$energy = ((JsonNumber) jaInfo.getValue(5)).getValue().intValue();
					String $path = ((JsonString) jaInfo.getValue(7)).getValue();
					cover = new IngressImage(null, null, new URL($path));
					$faction = ((JsonString) jaInfo.getValue(1)).getValue();
					$faction = IngressIntel.getFactionByChar($faction.charAt(0));
				} else {
					$faction = null;
					$latitude = ((JsonNumber) jaInfo.getValue(1)).getValue().longValue();
					$longitude = ((JsonNumber) jaInfo.getValue(2)).getValue().longValue();
				}
				ingressWaypoints.add(new IngressWaypoint($guid, null, $latitude, $longitude, null, $title, null, $level, $energy, $faction, null, cover, null, null, null, null, $type, null));
			} else {
				throw new IngressException("Ingress Waypoints not match available count");
			}
		}
		return new IngressMission(guid, agent, title, description, null, logoURL, logoURL, ingressWaypoints.toArray(new IngressWaypoint[ingressWaypoints.size()]));
	}

	public void getIngressLogs() throws IOException {
		this.getIngressLogs(-90000000, -180000000, 90000000, 180000000);
	}

	public IngressLog[] getIngressLogs(long minLatitude, long minLongitude, long maxLatitude, long maxLongitude) throws IOException {
		URL url = new URL(String.format("%s://%s/r/getPlexts", IngressIntel.PROTOCOL, IngressIntel.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressIntel.createCsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressIntel.createGetIngressLogsJSON(minLatitude, minLongitude, maxLatitude, maxLongitude, this.getV()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		ArrayList<IngressLog> ingressLogs = new ArrayList<>();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = ((JsonObject) jsonValue);
		JsonValue jvResult = jsonObject.getValue("result");
		JsonArray jaResult = ((JsonArray) jvResult);
		for (JsonValue jvPlext : jaResult.getValue()) {
			JsonArray jaPlext = ((JsonArray) jvPlext);
			String guid = ((JsonString) jaPlext.getValue(0)).getValue();
			Date date = new Date(((JsonNumber) jaPlext.getValue(1)).getValue().longValue());
			JsonValue jvPlextInfo = jaPlext.getValue(2);
			JsonObject joPlextInfo = ((JsonObject) jvPlextInfo);
			JsonValue jvInfo = joPlextInfo.getValue("plext");
			JsonObject joInfo = ((JsonObject) jvInfo);
			JsonValue jvMarkup = joInfo.getValue("markup");
			JsonArray jaMarkup = ((JsonArray) jvMarkup);
			ingressLogs.add(this.setIngressLog(guid, date, jaMarkup));
		}
		return ingressLogs.toArray(new IngressLog[ingressLogs.size()]);
	}

	private IngressLog setIngressLog(String guid, Date date, JsonArray jaFields) {
		boolean secure = false;
		String agentCode = null;
		String agentFaction = null;
		String agentAction = null;
		String sentToAgentCode = null;
		String sentToAgentFaction = null;
		String message = null;
		String portalName = null;
		String portalAddress = null;
		long portalLatitude = -1;
		long portalLongitude = -1;
		String linkedToPortalName = null;
		String linkedToPortalAddress = null;
		long linkedToPortalLatitude = -1;
		long linkedToPortalLongitude = -1;
		long mus = -1;
		for (JsonValue jvField : jaFields.getValue()) {
			JsonArray jaField = ((JsonArray) jvField);
			String type = ((JsonString) jaField.getValue(0)).getValue();
			if (type.equals("SECURE")) {
				secure = true;
			} else if (type.equals("SENDER")) {
				JsonValue jvSender = jaField.getValue(1);
				JsonObject joSender = ((JsonObject) jvSender);
				String sender = ((JsonString) joSender.getValue("plain")).getValue();
				agentCode = sender.split(": ")[0];
				agentFaction = ((JsonString) joSender.getValue("team")).getValue();
			} else if (type.equals("PLAYER")) {
				JsonValue jvPlayer = jaField.getValue(1);
				JsonObject joPlayer = ((JsonObject) jvPlayer);
				agentCode = ((JsonString) joPlayer.getValue("plain")).getValue();
				agentFaction = ((JsonString) joPlayer.getValue("team")).getValue();
			} else if (type.equals("AT_PLAYER")) {
				JsonValue jvAtPlayer = jaField.getValue(1);
				JsonObject joAtPlayer = ((JsonObject) jvAtPlayer);
				String atPlayer = ((JsonString) joAtPlayer.getValue("plain")).getValue();
				sentToAgentCode = atPlayer.substring(1);
				sentToAgentFaction = ((JsonString) joAtPlayer.getValue("team")).getValue();
			} else if (type.equals("PORTAL")) {
				JsonValue jvPortal = jaField.getValue(1);
				JsonObject joPortal = ((JsonObject) jvPortal);
				if (portalName == null) {
					portalName = ((JsonString) joPortal.getValue("name")).getValue();
					portalAddress = ((JsonString) joPortal.getValue("address")).getValue().replaceAll("\n", " ");
					portalLatitude = ((JsonNumber) joPortal.getValue("latE6")).getValue().longValue();
					portalLongitude = ((JsonNumber) joPortal.getValue("lngE6")).getValue().longValue();
				} else {
					linkedToPortalName = ((JsonString) joPortal.getValue("name")).getValue();
					linkedToPortalAddress = ((JsonString) joPortal.getValue("address")).getValue().replaceAll("\n", " ");
					linkedToPortalLatitude = ((JsonNumber) joPortal.getValue("latE6")).getValue().longValue();
					linkedToPortalLongitude = ((JsonNumber) joPortal.getValue("lngE6")).getValue().longValue();
				}
			} else if (type.equals("TEXT")) {
				JsonValue jvText = jaField.getValue(1);
				JsonObject joText = ((JsonObject) jvText);
				String text = ((JsonString) joText.getValue("plain")).getValue().trim();
				if (agentAction == null) {
					agentAction = text;
				} else {
					if (sentToAgentCode != null && message == null) {
						message = text;
					} else {
						try {
							mus = Long.parseLong(text);
						} catch (Exception ex) {
						}
					}
				}
			}
		}
		return new IngressLog(guid, date, secure, agentCode, agentFaction, agentAction, sentToAgentCode, sentToAgentFaction, message, portalName, portalAddress, portalLatitude, portalLongitude, linkedToPortalName, linkedToPortalAddress, linkedToPortalLatitude, linkedToPortalLongitude, mus);
	}
}
