package mra.google.ingress;

public class IngressInvitationInfo {

	private final int availableInvitations;
	private final IngressRecruit[] ingressRecruits;

	IngressInvitationInfo(int availableInvitations, IngressRecruit[] ingressRecruits) {
		this.availableInvitations = availableInvitations;
		this.ingressRecruits = ingressRecruits;
	}

	public int getAvailableInvitations() {
		return this.availableInvitations;
	}

	public IngressRecruit[] getIngressRecruits() {
		return this.ingressRecruits;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Available Invitations = ");
		sb.append(this.getAvailableInvitations());
		sb.append("\nInvitations Send To = {");
		for (int i = 0; i < this.ingressRecruits.length; i++) {
			if (i > 0) {
				sb.append(",");
			}
			sb.append("\n{\n");
			sb.append(this.ingressRecruits[i].toString());
			sb.append("\n}");
		}
		sb.append("\n}");
		return sb.toString();
	}
}
