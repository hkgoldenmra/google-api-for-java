package mra.google.ingress;

public class IngressHeatSinkVeryRare extends IngressHeatSinks {

	IngressHeatSinkVeryRare(String guid, IngressAgent owner, int removalStickiness) {
		super(guid, owner, 100, "Heat Sink", "Very Rare", 1000, removalStickiness, 700000);
	}
}
