package mra.google.ingress;

public class IngressLawsonPowerCube extends IngressStorableInventory {

	IngressLawsonPowerCube(String guid, IngressAgent owner) {
		super(guid, owner, 0, "Lawson Power Cube", "Very Rare");
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		return sb.toString();
	}
}
