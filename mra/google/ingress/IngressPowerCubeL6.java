package mra.google.ingress;

public class IngressPowerCubeL6 extends IngressPowerCubes {

	IngressPowerCubeL6(String guid, IngressAgent owner) {
		super(guid, owner, 6);
	}
}
