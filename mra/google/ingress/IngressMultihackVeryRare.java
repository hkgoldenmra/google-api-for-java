package mra.google.ingress;

public class IngressMultihackVeryRare extends IngressMultihacks {

	IngressMultihackVeryRare(String guid, IngressAgent owner, int removalStickiness) {
		super(guid, owner, 100, "Multi-hack", "Very Rare", 1000, removalStickiness, 12);
	}
}
