package mra.google.ingress;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import mra.http.HttpRequest;
import mra.json.JsonArray;
import mra.json.JsonBoolean;
import mra.json.JsonFalse;
import mra.json.JsonNumber;
import mra.json.JsonNull;
import mra.json.JsonObject;
import mra.json.JsonParser;
import mra.json.JsonString;
import mra.json.JsonTrue;
import mra.json.JsonValue;

public class IngressApp {

	private static final String PROTOCOL = "https";
	private static final String HOST = "m-dot-betaspike.appspot.com";
	public static final double EARTH_RADIUS_IN_METER = 6371000;
	public static final double STANDARD_HUMAN_WALKING_SPEED_IN_METER_PER_SECOND = 1.0;

	private static double degreesToRadius(double degrees) {
		return degrees * (Math.PI / 180);
	}

	private static String getIngressErrorMessage(String errorCode) {
		String error = "Unknown error: " + errorCode;
		if (errorCode.startsWith("TOO_SOON_")) {
			int secs = 300;
			Pattern pattern = Pattern.compile("^TOO_SOON_([0-9]+)_SECS$");
			Matcher matcher = pattern.matcher(errorCode);
			if (matcher.matches()) {
				secs = Integer.parseInt(matcher.group(1));
			}
			error = String.format("Portal running hot! Unsafe to acquire items. Estimated time to cooldown: %d seconds", secs);
		} else if (errorCode.equals("TOO_OFTEN")) {
			error = "Portal burned out! It may take significant time for the Portal to reset";
		} else if (errorCode.equals("OUT_OF_RANGE")) {
			error = "Portal is out of range";
		} else if (errorCode.equals("NEED_MORE_ENERGY")) {
			error = "You don't have enough XM";
		} else if (errorCode.equals("PORTAL_OUT_OF_RANGE")) {
			error = "Portal out of range";
		} else if (errorCode.equals("INVENTORY_FULL")) {
			error = "Too many items in Inventory. Your Inventory can have no more than 2000 items";
		} else if (errorCode.equals("TOO_MANY_RESONATORS_FOR_LEVEL_BY_USER")) {
			error = "Too many resonators with same level by you";
		} else if (errorCode.equals("PORTAL_AT_MAX_RESONATORS")) {
			error = "Portal has all resonators";
		} else if (errorCode.equals("ITEM_DOES_NOT_EXIST")) {
			error = "Item does not exist";
		} else if (errorCode.equals("EDGE_ALREADY_EXISTS")) {
			error = "Link already exists";
		} else if (errorCode.equals("DESTINATION_MUST_BE_FULL")) {
			error = "Portal missing Resonators";
		} else if (errorCode.equals("DESTINATION_UNOWNED")) {
			error = "Neutral destination Portal";
		} else if (errorCode.equals("DESTINATION_WRONG_TEAM")) {
			error = "Enemy Portal";
		} else if (errorCode.equals("ORIGIN_LINK_CAPACITY_REACHED")) {
			error = "Portal can't support more Links";
		} else if (errorCode.equals("CROSSES_EXISTING_LINK")) {
			error = "Link crosses an existing link";
		} else if (errorCode.equals("CONTAINED_WITHIN_CAPTURED_REGION")) {
			error = "Portal is within existing field";
		} else if (errorCode.equals("PORTAL_BELONGS_TO_ENEMY")) {
			error = "Enemy Portal";
		} else if (errorCode.equals("RESONATORS_FULLY_CHARGED")) {
			error = "Fully charged";
		} else if (errorCode.equals("SERVER_ERROR")) {
			error = "Server error";
		}
		return error;
	}

	public static double getGeomeasurementDistance(double latitude1, double longitude1, double latitude2, double longitude2) {
		double latitudeDistance = IngressApp.degreesToRadius(latitude2 - latitude1);
		double longitudeDistance = IngressApp.degreesToRadius(longitude2 - longitude1);
		double a = Math.pow(Math.sin(latitudeDistance / 2), 2)
			+ Math.pow(Math.sin(longitudeDistance / 2), 2)
			* Math.cos(IngressApp.degreesToRadius(latitude1))
			* Math.cos(IngressApp.degreesToRadius(latitude2));
		double b = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = IngressApp.EARTH_RADIUS_IN_METER * b;
		return distance;
	}

	////////// static method start //////////
	static Map<String, String> createXsrfTokenHeaders(String sacSid, String token) {
		Map<String, String> requestHeaders = new LinkedHashMap<>();
		requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_JSON + "; charset=" + HttpRequest.CHARSET_UTF8);
		requestHeaders.put("Cookie", "SACSID=" + sacSid);
		requestHeaders.put("X-XsrfToken", token);
		return requestHeaders;
	}

	static JsonValue createGetIngressScoreJSON(Long latitude, Long longitude) {
		JsonObject jsonObject = new JsonObject();
		if (latitude != null && longitude != null) {
			JsonObject joParams = new JsonObject();
			joParams.setValue("location", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		} else {
			jsonObject.setValue("params", new JsonArray());
		}
		return jsonObject;
	}

	static JsonValue createGetIngressAgentByGuidJSON(String guid) {
		JsonArray jsonArray = new JsonArray();
		jsonArray.addValue(new JsonString(guid));
		JsonArray jaParams = new JsonArray();
		jaParams.addValue(jsonArray);
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", jaParams);
		return jsonObject;
	}

	static JsonValue createGetIngressAgentJSON(String code) {
		JsonArray jsonArray = new JsonArray();
		jsonArray.addValue(new JsonString(code));
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", jsonArray);
		return jsonObject;
	}

	static JsonValue createGetIngressInvitationInfoJSON() {
		JsonObject joParams = new JsonObject();
		joParams.setValue("params", new JsonArray());
		return joParams;
	}

	static JsonValue createGetIngressInventoryContainerJSON() {
		JsonObject jsonLastQueryTimestamp = new JsonObject();
		jsonLastQueryTimestamp.setValue("lastQueryTimestamp", new JsonNumber(0));
		JsonObject jsonParams = new JsonObject();
		jsonParams.setValue("params", jsonLastQueryTimestamp);
		return jsonParams;
	}

	static JsonValue createGetIngressPortalJSON(String guid, String clientBlob) {
		return IngressApp.createGetIngressPortalJSON(guid, 0L, 0L, clientBlob);
	}

	static JsonValue createGetIngressPortalJSON(String guid, long latitude, long longitude, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		JsonArray jaGuids = new JsonArray();
		jaGuids.addValue(new JsonString(guid));
		joParams.setValue("guids", jaGuids);
		joParams.setValue("location", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		JsonArray jaTimestampMs = new JsonArray();
		jaTimestampMs.addValue(new JsonNumber(0));
		joParams.setValue("timestampsMs", jaTimestampMs);
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createGetIngressMissionJSON(String guid, long latitude, long longitude, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		joParams.setValue("value", new JsonString(guid));
		joParams.setValue("playerLocation", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createGetIngressMissionsByIngressPortalGuidJSON(String guid, long latitude, long longitude, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		joParams.setValue("value", new JsonString(guid));
		joParams.setValue("playerLocation", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createGetIngressMissionsByLocationJSON(long latitude, long longitude, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		joParams.setValue("location", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createGetIngressImageJSON(String ingressPortalGuid) {
		JsonObject joParams = new JsonObject();
		joParams.setValue("portalGuid", new JsonString(ingressPortalGuid));
		joParams.setValue("maxItemsPerPage", new JsonNumber(10));
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createInstallIngressModJSON(String portalGuid, long latitude, long longitude, String modGuid, int index, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		joParams.setValue("modableGuid", new JsonString(portalGuid));
		joParams.setValue("modResourceGuid", new JsonString(modGuid));
		joParams.setValue("playerLocation", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		joParams.setValue("index", new JsonNumber(index));
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createSendMessageJSON(String message, boolean factionOnly) {
		JsonObject joInfo = new JsonObject();
		joInfo.setValue("factionOnly", factionOnly ? new JsonTrue() : new JsonFalse());
		joInfo.setValue("message", new JsonString(message));
		JsonObject jsonParams = new JsonObject();
		jsonParams.setValue("params", joInfo);
		return jsonParams;
	}

	static JsonValue createUpdateIngressPortalJSON(String guid, String title, String description) {
		JsonObject joInfo = new JsonObject();
		joInfo.setValue("curationType", new JsonString("PLAYER_EDIT"));
		joInfo.setValue("portalGuid", new JsonString(guid));
		if (title != null) {
			joInfo.setValue("title", new JsonString(title));
		}
		if (description != null) {
			joInfo.setValue("description", new JsonString(description));
		}
		JsonArray jaParams = new JsonArray();
		jaParams.addValue(joInfo);
		JsonObject jsonParams = new JsonObject();
		jsonParams.setValue("params", jaParams);
		return jsonParams;
	}

	static JsonValue createRechargeIngressPortalJSON(String guid, long latitude, long longitude, boolean boost, int index, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		joParams.setValue("isBoostRecharge", boost ? new JsonTrue() : new JsonFalse());
		joParams.setValue("location", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		joParams.setValue("portalGuid", new JsonString(guid));
		JsonArray jaResonatorSlots = new JsonArray();
		if (index < 0 || index > 7) {
			for (int i = 0; i < 8; i++) {
				jaResonatorSlots.addValue(new JsonNumber(i));
			}
		} else {
			jaResonatorSlots.addValue(new JsonNumber(index));
		}
		joParams.setValue("resonatorSlots", jaResonatorSlots);
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createRemoteRechargeIngressPortalJSON(String guid, long latitude, long longitude, boolean boost, int index, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		joParams.setValue("isBoostRecharge", boost ? new JsonTrue() : new JsonFalse());
		joParams.setValue("location", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		joParams.setValue("portalKeyGuid", new JsonString(guid));
		JsonArray jaResonatorSlots = new JsonArray();
		if (index < 0 || index > 7) {
			for (int i = 0; i < 8; i++) {
				jaResonatorSlots.addValue(new JsonNumber(i));
			}
		} else {
			jaResonatorSlots.addValue(new JsonNumber(index));
		}
		joParams.setValue("resonatorSlots", jaResonatorSlots);
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createHackIngressPortalJSON(String guid, long latitude, long longitude, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		joParams.setValue("portalGuid", new JsonString(guid));
		joParams.setValue("location", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		joParams.setValue("glyphGameRequested", new JsonFalse());
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createHackIngressPortalNoKeyJSON(String guid, long latitude, long longitude, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		joParams.setValue("portalGuid", new JsonString(guid));
		joParams.setValue("location", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		joParams.setValue("glyphGameRequested", new JsonFalse());
		JsonArray jaGlyphSequence = new JsonArray();
		JsonObject joGlyphOrder = new JsonObject();
		joGlyphOrder.setValue("glyphOrder", new JsonString("NOKEY"));
		jaGlyphSequence.addValue(joGlyphOrder);
		JsonObject joSpewInfluenceGlyphSequence = new JsonObject();
		joSpewInfluenceGlyphSequence.setValue("bypassed", new JsonFalse());
		joSpewInfluenceGlyphSequence.setValue("glyphSequence", jaGlyphSequence);
		joSpewInfluenceGlyphSequence.setValue("inputTimeMs", new JsonNumber(0));
		joParams.setValue("spewInfluenceGlyphSequence", joSpewInfluenceGlyphSequence);
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createHackIngressPortalWithGlyphsJSON(String guid, long latitude, long longitude, String[] glyphs, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		joParams.setValue("portalGuid", new JsonString(guid));
		joParams.setValue("location", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		joParams.setValue("glyphGameRequested", new JsonFalse());
		JsonArray jaGlyphSequence = new JsonArray();
		for (String glyph : glyphs) {
			JsonObject joGlyphOrder = new JsonObject();
			joGlyphOrder.setValue("glyphOrder", new JsonString(glyph));
			jaGlyphSequence.addValue(joGlyphOrder);
		}
		JsonObject joSpewInfluenceGlyphSequence = new JsonObject();
		joSpewInfluenceGlyphSequence.setValue("bypassed", new JsonFalse());
		joSpewInfluenceGlyphSequence.setValue("glyphSequence", jaGlyphSequence);
		joSpewInfluenceGlyphSequence.setValue("inputTimeMs", new JsonNumber(0));
		joParams.setValue("userInputGlyphSequence", joSpewInfluenceGlyphSequence);
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createRequestGlyphIngressPortalJSON(String guid, long latitude, long longitude, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		joParams.setValue("portalGuid", new JsonString(guid));
		joParams.setValue("location", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		joParams.setValue("glyphGameRequested", new JsonTrue());
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createRecycleIngressInventoriesJSON(String[] guids, long latitude, long longitude, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		JsonArray jaItemGuids = new JsonArray();
		for (String guid : guids) {
			jaItemGuids.addValue(new JsonString(guid));
		}
		joParams.setValue("itemGuids", jaItemGuids);
		joParams.setValue("playerLocation", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createUseIngressPowerCubeJSON(String guid, long latitude, long longitude, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		joParams.setValue("itemGuid", new JsonString(guid));
		joParams.setValue("playerLocation", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createLinkIngressPortal(String guidFrom, long latitude, long longitude, String guidTo, String keyGuid, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		joParams.setValue("originPortalGuid", new JsonString(guidFrom));
		joParams.setValue("destinationPortalGuid", new JsonString(guidTo));
		joParams.setValue("linkKeyGuid", new JsonString(keyGuid));
		joParams.setValue("playerLocation", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createDeployIngressResonatorJSON(String portalGuid, long latitude, long longitude, String resonatorGuid, int index, String clientBlob) {
		if (index < 0 || index > 7) {
			index = 255;
		}
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		JsonArray jaItemGuids = new JsonArray();
		jaItemGuids.addValue(new JsonString(resonatorGuid));
		joParams.setValue("itemGuids", jaItemGuids);
		joParams.setValue("portalGuid", new JsonString(portalGuid));
		joParams.setValue("location", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		joParams.setValue("preferredSlot", new JsonNumber(index));
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createUpgradeIngressResonatorJSON(String portalGuid, long latitude, long longitude, String resonatorGuid, int index, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		joParams.setValue("emitterGuid", new JsonString(resonatorGuid));
		joParams.setValue("portalGuid", new JsonString(portalGuid));
		joParams.setValue("location", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		joParams.setValue("resonatorSlotToUpgrade", new JsonNumber(index));
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createRedeemRewardJSON(String passcode) {
		JsonArray jsonArray = new JsonArray();
		jsonArray.addValue(new JsonString(passcode));
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", jsonArray);
		return jsonObject;
	}

	static JsonValue createFlipIngressPortalJSON(String portalGuid, long latitude, long longitude, String flipGuid, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		joParams.setValue("portalGuid", new JsonString(portalGuid));
		joParams.setValue("resourceGuid", new JsonString(flipGuid));
		joParams.setValue("location", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createLoadIngressStorableInventoryToIngressCapsuleJSON(String capsuleGuid, String[] storableGuids, long latitude, long longitude, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		joParams.setValue("containerGuid", new JsonString(capsuleGuid));
		JsonArray jaLoadGuids = new JsonArray();
		JsonArray jaUnloadGuids = new JsonArray();
		for (String storableGuid : storableGuids) {
			jaLoadGuids.addValue(new JsonString(storableGuid));
		}
		joParams.setValue("inventoryGuidsToLoad", jaLoadGuids);
		joParams.setValue("containerGuidsToUnload", jaUnloadGuids);
		joParams.setValue("playerLocation", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createUnloadIngressStorableInventoryFromIngressCapsuleJSON(String capsuleGuid, String[] storableGuids, long latitude, long longitude, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		joParams.setValue("containerGuid", new JsonString(capsuleGuid));
		JsonArray jaLoadGuids = new JsonArray();
		JsonArray jaUnloadGuids = new JsonArray();
		joParams.setValue("inventoryGuidsToLoad", jaLoadGuids);
		for (String storableGuid : storableGuids) {
			jaUnloadGuids.addValue(new JsonString(storableGuid));
		}
		joParams.setValue("containerGuidsToUnload", jaUnloadGuids);
		joParams.setValue("playerLocation", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createDropIngressInventoryJSON(String guid, long latitude, long longitude, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		joParams.setValue("itemGuid", new JsonString(guid));
		joParams.setValue("playerLocation", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		System.out.println(jsonObject.toString(true));
		return jsonObject;
	}

	static JsonValue createPickIngressInventoryJSON(String guid, long latitude, long longitude, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		joParams.setValue("itemGuid", new JsonString(guid));
		joParams.setValue("playerLocation", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createSendInvitationJSON(String email, String message) {
		JsonObject joParams = new JsonObject();
		joParams.setValue("inviteeEmailAddress", new JsonString(email));
		joParams.setValue("customMessage", new JsonString(message));
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createStartIngressMissionJSON(String guid, long latitude, long longitude, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		joParams.setValue("value", new JsonString(guid));
		joParams.setValue("playerLocation", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createFireIngressWeaponJSON(String guid, long latitude, long longitude, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		joParams.setValue("encodedBoost", new JsonString(""));
		joParams.setValue("itemGuid", new JsonString(guid));
		joParams.setValue("playerLocation", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createRateIngressMissionJSON(String guid, long latitude, long longitude, boolean yes, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		joParams.setValue("missionGuid", new JsonString(guid));
		joParams.setValue("location", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		joParams.setValue("rating", new JsonNumber(yes ? 1000000 : 0));
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createEnterPassphraseJSON(String guid, long latitude, long longitude, int index, String passphrase, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		joParams.setValue("missionGuid", new JsonString(guid));
		joParams.setValue("location", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		joParams.setValue("waypointIndex", new JsonNumber(index));
		joParams.setValue("passphraseAnswer", new JsonString(passphrase));
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createViewTripJSON(String guid, long latitude, long longitude, String missionGuid, String clientBlob) {
		JsonObject joParams = new JsonObject();
		JsonObject joClientBasket = new JsonObject();
		joClientBasket.setValue("clientBlob", new JsonString(clientBlob));
		joParams.setValue("clientBasket", joClientBasket);
		joParams.setValue("missionGuid", new JsonString(missionGuid));
		joParams.setValue("location", new JsonString(String.format("%08x,%08x", latitude, longitude)));
		joParams.setValue("fieldTripCardGuid", new JsonString(guid));
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", joParams);
		return jsonObject;
	}

	static JsonValue createSetAgentProfileVisibilityJSON(boolean enabled) {
		JsonArray jaParams = new JsonArray();
		JsonObject joParam1 = new JsonObject();
		joParam1.setValue("areMetricsPublic", new JsonBoolean(enabled));
		jaParams.addValue(joParam1);
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", jaParams);
		return jsonObject;
	}

	static JsonValue createSetNotificationJSON(Boolean retrieveNoticationEmail, Boolean retrievePromotionEmail, Boolean retrievePlayerMessage, Boolean retrievePortalAttack, Boolean retrieveInvitation, Boolean retrieveNews) {
		JsonObject joParams = new JsonObject();
		JsonObject joNotificationSettings = new JsonObject();
		joNotificationSettings.setValue("locale", new JsonString("en_US"));
		if (retrieveNoticationEmail != null) {
			joNotificationSettings.setValue("shouldSendEmail", new JsonBoolean(retrieveNoticationEmail));
		}
		if (retrievePromotionEmail != null) {
			joNotificationSettings.setValue("maySendPromoEmail", new JsonBoolean(retrievePromotionEmail));
		}
		if (retrievePlayerMessage != null) {
			joNotificationSettings.setValue("shouldPushNotifyForAtPlayer", new JsonBoolean(retrievePlayerMessage));
		}
		if (retrievePortalAttack != null) {
			joNotificationSettings.setValue("shouldPushNotifyForPortalAttacks", new JsonBoolean(retrievePortalAttack));
		}
		if (retrieveInvitation != null) {
			joNotificationSettings.setValue("shouldPushNotifyForInvitesAndFactionInfo", new JsonBoolean(retrieveInvitation));
		}
		if (retrieveNews != null) {
			joNotificationSettings.setValue("shouldPushNotifyForNewsOfTheDay", new JsonBoolean(retrieveNews));
		}
		JsonObject joParam1 = new JsonObject();
		joParam1.setValue("clientBasket", new JsonObject());
		joParam1.setValue("notificationSettings", joNotificationSettings);
		JsonArray jaParams = new JsonArray();
		jaParams.addValue(joParam1);
		joParams.setValue("params", jaParams);
		return joParams;
	}
	////////// static method end //////////

	private final String sacSid;
	private final String token;
	private final String clientBlob;

	public IngressApp(String sacSid, String token, String clientBlob) {
		this.sacSid = sacSid;
		this.token = token;
		this.clientBlob = clientBlob;
	}

	String getSacSid() {
		return this.sacSid;
	}

	String getToken() {
		return this.token;
	}

	String getClientBlob() {
		return this.clientBlob;
	}

	public IngressScore getIngressScore(long latitude, long longitude) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/playerUndecorated/getRegionScoreByLocation", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createGetIngressScoreJSON(latitude, longitude).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = ((JsonObject) jsonValue);
		JsonValue jvResult = jsonObject.getValue("result");
		JsonObject joResult = ((JsonObject) jvResult);
		JsonValue jvCard = joResult.getValue("card");
		JsonObject joCard = ((JsonObject) jvCard);
		JsonValue jvCurrentScore = joCard.getValue("currentScore");
		JsonObject joCurrentScore = ((JsonObject) jvCurrentScore);
		JsonValue jvEnlightenedScore = joCurrentScore.getValue("alienScore");
		long enlightenedScore = Long.parseLong(((JsonString) jvEnlightenedScore).getValue());
		JsonValue jvResistanceScore = joCurrentScore.getValue("resistanceScore");
		long resistanceScore = Long.parseLong(((JsonString) jvResistanceScore).getValue());
		String regionName = ((JsonString) jvCard).getValue();
		return new IngressScore(regionName, enlightenedScore, resistanceScore);
	}

	private IngressScore getIngressScore() throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/playerUndecorated/getGameScore", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createGetIngressScoreJSON(null, null).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = ((JsonObject) jsonValue);
		JsonValue jvResult = jsonObject.getValue("result");
		JsonObject joResult = ((JsonObject) jvResult);
		JsonValue jvEnlightenedScore = joResult.getValue("alienScore");
		long enlightenedScore = Long.parseLong(((JsonString) jvEnlightenedScore).getValue());
		JsonValue jvResistanceScore = joResult.getValue("resistanceScore");
		long resistanceScore = Long.parseLong(((JsonString) jvResistanceScore).getValue());
		return new IngressScore(null, enlightenedScore, resistanceScore);
	}

	public IngressAgent getIngressAgent(String code) throws IOException {
		return this.getIngressAgent(null, code, false);
	}

	public IngressAgent getIngressAgentByGuid(String guid) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/playerUndecorated/getNickNamesFromPlayerIds", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createGetIngressAgentByGuidJSON(guid).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = ((JsonObject) jsonValue);
		JsonValue jvResult = jsonObject.getValue("result");
		JsonArray jaResult = ((JsonArray) jvResult);
		String code = ((JsonString) jaResult.getValue(0)).getValue();
		return this.getIngressAgent(guid, code, false);
	}

	public IngressAgent getIngressAgent(String guid, String code, boolean showAllMissionAchievements) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/playerUndecorated/getPlayerProfile", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createGetIngressAgentJSON(code).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		Map<String, Long> statistics = IngressAgent.initialStatistics();
		Map<String, String> standardAchievements = IngressAgent.initialStandardAchievements();
		ArrayList<String> otherAchievements = new ArrayList<>();
		ArrayList<String> missionAchievements = new ArrayList<>();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = ((JsonObject) jsonValue);
		JsonValue jvResult = jsonObject.getValue("result");
		JsonObject joResult = ((JsonObject) jvResult);
		String team = ((JsonString) joResult.getValue("team")).getValue();
		String faction = ((team.equals("RESISTANCE")) ? "Resistance" : "Enlightened");
		int level = ((JsonNumber) joResult.getValue("verifiedLevel")).getValue().intValue();
		long ap = Long.parseLong(((JsonString) joResult.getValue("ap")).getValue());
		JsonValue jvMetrics = joResult.getValue("metrics");
		JsonArray jaMetrics = ((JsonArray) jvMetrics);
		for (JsonValue jvMetric : jaMetrics.getValue()) {
			JsonObject joMetric = ((JsonObject) jvMetric);
			String name = ((JsonString) joMetric.getValue("metricName")).getValue();
			if (statistics.containsKey(name)) {
				String formattedValue = null;
				try {
					formattedValue = ((JsonString) joMetric.getValue("formattedValueAllTime")).getValue();
				} catch (Exception ex) {
					formattedValue = ((JsonString) joMetric.getValue("formattedValueNow")).getValue();
				}
				if (formattedValue != null) {
					String[] fields = formattedValue.replaceAll(",", "").split(" ");
					long value = Long.parseLong(fields[0]);
					statistics.put(name, value);
				}
			}
		}
		JsonValue jvHighlightedAchievements = joResult.getValue("highlightedAchievements");
		JsonArray jaHighlightedAchievements = ((JsonArray) jvHighlightedAchievements);
		for (JsonValue jvHighlightedAchievement : jaHighlightedAchievements.getValue()) {
			JsonObject joHighlightedAchievement = ((JsonObject) jvHighlightedAchievement);
			String name = ((JsonString) joHighlightedAchievement.getValue("title")).getValue();
			if (standardAchievements.containsKey(name)) {
				JsonValue jvTiers = joHighlightedAchievement.getValue("tiers");
				JsonArray jaTiers = ((JsonArray) jvTiers);
				int rank = 0;
				for (JsonValue jvTier : jaTiers.getValue()) {
					JsonObject joTier = ((JsonObject) jvTier);
					boolean locked = ((JsonBoolean) joTier.getValue("locked")).getValue();
					if (!locked) {
						rank++;
					}
				}
				standardAchievements.put(name, IngressAgent.STANDARD_ACHIEVEMENT_LEVELS[rank]);
			} else {
				otherAchievements.add(name);
			}
		}
		JsonValue jvHighlightedMissionBadges = joResult.getValue("highlightedMissionBadges");
		JsonArray jaHighlightedMissionBadges = ((JsonArray) jvHighlightedMissionBadges);
		for (JsonValue jvHighlightedMissionBadge : jaHighlightedMissionBadges.getValue()) {
			JsonObject joHighlightedMissionBadge = ((JsonObject) jvHighlightedMissionBadge);
			String name = ((JsonString) joHighlightedMissionBadge.getValue("missionTitle")).getValue();
			missionAchievements.add(name);
		}
		if (showAllMissionAchievements) {
			JsonValue jvFirstMissionBadgeContinuationToken = joResult.getValue("firstMissionBadgeContinuationToken");
			if (jvFirstMissionBadgeContinuationToken != null) {
				String continuationToken = ((JsonString) jvFirstMissionBadgeContinuationToken).getValue();
				getMissionAchievements(missionAchievements, continuationToken);
			}
		}
		return new IngressAgent(guid, code, faction, level, ap, statistics, standardAchievements, otherAchievements.toArray(new String[otherAchievements.size()]), missionAchievements.toArray(new String[missionAchievements.size()]));
	}

	private void getMissionAchievements(ArrayList<String> missionAchievements, String continuationToken) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/playerUndecorated/getPaginatedMissionBadges", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.sacSid, this.token);
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressAgent.createGetMissionAchievementsJSON(continuationToken).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = ((JsonObject) jsonValue);
		JsonValue jvResult = jsonObject.getValue("result");
		JsonObject joResult = ((JsonObject) jvResult);
		JsonValue jvMissionBadges = joResult.getValue("missionBadges");
		JsonArray jaMissionBadges = ((JsonArray) jvMissionBadges);
		for (JsonValue jvMissionBadge : jaMissionBadges.getValue()) {
			JsonObject joMissionBadge = ((JsonObject) jvMissionBadge);
			String name = ((JsonString) joMissionBadge.getValue("missionTitle")).getValue();
			missionAchievements.add(name);
		}
		JsonValue jvContinuationToken = joResult.getValue("continuationToken");
		if (jvContinuationToken != null) {
			continuationToken = ((JsonString) jvContinuationToken).getValue();
			this.getMissionAchievements(missionAchievements, continuationToken);
		}
	}

	public IngressInvitationInfo getIngressInvitationInfo() throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/playerUndecorated/getInviteInfo", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createGetIngressInvitationInfoJSON().toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		ArrayList<IngressRecruit> ingressRecruits = new ArrayList<>();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = ((JsonObject) jsonValue);
		JsonValue jvResult = jsonObject.getValue("result");
		JsonObject joResult = ((JsonObject) jvResult);
		int availableInvitations = ((JsonNumber) joResult.getValue("numAvailableInvites")).getValue().intValue();
		JsonValue jvInvitations = joResult.getValue("inviteeToStatusMap");
		JsonObject joInvitations = ((JsonObject) jvInvitations);
		for (String email : joInvitations.getValue().keySet()) {
			String status = ((JsonString) joInvitations.getValue().get(email)).getValue();
			ingressRecruits.add(new IngressRecruit(email, status));
		}
		return new IngressInvitationInfo(availableInvitations, ingressRecruits.toArray(new IngressRecruit[ingressRecruits.size()]));
	}

	public IngressInventoryContainer getIngressInventoryContainer() throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/playerUndecorated/getInventory", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createGetIngressInventoryContainerJSON().toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		IngressAgent owner = null;
		ArrayList<IngressInventory> ingressInventories = new ArrayList<>();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = ((JsonObject) jsonValue);
		JsonValue jvGameBasket = jsonObject.getValue("gameBasket");
		JsonObject joGameBasket = ((JsonObject) jvGameBasket);
		JsonValue jvInventories = joGameBasket.getValue("inventory");
		JsonArray jaInventories = ((JsonArray) jvInventories);
		for (JsonValue jvInventory : jaInventories.getValue()) {
			JsonArray jaInventory = ((JsonArray) jvInventory);
			String guid = ((JsonString) jaInventory.getValue(0)).getValue();
			JsonValue jvInventoryInfo = jaInventory.getValue(2);
			JsonObject joInventoryInfo = ((JsonObject) jvInventoryInfo);
			if (owner == null) {
				JsonValue jvInInventory = joInventoryInfo.getValue("inInventory");
				JsonObject joInInventory = ((JsonObject) jvInInventory);
				String ownerGuid = ((JsonString) joInInventory.getValue("playerId")).getValue();
				owner = this.getIngressAgentByGuid(ownerGuid);
			}
			ingressInventories.add(IngressInventoryContainer.parseIngressInventory(guid, owner, jvInventoryInfo));
		}
		return new IngressInventoryContainer(ingressInventories.toArray(new IngressInventory[ingressInventories.size()]));
	}

	public IngressPortal getIngressPortal(String guid) throws IOException {
		return this.getIngressPortal(guid, 0L, 0L);
	}

	public IngressPortal getIngressPortal(String guid, long latitude, long longitude) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/gameplay/getModifiedEntitiesByGuid", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createGetIngressPortalJSON(guid, latitude, longitude, this.getClientBlob()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		Map<String, IngressAgent> ingressAgents = new LinkedHashMap<>();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = ((JsonObject) jsonValue);
		JsonValue jvGameBasket = jsonObject.getValue("gameBasket");
		JsonObject joGameBasket = ((JsonObject) jvGameBasket);
		JsonValue jvGameEntities = joGameBasket.getValue("gameEntities");
		JsonArray jaGameEntities = ((JsonArray) jvGameEntities);
		JsonValue jvGameEntitiy = jaGameEntities.getValue(0);
		JsonArray jaGameEntitiy = ((JsonArray) jvGameEntitiy);
		JsonValue jvInfo = jaGameEntitiy.getValue(2);
		JsonObject joInfo = ((JsonObject) jvInfo);
		JsonValue jvLocationE6 = ((JsonObject) joInfo).getValue("locationE6");
		JsonObject joLocationE6 = ((JsonObject) jvLocationE6);
		latitude = ((JsonNumber) joLocationE6.getValue("latE6")).getValue().longValue();
		longitude = ((JsonNumber) joLocationE6.getValue("lngE6")).getValue().longValue();
		String capturerGuid = null;
		try {
			capturerGuid = ((JsonString) ((JsonObject) ((JsonObject) joInfo).getValue("captured")).getValue("capturingPlayerId")).getValue();
			if (!ingressAgents.containsKey(capturerGuid)) {
				ingressAgents.put(capturerGuid, this.getIngressAgentByGuid(capturerGuid));
			}
		} catch (Exception ex) {
		}
		String faction = ((JsonString) ((JsonObject) ((JsonObject) joInfo).getValue("controllingTeam")).getValue("team")).getValue();
		JsonValue jvPhotoStreamInfo = joInfo.getValue("photoStreamInfo");
		JsonObject joPhotoStreamInfo = ((JsonObject) jvPhotoStreamInfo);
		JsonValue jvCoverPhoto = joPhotoStreamInfo.getValue("coverPhoto");
		JsonObject joCoverPhoto = ((JsonObject) jvCoverPhoto);
		String imageGuid = ((JsonString) joCoverPhoto.getValue("portalImageGuid")).getValue();
		String imageURL = ((JsonString) joCoverPhoto.getValue("imageUrl")).getValue();
		JsonValue jvAttributionMarkup = joCoverPhoto.getValue("attributionMarkup");
		JsonArray jaAttributionMarkup = ((JsonArray) jvAttributionMarkup);
		String takerGuid = null;
		try {
			JsonValue jvTakerInfo = jaAttributionMarkup.getValue(1);
			JsonObject joTakerInfo = ((JsonObject) jvTakerInfo);
			takerGuid = ((JsonString) joTakerInfo.getValue("guid")).getValue();
			if (!ingressAgents.containsKey(takerGuid)) {
				ingressAgents.put(takerGuid, this.getIngressAgentByGuid(takerGuid));
			}
		} catch (Exception ex) {
		}
		IngressImage cover = new IngressImage(imageGuid, ingressAgents.get(takerGuid), new URL(imageURL));
		JsonValue jvDescriptiveText = ((JsonObject) joInfo).getValue("descriptiveText");
		JsonObject joDescriptiveText = ((JsonObject) jvDescriptiveText);
		JsonValue jvMap = ((JsonObject) joDescriptiveText).getValue("map");
		JsonObject joMap = ((JsonObject) jvMap);
		String address = ((JsonString) joMap.getValue("ADDRESS")).getValue();
		String title = ((JsonString) joMap.getValue("TITLE")).getValue();
		String description = "";
		try {
			description = ((JsonString) joMap.getValue("DESCRIPTION")).getValue();
		} catch (Exception ex) {
		}
		String discovererGuid = null;
		try {
			discovererGuid = ((JsonString) ((JsonObject) ((JsonObject) ((JsonObject) joInfo).getValue("discoverer")).getValue("playerMarkupArgSet")).getValue("guid")).getValue();
			if (!ingressAgents.containsKey(discovererGuid)) {
				ingressAgents.put(discovererGuid, this.getIngressAgentByGuid(discovererGuid));
			}
		} catch (Exception ex) {
		}
		JsonValue jvResonatorArray = ((JsonObject) joInfo).getValue("resonatorArray");
		JsonObject joResonatorArray = ((JsonObject) jvResonatorArray);
		JsonValue jvResonators = ((JsonObject) joResonatorArray).getValue("resonators");
		JsonArray jaResonators = ((JsonArray) jvResonators);
		IngressResonators[] ingressResonatorses = new IngressResonators[8];
		for (JsonValue jvResonator : jaResonators.getValue()) {
			if (!(jvResonator instanceof JsonNull)) {
				JsonObject joResonator = ((JsonObject) jvResonator);
				int slot = ((JsonNumber) joResonator.getValue("slot")).getValue().intValue();
				int level = ((JsonNumber) joResonator.getValue("level")).getValue().intValue();
				String deployerGuid = null;
				try {
					deployerGuid = ((JsonString) joResonator.getValue("ownerGuid")).getValue();
					if (!ingressAgents.containsKey(deployerGuid)) {
						ingressAgents.put(deployerGuid, this.getIngressAgentByGuid(deployerGuid));
					}
				} catch (Exception ex) {
				}
				int energy = ((JsonNumber) joResonator.getValue("energyTotal")).getValue().intValue();
				int distance = ((JsonNumber) joResonator.getValue("distanceToPortal")).getValue().intValue();
				ingressResonatorses[slot] = IngressResonators.createIngressResonators(guid, ingressAgents.get(deployerGuid), level, energy, distance);
			}
		}
		JsonValue jvPortalV2 = ((JsonObject) joInfo).getValue("portalV2");
		JsonObject joPortalV2 = ((JsonObject) jvPortalV2);
		JsonValue jvLinkedEdges = ((JsonObject) joPortalV2).getValue("linkedEdges");
		JsonArray jaLinkedEdges = ((JsonArray) jvLinkedEdges);
		ArrayList<IngressLink> ingressLinkInGuids = new ArrayList<>();
		ArrayList<IngressLink> ingressLinkOutGuids = new ArrayList<>();
		for (JsonValue jvLinkedEdge : jaLinkedEdges.getValue()) {
			JsonObject joLinkedEdge = ((JsonObject) jvLinkedEdge);
			String linkGuid = ((JsonString) joLinkedEdge.getValue("edgeGuid")).getValue();
			String portalGuid = ((JsonString) joLinkedEdge.getValue("otherPortalGuid")).getValue();
			boolean isOrigin = ((JsonBoolean) joLinkedEdge.getValue("isOrigin")).getValue();
			if (isOrigin) {
				ingressLinkInGuids.add(new IngressLink(linkGuid, portalGuid, guid));
			} else {
				ingressLinkOutGuids.add(new IngressLink(linkGuid, guid, portalGuid));
			}
		}
		IngressLink[] ingressLinksIn = ingressLinkInGuids.toArray(new IngressLink[ingressLinkInGuids.size()]);
		IngressLink[] ingressLinksOut = ingressLinkOutGuids.toArray(new IngressLink[ingressLinkOutGuids.size()]);
		JsonValue jvMods = ((JsonObject) jvPortalV2).getValue("linkedModArray");
		JsonArray jaMods = ((JsonArray) jvMods);
		IngressMods[] ingressMods = new IngressMods[4];
		for (int i = 0; i < ingressMods.length; i++) {
			JsonValue jvMod = jaMods.getValue(i);
			if (!(jvMod instanceof JsonNull)) {
				JsonObject joMod = ((JsonObject) jvMod);
				String installerGuid = null;
				try {
					installerGuid = ((JsonString) joMod.getValue("installingUser")).getValue();
					if (!ingressAgents.containsKey(installerGuid)) {
						ingressAgents.put(installerGuid, this.getIngressAgentByGuid(installerGuid));
					}
				} catch (Exception ex) {
				}
				ingressMods[i] = IngressInventoryContainer.parseIngressMods(guid, ingressAgents.get(installerGuid), jvMod);
			}
		}
		return new IngressPortal(guid, ingressAgents.get(capturerGuid), latitude, longitude, address, title, description, -1, -1, faction, ingressAgents.get(discovererGuid), cover, ingressResonatorses, ingressMods, ingressLinksIn, ingressLinksOut);
	}

	public IngressMission getIngressMission(String guid, long latitude, long longitude) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/gameplay/getMissionDetails", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createGetIngressMissionJSON(guid, latitude, longitude, this.getClientBlob()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		ArrayList<IngressWaypoint> ingressWaypoints = new ArrayList<>();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = ((JsonObject) jsonValue);
		JsonValue jvResult = jsonObject.getValue("result");
		JsonObject joResult = ((JsonObject) jvResult);
		JsonValue jvHeader = joResult.getValue("header");
		JsonObject joHeader = ((JsonObject) jvHeader);
		String title = ((JsonString) joHeader.getValue("title")).getValue();
		URL logoURL = new URL(((JsonString) joHeader.getValue("logoUrl")).getValue());
		URL achievementURL = new URL(((JsonString) joHeader.getValue("badgeUrl")).getValue());
		String ownerGuid = ((JsonString) joHeader.getValue("authorNickname")).getValue();
		IngressAgent ingressAgent = null;
		try {
			this.getIngressAgentByGuid(ownerGuid);
		} catch (Exception ex) {
		}
		String description = ((JsonString) joResult.getValue("description")).getValue();
		String type = ((JsonString) joResult.getValue("type")).getValue();
		JsonArray jaWaypoints = ((JsonArray) joResult.getValue("waypoints"));
		for (JsonValue jvWaypoint : jaWaypoints.getValue()) {
			JsonObject joWaypoint = ((JsonObject) jvWaypoint);
			String portalGuid = ((JsonString) joWaypoint.getValue("referenceGuid")).getValue();
			JsonObject joObjective = ((JsonObject) joWaypoint.getValue("objective"));
			String waypointType = ((JsonString) joObjective.getValue("objectiveType")).getValue();
			String question = null;
			if (waypointType.equals(IngressWaypoint.WAYPOINT_TYPE_ENTER_THE_PASSPHRASE)) {
				JsonObject joPassphraseParams = ((JsonObject) joObjective.getValue("passphraseParams"));
				question = ((JsonString) joPassphraseParams.getValue("question")).getValue();
			}
			ingressWaypoints.add(new IngressWaypoint(this.getIngressPortal(portalGuid), waypointType, question));
		}
		return new IngressMission(guid, ingressAgent, title, description, type, logoURL, achievementURL, ingressWaypoints.toArray(new IngressWaypoint[ingressWaypoints.size()]));
	}

	public IngressMission[] getIngressMissionsByIngressPortalGuid(String guid, long latitude, long longitude) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/gameplay/getTopMissionsForPortal", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createGetIngressMissionsByIngressPortalGuidJSON(guid, latitude, longitude, this.getClientBlob()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		ArrayList<IngressMission> ingressMissions = new ArrayList<>();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = ((JsonObject) jsonValue);
		JsonValue jvResult = jsonObject.getValue("result");
		JsonObject joResult = ((JsonObject) jvResult);
		JsonValue jvMissionSnippets = joResult.getValue("missionSnippets");
		JsonArray jaMissionSnippets = ((JsonArray) jvMissionSnippets);
		for (JsonValue jvMissionSnippet : jaMissionSnippets.getValue()) {
			JsonObject joMissionSnippet = ((JsonObject) jvMissionSnippet);
			JsonValue jvHeader = joMissionSnippet.getValue("header");
			JsonObject joHeader = ((JsonObject) jvHeader);
			String missionGuid = ((JsonString) joHeader.getValue("guid")).getValue();
			ingressMissions.add(this.getIngressMission(missionGuid, latitude, longitude));
		}
		return ingressMissions.toArray(new IngressMission[ingressMissions.size()]);
	}

	public IngressMission[] getIngressMissionsByLocation(long latitude, long longitude) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/gameplay/getNearbyMissions", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createGetIngressMissionsByLocationJSON(latitude, longitude, this.getClientBlob()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		ArrayList<IngressMission> ingressMissions = new ArrayList<>();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = ((JsonObject) jsonValue);
		JsonValue jvResult = jsonObject.getValue("result");
		JsonObject joResult = ((JsonObject) jvResult);
		JsonValue jvMissionSnippets = joResult.getValue("missionSnippets");
		JsonArray jaMissionSnippets = ((JsonArray) jvMissionSnippets);
		for (JsonValue jvMissionSnippet : jaMissionSnippets.getValue()) {
			JsonObject joMissionSnippet = ((JsonObject) jvMissionSnippet);
			JsonValue jvHeader = joMissionSnippet.getValue("header");
			JsonObject joHeader = ((JsonObject) jvHeader);
			String missionGuid = ((JsonString) joHeader.getValue("guid")).getValue();
			ingressMissions.add(this.getIngressMission(missionGuid, latitude, longitude));
		}
		return ingressMissions.toArray(new IngressMission[ingressMissions.size()]);
	}

	public IngressImage[] getIngressImages(String ingressPortalGuid) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/geoInfo/getPortalImages", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createGetIngressImageJSON(ingressPortalGuid).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		ArrayList<IngressImage> ingressImages = new ArrayList<>();
		Map<String, IngressAgent> ingressAgents = new LinkedHashMap<>();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = ((JsonObject) jsonValue);
		JsonValue jvResult = jsonObject.getValue("result");
		JsonObject joResult = ((JsonObject) jvResult);
		JsonValue jvPortalImages = joResult.getValue("portalImages");
		JsonArray jaPortalImages = ((JsonArray) jvPortalImages);
		for (JsonValue jvPortalImage : jaPortalImages.getValue()) {
			JsonObject joPortalImage = ((JsonObject) jvPortalImage);
			String guid = ((JsonString) joPortalImage.getValue("portalImageGuid")).getValue();
			String imageURL = ((JsonString) joPortalImage.getValue("imageUrl")).getValue();
			JsonValue jvAttributionMarkup = joPortalImage.getValue("attributionMarkup");
			JsonArray jaAttributionMarkup = ((JsonArray) jvAttributionMarkup);
			JsonValue jvTakerInfo = jaAttributionMarkup.getValue(1);
			JsonObject joTakerInfo = ((JsonObject) jvTakerInfo);
			String takerGuid = ((JsonString) joTakerInfo.getValue("guid")).getValue();
			if (!ingressAgents.containsKey(takerGuid)) {
				ingressAgents.put(takerGuid, this.getIngressAgentByGuid(takerGuid));
			}
			ingressImages.add(new IngressImage(guid, ingressAgents.get(takerGuid), new URL(imageURL)));
		}
		return ingressImages.toArray(new IngressImage[ingressImages.size()]);
	}
	/*
	 public void getIngressLogs(long latitude, long longitude) throws IOException {
	 URL url = new URL("https://m-dot-betaspike.appspot.com/rpc/gameplay/getPaginatedPlexts");
	 HttpRequest httpRequest = new HttpRequest(url);
	 httpRequest.setMethod(HttpRequest.METHOD_POST);
	 httpRequest.setContentType(HttpRequest.CONTENT_TYPE_JSON);
	 DuplicatePairList<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
	 httpRequest.setRequestHeaders(requestHeaders);
	 String requestData = IngressApp.createGetIngressLogsJSON(latitude, longitude, this.getClientBlob()).toString();
	 httpRequest.setRequestData(requestData);
	 httpRequest.send();
	 String responseString = httpRequest.getResponseString();
	 JsonValue jsonValue = JsonParser.parse(responseString);
	 System.out.println(jsonValue.toString(true));
	 }
	 */

	public void installIngressMod(String ingressPortalGuid, long latitude, long longitude, String ingressModsGuid, int index) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/gameplay/addMod", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createInstallIngressModJSON(ingressPortalGuid, latitude, longitude, ingressModsGuid, index, this.getClientBlob()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = (JsonObject) jsonValue;
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		} else {
			System.out.println(jsonValue.toString(true));
		}
	}

	public void sendMessage(String message) throws IOException {
		this.sendMessage(message, true);
	}

	public void sendMessage(String message, boolean factionOnly) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/player/say", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createSendMessageJSON(message, factionOnly).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = (JsonObject) jsonValue;
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		} else {
			System.out.println(jsonValue.toString(true));
		}
	}

	public void updateIngressPortalTitle(String guid, String title) throws IOException {
		this.updateIngressPortal(guid, title, null);
	}

	public void updateIngressPortalDescription(String guid, String description) throws IOException {
		this.updateIngressPortal(guid, null, description);
	}

	public void updateIngressPortal(String guid, String title, String description) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/playerUndecorated/setPortalDetailsForCuration", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createUpdateIngressPortalJSON(guid, title, description).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = (JsonObject) jsonValue;
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		} else {
			System.out.println(jsonValue.toString(true));
		}
	}

	public void rechargeIngressPortal(String guid, long latitude, long longitude, boolean boost, int index) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/gameplay/rechargeResonatorsV2", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createRechargeIngressPortalJSON(guid, latitude, longitude, boost, index, this.getClientBlob()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = (JsonObject) jsonValue;
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		} else {
			System.out.println(jsonValue.toString(true));
		}
	}

	public void remoteRechargeIngressPortal(String guid, long latitude, long longitude, boolean boost, int index) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/gameplay/remoteRechargeResonatorsV2", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createRemoteRechargeIngressPortalJSON(guid, latitude, longitude, boost, index, this.getClientBlob()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = (JsonObject) jsonValue;
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		} else {
			System.out.println(jsonValue.toString(true));
		}
	}

	public IngressHackResponse hackIngressPortal(String guid, long latitude, long longitude) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/gameplay/collectItemsOrGlyphsFromPortal", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createHackIngressPortalJSON(guid, latitude, longitude, this.getClientBlob()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = ((JsonObject) jsonValue);
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		} else {
			JsonValue jvGameBasket = jsonObject.getValue("gameBasket");
			JsonObject joGameBasket = ((JsonObject) jvGameBasket);
			JsonValue jvPlayerDamages = joGameBasket.getValue("playerDamages");
			JsonArray jaPlayerDamages = ((JsonArray) jvPlayerDamages);
			long damage = 0;
			for (JsonValue jvPlayerDamage : jaPlayerDamages.getValue()) {
				JsonObject joPlayerDamage = ((JsonObject) jvPlayerDamage);
				damage += Long.parseLong(((JsonString) joPlayerDamage.getValue("damageAmount")).getValue());
			}
			JsonValue jvPlayerEntity = joGameBasket.getValue("playerEntity");
			JsonArray jaPlayerEntity = ((JsonArray) jvPlayerEntity);
			String ownerGuid = ((JsonString) jaPlayerEntity.getValue(0)).getValue();
			IngressAgent owner = this.getIngressAgentByGuid(ownerGuid);
			JsonValue jvPlayerInfo = jaPlayerEntity.getValue(2);
			JsonObject joPlayerInfo = ((JsonObject) jvPlayerInfo);
			JsonValue jvPlayerPersonal = joPlayerInfo.getValue("playerPersonal");
			JsonObject joPlayerPersonal = ((JsonObject) jvPlayerPersonal);
			int remainedEnergy = ((JsonNumber) joPlayerPersonal.getValue("energy")).getValue().intValue();
			String status = ((JsonString) joPlayerPersonal.getValue("energyState")).getValue();
			JsonValue jvInventories = joGameBasket.getValue("inventory");
			ArrayList<IngressInventory> ingressInventories = new ArrayList<>();
			if (jvInventories != null) {
				JsonArray jaInventories = ((JsonArray) jvInventories);
				for (JsonValue jvInventory : jaInventories.getValue()) {
					JsonArray jaInventory = ((JsonArray) jvInventory);
					String inventoryGuid = ((JsonString) jaInventory.getValue(0)).getValue();
					JsonValue jvInventoryInfo = jaInventory.getValue(2);
					ingressInventories.add(IngressInventoryContainer.parseIngressInventory(inventoryGuid, owner, jvInventoryInfo));
				}
			}
			return new IngressHackResponse(damage, remainedEnergy, status, ingressInventories.toArray(new IngressInventory[ingressInventories.size()]));
		}
	}

	public IngressHackResponse hackIngressPortalNoKey(String guid, long latitude, long longitude) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/gameplay/collectItemsOrGlyphsFromPortal", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createHackIngressPortalNoKeyJSON(guid, latitude, longitude, this.getClientBlob()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = ((JsonObject) jsonValue);
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		} else {
			JsonValue jvGameBasket = jsonObject.getValue("gameBasket");
			JsonObject joGameBasket = ((JsonObject) jvGameBasket);
			JsonValue jvPlayerDamages = joGameBasket.getValue("playerDamages");
			JsonArray jaPlayerDamages = ((JsonArray) jvPlayerDamages);
			long damage = 0;
			for (JsonValue jvPlayerDamage : jaPlayerDamages.getValue()) {
				JsonObject joPlayerDamage = ((JsonObject) jvPlayerDamage);
				damage += Long.parseLong(((JsonString) joPlayerDamage.getValue("damageAmount")).getValue());
			}
			JsonValue jvPlayerEntity = joGameBasket.getValue("playerEntity");
			JsonArray jaPlayerEntity = ((JsonArray) jvPlayerEntity);
			String ownerGuid = ((JsonString) jaPlayerEntity.getValue(0)).getValue();
			IngressAgent owner = this.getIngressAgentByGuid(ownerGuid);
			JsonValue jvPlayerInfo = jaPlayerEntity.getValue(2);
			JsonObject joPlayerInfo = ((JsonObject) jvPlayerInfo);
			JsonValue jvPlayerPersonal = joPlayerInfo.getValue("playerPersonal");
			JsonObject joPlayerPersonal = ((JsonObject) jvPlayerPersonal);
			int remainedEnergy = ((JsonNumber) joPlayerPersonal.getValue("energy")).getValue().intValue();
			String status = ((JsonString) joPlayerPersonal.getValue("energyState")).getValue();
			JsonValue jvInventories = joGameBasket.getValue("inventory");
			ArrayList<IngressInventory> ingressInventories = new ArrayList<>();
			if (jvInventories != null) {
				JsonArray jaInventories = ((JsonArray) jvInventories);
				for (JsonValue jvInventory : jaInventories.getValue()) {
					JsonArray jaInventory = ((JsonArray) jvInventory);
					String inventoryGuid = ((JsonString) jaInventory.getValue(0)).getValue();
					JsonValue jvInventoryInfo = jaInventory.getValue(2);
					ingressInventories.add(IngressInventoryContainer.parseIngressInventory(inventoryGuid, owner, jvInventoryInfo));
				}
			}
			return new IngressHackResponse(damage, remainedEnergy, status, ingressInventories.toArray(new IngressInventory[ingressInventories.size()]));
		}
	}

	public IngressHackResponse hackIngressPortalWithGlyphs(String guid, long latitude, long longitude, String[] glyphs) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/gameplay/collectItemsFromPortalWithGlyphResponse", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createHackIngressPortalWithGlyphsJSON(guid, latitude, longitude, glyphs, this.getClientBlob()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = ((JsonObject) jsonValue);
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		} else {
			JsonValue jvGameBasket = jsonObject.getValue("gameBasket");
			JsonObject joGameBasket = ((JsonObject) jvGameBasket);
			JsonValue jvPlayerDamages = joGameBasket.getValue("playerDamages");
			JsonArray jaPlayerDamages = ((JsonArray) jvPlayerDamages);
			long damage = 0;
			for (JsonValue jvPlayerDamage : jaPlayerDamages.getValue()) {
				JsonObject joPlayerDamage = ((JsonObject) jvPlayerDamage);
				damage += Long.parseLong(((JsonString) joPlayerDamage.getValue("damageAmount")).getValue());
			}
			JsonValue jvPlayerEntity = joGameBasket.getValue("playerEntity");
			JsonArray jaPlayerEntity = ((JsonArray) jvPlayerEntity);
			String ownerGuid = ((JsonString) jaPlayerEntity.getValue(0)).getValue();
			IngressAgent owner = this.getIngressAgentByGuid(ownerGuid);
			JsonValue jvPlayerInfo = jaPlayerEntity.getValue(2);
			JsonObject joPlayerInfo = ((JsonObject) jvPlayerInfo);
			JsonValue jvPlayerPersonal = joPlayerInfo.getValue("playerPersonal");
			JsonObject joPlayerPersonal = ((JsonObject) jvPlayerPersonal);
			int remainedEnergy = ((JsonNumber) joPlayerPersonal.getValue("energy")).getValue().intValue();
			String status = ((JsonString) joPlayerPersonal.getValue("energyState")).getValue();
			JsonValue jvInventories = joGameBasket.getValue("inventory");
			ArrayList<IngressInventory> ingressInventories = new ArrayList<>();
			if (jvInventories != null) {
				JsonArray jaInventories = ((JsonArray) jvInventories);
				for (JsonValue jvInventory : jaInventories.getValue()) {
					JsonArray jaInventory = ((JsonArray) jvInventory);
					String inventoryGuid = ((JsonString) jaInventory.getValue(0)).getValue();
					JsonValue jvInventoryInfo = jaInventory.getValue(2);
					ingressInventories.add(IngressInventoryContainer.parseIngressInventory(inventoryGuid, owner, jvInventoryInfo));
				}
			}
			return new IngressHackResponse(damage, remainedEnergy, status, ingressInventories.toArray(new IngressInventory[ingressInventories.size()]));
		}
	}

	public String[] requestGlyphIngressPortal(String guid, long latitude, long longitude) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/gameplay/collectItemsOrGlyphsFromPortal", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createRequestGlyphIngressPortalJSON(guid, latitude, longitude, this.getClientBlob()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = ((JsonObject) jsonValue);
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		} else {
			ArrayList<String> glyphs = new ArrayList<>();
			JsonValue jvResult = jsonObject.getValue("result");
			JsonObject joResult = ((JsonObject) jvResult);
			JsonValue jvGlyphs = joResult.getValue("glyphs");
			JsonObject joGlyphs = ((JsonObject) jvGlyphs);
			JsonValue jvGlyphSequence = joGlyphs.getValue("glyphSequence");
			JsonArray jaGlyphSequence = ((JsonArray) jvGlyphSequence);
			for (JsonValue jvGlyph : jaGlyphSequence.getValue()) {
				JsonObject joGlyph = (JsonObject) jvGlyph;
				String glyph = ((JsonString) joGlyph.getValue("glyphOrder")).getValue();
				glyphs.add(glyph);
			}
			return glyphs.toArray(new String[glyphs.size()]);
		}
	}

	public IngressRecycleResponse recycleIngressInventories(String[] guids, long latitude, long longitude) throws IOException {
		if (guids.length > 0) {
			URL url = new URL(String.format("%s://%s/rpc/gameplay/recycleItemsBulk", IngressApp.PROTOCOL, IngressApp.HOST));
			HttpRequest httpRequest = new HttpRequest(url);
			httpRequest.setMethod(HttpRequest.METHOD_POST);
			Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
			httpRequest.setRequestHeaders(requestHeaders);
			String requestData = IngressApp.createRecycleIngressInventoriesJSON(guids, latitude, longitude, this.getClientBlob()).toString();
			httpRequest.setRequestData(requestData);
			httpRequest.send();
			String responseData = httpRequest.getResponseString();
			JsonValue jsonValue = JsonParser.parse(responseData);
			JsonObject jsonObject = (JsonObject) jsonValue;
			JsonValue jvGameBasket = jsonObject.getValue("gameBasket");
			JsonObject joGameBasket = (JsonObject) jvGameBasket;
			JsonValue jvDeletedEntityGuids = joGameBasket.getValue("deletedEntityGuids");
			JsonArray jaDeletedEntityGuids = (JsonArray) jvDeletedEntityGuids;
			ArrayList<String> rGuids = new ArrayList<>();
			for (JsonValue jvDeletedEntityGuid : jaDeletedEntityGuids.getValue()) {
				String guid = ((JsonString) jvDeletedEntityGuid).getValue();
				rGuids.add(guid);
			}
			guids = rGuids.toArray(new String[rGuids.size()]);
		}
		return new IngressRecycleResponse(guids);
	}

	public IngressUsePowerCubeResponse useIngressPowerCube(String guid, long latitude, long longitude) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/gameplay/dischargePowerCube", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createUseIngressPowerCubeJSON(guid, latitude, longitude, this.getClientBlob()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = (JsonObject) jsonValue;
		JsonValue jvResult = jsonObject.getValue("result");
		JsonObject joResult = (JsonObject) jvResult;
		JsonValue jvXmGained = joResult.getValue("xmGained");
		int xm = ((JsonNumber) jvXmGained).getValue().intValue();
		JsonValue jvGameBasket = jsonObject.getValue("gameBasket");
		JsonObject joGameBasket = (JsonObject) jvGameBasket;
		JsonValue jvDeletedEntityGuids = joGameBasket.getValue("deletedEntityGuids");
		JsonArray jaDeletedEntityGuids = (JsonArray) jvDeletedEntityGuids;
		guid = ((JsonString) jaDeletedEntityGuids.getValue(0)).getValue();
		return new IngressUsePowerCubeResponse(guid, xm);
	}

	public void linkIngressPortal(String guidFrom, long latitude, long longitude, String guidTo, String keyGuid) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/gameplay/createLink", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createLinkIngressPortal(guidFrom, latitude, longitude, guidTo, keyGuid, this.getClientBlob()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = (JsonObject) jsonValue;
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		}
	}

	public void deployIngressResonator(String portalGuid, long latitude, long longitude, String resonatorGuid) throws IOException {
		this.deployIngressResonator(portalGuid, latitude, longitude, resonatorGuid, 255);
	}

	public void deployIngressResonator(String portalGuid, long latitude, long longitude, String resonatorGuid, int index) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/gameplay/deployResonatorV2", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createDeployIngressResonatorJSON(portalGuid, latitude, longitude, resonatorGuid, index, this.getClientBlob()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = (JsonObject) jsonValue;
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		}
	}

	public void upgradeIngressResonator(String portalGuid, long latitude, long longitude, String resonatorGuid, int index) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/gameplay/upgradeResonatorV2", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createUpgradeIngressResonatorJSON(portalGuid, latitude, longitude, resonatorGuid, index, this.getClientBlob()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = (JsonObject) jsonValue;
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		} else {
			System.out.println(jsonValue.toString(true));
		}
	}

	public void redeemReward(String passcode) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/playerUndecorated/redeemReward", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createRedeemRewardJSON(passcode).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = (JsonObject) jsonValue;
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		} else {
			System.out.println(jsonValue.toString(true));
		}
	}

	public void flipIngressPortal(String portalGuid, long latitude, long longitude, String flipGuid) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/gameplay/flipPortal", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createFlipIngressPortalJSON(portalGuid, latitude, longitude, flipGuid, this.getClientBlob()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = (JsonObject) jsonValue;
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		} else {
			System.out.println(jsonValue.toString(true));
		}
	}

	public void loadIngressStorableInventoryToIngressCapsule(String capsuleGuid, String[] storableGuids, long latitude, long longitude) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/gameplay/updateContainer", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createLoadIngressStorableInventoryToIngressCapsuleJSON(capsuleGuid, storableGuids, latitude, longitude, this.getClientBlob()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = (JsonObject) jsonValue;
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		} else {
			System.out.println(jsonValue.toString(true));
		}
	}

	public void unloadIngressStorableInventoryFromIngressCapsule(String capsuleGuid, String[] storableGuids, long latitude, long longitude) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/gameplay/updateContainer", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createUnloadIngressStorableInventoryFromIngressCapsuleJSON(capsuleGuid, storableGuids, latitude, longitude, this.getClientBlob()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = (JsonObject) jsonValue;
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		} else {
			System.out.println(jsonValue.toString(true));
		}
	}

	public void dropIngressInventory(String guid, long latitude, long longitude) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/gameplay/dropItem", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createDropIngressInventoryJSON(guid, latitude, longitude, this.getClientBlob()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = (JsonObject) jsonValue;
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		} else {
			System.out.println(jsonValue.toString(true));
		}
	}

	public void pickIngressInventory(String guid, long latitude, long longitude) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/gameplay/pickUp", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createPickIngressInventoryJSON(guid, latitude, longitude, this.getClientBlob()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = (JsonObject) jsonValue;
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		} else {
			System.out.println(jsonValue.toString(true));
		}
	}

	public void sendIngressInvitation(String email) throws IOException {
		this.sendIngressInvitation(email, "");
	}

	public void sendIngressInvitation(String email, String message) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/playerUndecorated/inviteViaEmail", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createSendInvitationJSON(email, message).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = (JsonObject) jsonValue;
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		} else {
			System.out.println(jsonValue.toString(true));
		}
	}

	public void startIngressMission(String guid, long latitude, long longitude) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/gameplay/startMission", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createStartIngressMissionJSON(guid, latitude, longitude, this.getClientBlob()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = (JsonObject) jsonValue;
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		}
	}

	public IngressFireResponse fireIngressWeapon(String guid, long latitude, long longitude) throws IOException {
		if (false) {
			URL url = new URL(String.format("%s://%s/rpc/gameplay/fireUntargetedRadialWeaponV2", IngressApp.PROTOCOL, IngressApp.HOST));
			HttpRequest httpRequest = new HttpRequest(url);
			httpRequest.setMethod(HttpRequest.METHOD_POST);
			Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
			httpRequest.setRequestHeaders(requestHeaders);
			String requestData = IngressApp.createFireIngressWeaponJSON(guid, latitude, longitude, this.getClientBlob()).toString();
			httpRequest.setRequestData(requestData);
			httpRequest.send();
			String responseString = httpRequest.getResponseString();
			JsonValue jsonValue = JsonParser.parse(responseString);
			JsonObject jsonObject = (JsonObject) jsonValue;
			JsonValue jvGameBasket = jsonObject.getValue("gameBasket");
			if (jvGameBasket == null) {
				JsonValue jvError = jsonObject.getValue("error");
				String errorCode = ((JsonString) jvError).getValue();
				String error = IngressApp.getIngressErrorMessage(errorCode);
				throw new IngressException(error);
			} else {
				JsonObject joGameBasket = ((JsonObject) jvGameBasket);
				JsonValue jvPlayerDamages = joGameBasket.getValue("playerDamages");
				JsonArray jaPlayerDamages = ((JsonArray) jvPlayerDamages);
				long damage = 0;
				for (JsonValue jvPlayerDamage : jaPlayerDamages.getValue()) {
					JsonObject joPlayerDamage = ((JsonObject) jvPlayerDamage);
					damage += Long.parseLong(((JsonString) joPlayerDamage.getValue("damageAmount")).getValue());
				}
				JsonValue jvAPGains = joGameBasket.getValue("apGains");
				JsonArray jaAPGains = ((JsonArray) jvAPGains);
				long apGained = 0;
				for (JsonValue jvAPGain : jaAPGains.getValue()) {
					JsonObject joAPGain = ((JsonObject) jvAPGain);
					apGained += Long.parseLong(((JsonString) joAPGain.getValue("apGainAmount")).getValue());
				}
				JsonValue jvPlayerEntity = joGameBasket.getValue("playerEntity");
				JsonArray jaPlayerEntity = ((JsonArray) jvPlayerEntity);
				JsonValue jvPlayerInfo = jaPlayerEntity.getValue(2);
				JsonObject joPlayerInfo = ((JsonObject) jvPlayerInfo);
				JsonValue jvPlayerPersonal = joPlayerInfo.getValue("playerPersonal");
				JsonObject joPlayerPersonal = ((JsonObject) jvPlayerPersonal);
				int remainedEnergy = ((JsonNumber) joPlayerPersonal.getValue("energy")).getValue().intValue();
				String status = ((JsonString) joPlayerPersonal.getValue("energyState")).getValue();
				return new IngressFireResponse(damage, apGained, remainedEnergy, status);
			}
		} else {
			throw new IngressException("fireIngressWeapon unsupported temporarily, function hacking");
		}
	}

	public void rateIngressMission(String guid, long latitude, long longitude, boolean yes) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/gameplay/rateMission", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createRateIngressMissionJSON(guid, latitude, longitude, yes, this.getClientBlob()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = (JsonObject) jsonValue;
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		} else {
			System.out.println(jsonValue.toString(true));
		}
	}

	public void enterPassphrase(String guid, long latitude, long longitude, int index, String passphrase) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/gameplay/completePassphraseObjective", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createEnterPassphraseJSON(guid, latitude, longitude, index, passphrase, this.getClientBlob()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = (JsonObject) jsonValue;
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		} else {
			System.out.println(jsonValue.toString(true));
		}
	}

	public void viewTrip(String guid, long latitude, long longitude, String missionGuid) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/gameplay/fieldTripCardViewed", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createViewTripJSON(guid, latitude, longitude, missionGuid, this.getClientBlob()).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = (JsonObject) jsonValue;
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		} else {
			System.out.println(jsonValue.toString(true));
		}
	}

	public void setAgentProfileVisibility(boolean visibility) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/playerUndecorated/setProfileSettings", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createSetAgentProfileVisibilityJSON(visibility).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = (JsonObject) jsonValue;
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		} else {
			System.out.println(jsonValue.toString(true));
		}
	}

	private void setNotifications(Boolean retrieveNoticationEmail, Boolean retrievePromotionEmail, Boolean retrievePlayerMessage, Boolean retrievePortalBeingAttack, Boolean retrieveInvitation, Boolean retrieveNews) throws IOException {
		URL url = new URL(String.format("%s://%s/rpc/playerUndecorated/setNotificationSettings", IngressApp.PROTOCOL, IngressApp.HOST));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = IngressApp.createXsrfTokenHeaders(this.getSacSid(), this.getToken());
		httpRequest.setRequestHeaders(requestHeaders);
		String requestData = IngressApp.createSetNotificationJSON(retrieveNoticationEmail, retrievePromotionEmail, retrievePlayerMessage, retrievePortalBeingAttack, retrieveInvitation, retrieveNews).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = (JsonObject) jsonValue;
		JsonValue jvError = jsonObject.getValue("error");
		if (jvError != null) {
			String errorCode = ((JsonString) jvError).getValue();
			String error = IngressApp.getIngressErrorMessage(errorCode);
			throw new IngressException(error);
		} else {
			System.out.println(jsonValue.toString(true));
		}
	}

	public void setRetrieveNoticationEmail(boolean enabled) throws IOException {
		this.setNotifications(enabled, null, null, null, null, null);
	}

	public void setRetrievePromotionEmail(boolean enabled) throws IOException {
		this.setNotifications(null, enabled, null, null, null, null);
	}

	public void setRetrievePlayerMessage(boolean enabled) throws IOException {
		this.setNotifications(null, null, enabled, null, null, null);
	}

	public void setRetrievePortalBeingAttack(boolean enabled) throws IOException {
		this.setNotifications(null, null, null, enabled, null, null);
	}

	public void setRetrieveInvitation(boolean enabled) throws IOException {
		this.setNotifications(null, null, null, null, enabled, null);
	}

	public void setRetrieveNews(boolean enabled) throws IOException {
		this.setNotifications(null, null, null, null, null, enabled);
	}
}
