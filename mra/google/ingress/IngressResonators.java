package mra.google.ingress;

public abstract class IngressResonators extends IngressLevelableInventory {

	static IngressResonators createIngressResonators(String guid, IngressAgent owner, int level, int remainedEnergy, int distance) {
		switch (level) {
			case 1: {
				return new IngressResonatorL1(guid, owner, remainedEnergy, distance);
			}
			case 2: {
				return new IngressResonatorL2(guid, owner, remainedEnergy, distance);
			}
			case 3: {
				return new IngressResonatorL3(guid, owner, remainedEnergy, distance);
			}
			case 4: {
				return new IngressResonatorL4(guid, owner, remainedEnergy, distance);
			}
			case 5: {
				return new IngressResonatorL5(guid, owner, remainedEnergy, distance);
			}
			case 6: {
				return new IngressResonatorL6(guid, owner, remainedEnergy, distance);
			}
			case 7: {
				return new IngressResonatorL7(guid, owner, remainedEnergy, distance);
			}
			case 8: {
				return new IngressResonatorL8(guid, owner, remainedEnergy, distance);
			}
		}
		return null;
	}

	static IngressResonators createIngressResonators(String guid, IngressAgent owner, int level) {
		return IngressResonators.createIngressResonators(guid, owner, level, -1, -1);
	}

	public static final int EAST = 0;
	public static final int NORTH_EAST = 1;
	public static final int NORTH = 2;
	public static final int NORTH_WEST = 3;
	public static final int WEST = 4;
	public static final int SOUTH_WEST = 5;
	public static final int SOUTH = 6;
	public static final int SOUTH_EAST = 7;

	private final int originalEnergy;
	private final int cost;
	private final int remainedEnergy;
	private final int distance;

	IngressResonators(String guid, IngressAgent owner, int level, int originalEnergy, int remainedEnergy, int distance) {
		super(guid, owner, "Resonator", level);
		this.originalEnergy = originalEnergy;
		this.cost = 50 * level;
		this.remainedEnergy = ((remainedEnergy < 0) ? originalEnergy : remainedEnergy);
		this.distance = distance;
	}

	public int getOriginalEnergy() {
		return this.originalEnergy;
	}

	public int getCost() {
		return this.cost;
	}

	public int getRemainedEnergy() {
		return this.remainedEnergy;
	}

	public int getDistance() {
		return this.distance;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("\nOriginal Energy = ");
		sb.append(this.getOriginalEnergy());
		sb.append("\nCost = ");
		sb.append(this.getCost());
		sb.append("\nRemained Energy = ");
		sb.append(this.getRemainedEnergy());
		sb.append("\nDistance = ");
		sb.append(this.getDistance());
		return sb.toString();
	}
}
