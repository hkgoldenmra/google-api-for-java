package mra.google.ingress;

public class IngressPowerCubeL8 extends IngressPowerCubes {

	IngressPowerCubeL8(String guid, IngressAgent owner) {
		super(guid, owner, 8);
	}
}
