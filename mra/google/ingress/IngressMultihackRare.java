package mra.google.ingress;

public class IngressMultihackRare extends IngressMultihacks {

	IngressMultihackRare(String guid, IngressAgent owner, int removalStickiness) {
		super(guid, owner, 80, "Multi-hack", "Rare", 800, removalStickiness, 8);
	}
}
