package mra.google.ingress;

public class IngressSoftBankUltraLink extends IngressLinkAmps {

	private final int links;
	private final int forceAmplifier;

	IngressSoftBankUltraLink(String guid, IngressAgent owner, int removalStickiness) {
		super(guid, owner, 1000, "SoftBank Ultra Link", "Very Rare", 1000, removalStickiness, 1500);
		this.links = 8;
		this.forceAmplifier = 500;
	}

	public int getLinks() {
		return this.links;
	}

	public int getForceAmplifier() {
		return this.forceAmplifier;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("\nLinks");
		sb.append(this.getLinks());
		sb.append("\nForce Amplifier");
		sb.append(this.getForceAmplifier());
		return sb.toString();
	}
}
