package mra.google.ingress;

public abstract class IngressLevelableInventory extends IngressStorableInventory {

	private final int level;

	IngressLevelableInventory(String guid, IngressAgent owner, String name, int level) {
		super(guid, owner, 20 * level, name, "Very Common");
		this.level = level;
	}

	@Override
	public String getName() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.getName());
		sb.append(" L");
		sb.append(this.getLevel());
		return sb.toString();
	}

	public int getLevel() {
		return this.level;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("\nLevel = ");
		sb.append(this.getLevel());
		return sb.toString();
	}
}
