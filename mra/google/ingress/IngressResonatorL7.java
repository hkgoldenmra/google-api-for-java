package mra.google.ingress;

public class IngressResonatorL7 extends IngressResonators {

	IngressResonatorL7(String guid, IngressAgent owner) {
		this(guid, owner, -1, -1);
	}

	IngressResonatorL7(String guid, IngressAgent owner, int remainedEnergy, int distance) {
		super(guid, owner, 7, 5000, remainedEnergy, distance);
	}
}
