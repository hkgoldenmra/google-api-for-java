package mra.google.ingress;

public class IngressLink extends IngressObject {

	private final String portalFromGuid;
	private final String portalToGuid;

	IngressLink(String guid, String portalFromGuid, String portalToGuid) {
		super(guid);
		this.portalFromGuid = portalFromGuid;
		this.portalToGuid = portalToGuid;
	}

	public String getPortalFromGuid() {
		return this.portalFromGuid;
	}

	public String getPortalToGuid() {
		return this.portalToGuid;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("\nPortal From GUID = ");
		sb.append(this.getPortalFromGuid());
		sb.append("\nPortal To GUID = ");
		sb.append(this.getPortalToGuid());
		return sb.toString();
	}
}
