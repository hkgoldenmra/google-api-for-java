package mra.google.ingress;

public class IngressUltraStrikeL4 extends IngressUltraStrikes {

	IngressUltraStrikeL4(String guid, IngressAgent owner) {
		super(guid, owner, 4, 900, 18, 140);
	}
}
