package mra.google.ingress;

public class IngressAXAShield extends IngressShields {

	IngressAXAShield(String guid, IngressAgent owner, int removalStickiness) {
		super(guid, owner, 100, "AXA Shield", "Very Rare", 1000, removalStickiness, 70);
	}
}
