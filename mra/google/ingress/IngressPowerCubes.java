package mra.google.ingress;

public abstract class IngressPowerCubes extends IngressLevelableInventory {

	static IngressPowerCubes createIngressPowerCubes(String guid, IngressAgent owner, int level) {
		switch (level) {
			case 1: {
				return new IngressPowerCubeL1(guid, owner);
			}
			case 2: {
				return new IngressPowerCubeL2(guid, owner);
			}
			case 3: {
				return new IngressPowerCubeL3(guid, owner);
			}
			case 4: {
				return new IngressPowerCubeL4(guid, owner);
			}
			case 5: {
				return new IngressPowerCubeL5(guid, owner);
			}
			case 6: {
				return new IngressPowerCubeL6(guid, owner);
			}
			case 7: {
				return new IngressPowerCubeL7(guid, owner);
			}
			case 8: {
				return new IngressPowerCubeL8(guid, owner);
			}
		}
		return null;
	}
	private final int recover;

	IngressPowerCubes(String guid, IngressAgent owner, int level) {
		super(guid, owner, "Power Cube", level);
		this.recover = 1000 * this.getLevel();
	}

	public int getRecover() {
		return this.recover;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("\nRecover = ");
		sb.append(this.getRecover());
		return sb.toString();
	}
}
