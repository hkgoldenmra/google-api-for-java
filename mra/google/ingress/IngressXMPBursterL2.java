package mra.google.ingress;

public class IngressXMPBursterL2 extends IngressXMPBursters {

	IngressXMPBursterL2(String guid, IngressAgent owner) {
		super(guid, owner, 2, 300, 48);
	}
}
