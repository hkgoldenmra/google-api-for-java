package mra.google.ingress;

import java.util.LinkedHashMap;
import java.util.Map;
import mra.json.JsonArray;
import mra.json.JsonObject;
import mra.json.JsonString;
import mra.json.JsonValue;

public class IngressAgent extends IngressObject {

	static JsonValue createGetMissionAchievementsJSON(String continuationToken) {
		JsonObject joParams = new JsonObject();
		joParams.setValue("continuationToken", new JsonString(continuationToken));
		JsonArray jsonArray = new JsonArray();
		jsonArray.addValue(joParams);
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue("params", jsonArray);
		return jsonObject;
	}
	static final String[] STANDARD_ACHIEVEMENT_LEVELS = {"Locked", "Bronze", "Silver", "Gold", "Platinum", "Black"};

	private final String code;
	private final String faction;
	private final int level;
	private final long ap;
	private final Map<String, Long> statistics;
	private final Map<String, String> standardAchievements;
	private final String[] otherAchievements;
	private final String[] missionAchievements;

	static Map<String, Long> initialStatistics() {
		Map<String, Long> statistics = new LinkedHashMap<>();
		statistics.put("Unique Portals Visited", 0L);
		statistics.put("Portals Discovered", 0L);
		statistics.put("XM Collected", 0L);
		statistics.put("Distance Walked", 0L);
		statistics.put("Resonators Deployed", 0L);
		statistics.put("Links Created", 0L);
		statistics.put("Control Fields Created", 0L);
		statistics.put("Mind Units Captured", 0L);
		statistics.put("Longest Link Ever Created", 0L);
		statistics.put("Largest Control Field", 0L);
		statistics.put("XM Recharged", 0L);
		statistics.put("Portals Captured", 0L);
		statistics.put("Unique Portals Captured", 0L);
		statistics.put("Mods Deployed", 0L);
		statistics.put("Links Active", 0L);
		statistics.put("Portals Owned", 0L);
		statistics.put("Control Fields Active", 0L);
		statistics.put("Mind Unit Control", 0L);
		statistics.put("Resonators Destroyed", 0L);
		statistics.put("Portals Neutralized", 0L);
		statistics.put("Enemy Links Destroyed", 0L);
		statistics.put("Enemy Control Fields Destroyed", 0L);
		statistics.put("Max Time Portal Held", 0L);
		statistics.put("Max Time Link Maintained", 0L);
		statistics.put("Max Link Length x Days", 0L);
		statistics.put("Max Time Field Held", 0L);
		statistics.put("Largest Field MUs x Days", 0L);
		statistics.put("Unique Missions Completed", 0L);
		statistics.put("Hacks", 0L);
		statistics.put("Glyph Hack Points", 0L);
		statistics.put("Longest Hacking Streak", 0L);
		statistics.put("Agents Successfully Recruited", 0L);
		return statistics;
	}

	static Map<String, String> initialStandardAchievements() {
		Map<String, String> standardAchievements = new LinkedHashMap<>();
		standardAchievements.put("Innovator", IngressAgent.STANDARD_ACHIEVEMENT_LEVELS[0]);
		standardAchievements.put("Vanguard", IngressAgent.STANDARD_ACHIEVEMENT_LEVELS[0]);
		standardAchievements.put("Hacker", IngressAgent.STANDARD_ACHIEVEMENT_LEVELS[0]);
		standardAchievements.put("Explorer", IngressAgent.STANDARD_ACHIEVEMENT_LEVELS[0]);
		standardAchievements.put("Builder", IngressAgent.STANDARD_ACHIEVEMENT_LEVELS[0]);
		standardAchievements.put("Liberator", IngressAgent.STANDARD_ACHIEVEMENT_LEVELS[0]);
		standardAchievements.put("Pioneer", IngressAgent.STANDARD_ACHIEVEMENT_LEVELS[0]);
		standardAchievements.put("Connector", IngressAgent.STANDARD_ACHIEVEMENT_LEVELS[0]);
		standardAchievements.put("Mind Controller", IngressAgent.STANDARD_ACHIEVEMENT_LEVELS[0]);
		standardAchievements.put("Recharger", IngressAgent.STANDARD_ACHIEVEMENT_LEVELS[0]);
		standardAchievements.put("Purifier", IngressAgent.STANDARD_ACHIEVEMENT_LEVELS[0]);
		standardAchievements.put("Guardian", IngressAgent.STANDARD_ACHIEVEMENT_LEVELS[0]);
		standardAchievements.put("Engineer", IngressAgent.STANDARD_ACHIEVEMENT_LEVELS[0]);
		standardAchievements.put("SpecOps", IngressAgent.STANDARD_ACHIEVEMENT_LEVELS[0]);
		standardAchievements.put("Trekker", IngressAgent.STANDARD_ACHIEVEMENT_LEVELS[0]);
		standardAchievements.put("Recruiter", IngressAgent.STANDARD_ACHIEVEMENT_LEVELS[0]);
		standardAchievements.put("Translator", IngressAgent.STANDARD_ACHIEVEMENT_LEVELS[0]);
		standardAchievements.put("Illuminator", IngressAgent.STANDARD_ACHIEVEMENT_LEVELS[0]);
		standardAchievements.put("Sojourner", IngressAgent.STANDARD_ACHIEVEMENT_LEVELS[0]);
		return standardAchievements;
	}

	IngressAgent(String guid, String code, String faction, int level, long ap, Map<String, Long> statistics, Map<String, String> standardAchievements, String[] otherAchievements, String[] missionAchievements) {
		super(guid);
		this.code = code;
		this.faction = faction;
		this.level = level;
		this.ap = ap;
		this.statistics = IngressAgent.initialStatistics();
		for (String key : statistics.keySet()) {
			long value = statistics.get(key);
			this.statistics.put(key, value);
		}
		this.standardAchievements = IngressAgent.initialStandardAchievements();
		for (String key : standardAchievements.keySet()) {
			String value = standardAchievements.get(key);
			this.standardAchievements.put(key, value);
		}
		this.otherAchievements = otherAchievements;
		this.missionAchievements = missionAchievements;
	}

	public String getCode() {
		return this.code;
	}

	public String getFaction() {
		return this.faction;
	}

	public int getLevel() {
		return this.level;
	}

	public long getAP() {
		return this.ap;
	}

	public long getUniquePortalsVisited() {
		return this.statistics.get("Unique Portals Visited");
	}

	public long getPortalsDiscovered() {
		return this.statistics.get("Portals Discovered");
	}

	public long getXMCollected() {
		return this.statistics.get("XM Collected");
	}

	public long getDistanceWalked() {
		return this.statistics.get("Distance Walked");
	}

	public long getResonatorsDeployed() {
		return this.statistics.get("Resonators Deployed");
	}

	public long getLinksCreated() {
		return this.statistics.get("Links Created");
	}

	public long getControlFieldsCreated() {
		return this.statistics.get("Control Fields Created");
	}

	public long getMindUnitsCaptured() {
		return this.statistics.get("Mind Units Captured");
	}

	public long getLongestLinkEverCreated() {
		return this.statistics.get("Longest Link Ever Created");
	}

	public long getLargestControlField() {
		return this.statistics.get("Largest Control Field");
	}

	public long getXMRecharged() {
		return this.statistics.get("XM Recharged");
	}

	public long getPortalsCaptured() {
		return this.statistics.get("Portals Captured");
	}

	public long getUniquePortalsCaptured() {
		return this.statistics.get("Unique Portals Captured");
	}

	public long getModsDeployed() {
		return this.statistics.get("Mods Deployed");
	}

	public long getLinksActive() {
		return this.statistics.get("Links Active");
	}

	public long getPortalsOwned() {
		return this.statistics.get("Portals Owned");
	}

	public long getControlFieldsActive() {
		return this.statistics.get("Control Fields Active");
	}

	public long getMindUnitControl() {
		return this.statistics.get("Mind Unit Control");
	}

	public long getResonatorsDestroyed() {
		return this.statistics.get("Resonators Destroyed");
	}

	public long getPortalsNeutralized() {
		return this.statistics.get("Portals Neutralized");
	}

	public long getEnemyLinksDestroyed() {
		return this.statistics.get("Enemy Links Destroyed");
	}

	public long getEnemyControlFieldsDestroyed() {
		return this.statistics.get("Enemy Control Fields Destroyed");
	}

	public long getMaxTimePortalHeld() {
		return this.statistics.get("Max Time Portal Held");
	}

	public long getMaxTimeLinkMaintained() {
		return this.statistics.get("Max Time Link Maintained");
	}

	public long getMaxLinkLengthXDays() {
		return this.statistics.get("Max Link Length x Days");
	}

	public long getMaxTimeFieldHeld() {
		return this.statistics.get("Max Time Field Held");
	}

	public long getLargestFieldMUsXDays() {
		return this.statistics.get("Largest Field MUs x Days");
	}

	public long getUniqueMissionsCompleted() {
		return this.statistics.get("Unique Missions Completed");
	}

	public long getHacks() {
		return this.statistics.get("Hacks");
	}

	public long getGlyphHackPoints() {
		return this.statistics.get("Glyph Hack Points");
	}

	public long getLongestHackingStreak() {
		return this.statistics.get("Longest Hacking Streak");
	}

	public long getAgentsSuccessfullyRecruited() {
		return this.statistics.get("Agents Successfully Recruited");
	}

	public String getInnovator() {
		return this.standardAchievements.get("Innovator");
	}

	public String getHacker() {
		return this.standardAchievements.get("Hacker");
	}

	public String getExplorer() {
		return this.standardAchievements.get("Explorer");
	}

	public String getBuilder() {
		return this.standardAchievements.get("Builder");
	}

	public String getLiberator() {
		return this.standardAchievements.get("Liberator");
	}

	public String getConnector() {
		return this.standardAchievements.get("Connector");
	}

	public String getMindController() {
		return this.standardAchievements.get("Mind Controller");
	}

	public String getRecharger() {
		return this.standardAchievements.get("Recharger");
	}

	public String getPurifier() {
		return this.standardAchievements.get("Purifier");
	}

	public String getGuardian() {
		return this.standardAchievements.get("Guardian");
	}

	public String getEngineer() {
		return this.standardAchievements.get("Engineer");
	}

	public String getSpecOps() {
		return this.standardAchievements.get("SpecOps");
	}

	public String getTrekker() {
		return this.standardAchievements.get("Trekker");
	}

	public String getRecruiter() {
		return this.standardAchievements.get("Recruiter");
	}

	public String getTranslator() {
		return this.standardAchievements.get("Translator");
	}

	public String getIlluminator() {
		return this.standardAchievements.get("Illuminator");
	}

	public String getSojourner() {
		return this.standardAchievements.get("Sojourner");
	}

	public String[] listOtherAchievements() {
		return this.otherAchievements;
	}

	public String[] listMissionAchievements() {
		return this.missionAchievements;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("\nCode = ");
		sb.append(this.getCode());
		sb.append("\nFaction = ");
		sb.append(this.getFaction());
		sb.append("\nLevel = ");
		sb.append(this.getLevel());
		sb.append("\nAP = ");
		sb.append(this.getAP());
		for (String key : this.statistics.keySet()) {
			long value = this.statistics.get(key);
			sb.append("\n");
			sb.append(key);
			sb.append(" = ");
			sb.append(value);
		}
		for (String key : this.standardAchievements.keySet()) {
			String value = this.standardAchievements.get(key);
			sb.append("\n");
			sb.append(key);
			sb.append(" = ");
			sb.append(value);
		}
		sb.append("\nOther Achievements = {");
		for (String otherAchievement : this.listOtherAchievements()) {
			sb.append("\n");
			sb.append(otherAchievement);
		}
		sb.append("\n}");
		sb.append("\nMission Achievements = {");
		for (String missionAchievement : this.listMissionAchievements()) {
			sb.append("\n");
			sb.append(missionAchievement);
		}
		sb.append("\n}");
		return sb.toString();
	}
}
