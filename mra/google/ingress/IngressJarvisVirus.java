package mra.google.ingress;

public class IngressJarvisVirus extends IngressFlipCards {

	IngressJarvisVirus(String guid, IngressAgent owner) {
		super(guid, owner, "Jarvis Virus");
	}
}
