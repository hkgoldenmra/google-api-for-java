package mra.google.ingress;

public class IngressLinkAmpVeryRare extends IngressLinkAmps {

	IngressLinkAmpVeryRare(String guid, IngressAgent owner, int removalStickiness) {
		super(guid, owner, 100, "Link Amp", "Very Rare", 1000, removalStickiness, 7000);
	}
}
