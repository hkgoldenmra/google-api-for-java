package mra.google.ingress;

public abstract class IngressMods extends IngressStorableInventory {

	private final int cost;
	private final int removalStickiness;

	IngressMods(String guid, IngressAgent owner, int recycle, String name, String rarity, int cost, int removalStickiness) {
		super(guid, owner, recycle, name, rarity);
		this.cost = cost;
		this.removalStickiness = removalStickiness;
	}

	public int getRemovalStickiness() {
		return this.removalStickiness;
	}

	public int getCost() {
		return this.cost;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("\nCost = ");
		sb.append(this.getCost());
		sb.append("\nRemoval Stickiness = ");
		sb.append(this.getRemovalStickiness());
		return sb.toString();
	}
}
