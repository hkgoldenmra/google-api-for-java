package mra.google.ingress;

public abstract class IngressNonAgentObject extends IngressObject {

	private final IngressAgent owner;

	IngressNonAgentObject(String guid, IngressAgent owner) {
		super(guid);
		this.owner = owner;
	}

	public IngressAgent getOwner() {
		return this.owner;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("\nIngress Agent = {\n");
		sb.append(this.getOwner() == null ? "<null>" : this.getOwner().toString());
		sb.append("\n}");
		return sb.toString();
	}
}
