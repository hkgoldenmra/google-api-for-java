package mra.google.ingress;

public class IngressRecruit {

	private final String email;
	private final String status;

	IngressRecruit(String email, String status) {
		this.email = email;
		this.status = status;
	}

	public String getEmail() {
		return this.email;
	}

	public String getStatus() {
		return this.status;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("E-Mail = ");
		sb.append(this.getEmail());
		sb.append("\nStatus = ");
		sb.append(this.getStatus());
		return sb.toString();
	}
}
