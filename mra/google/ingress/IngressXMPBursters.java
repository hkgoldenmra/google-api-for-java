package mra.google.ingress;

public abstract class IngressXMPBursters extends IngressWeapons {

	static IngressXMPBursters createIngressXMPBursters(String guid, IngressAgent owner, int level) {
		switch (level) {
			case 1: {
				return new IngressXMPBursterL1(guid, owner);
			}
			case 2: {
				return new IngressXMPBursterL2(guid, owner);
			}
			case 3: {
				return new IngressXMPBursterL3(guid, owner);
			}
			case 4: {
				return new IngressXMPBursterL4(guid, owner);
			}
			case 5: {
				return new IngressXMPBursterL5(guid, owner);
			}
			case 6: {
				return new IngressXMPBursterL6(guid, owner);
			}
			case 7: {
				return new IngressXMPBursterL7(guid, owner);
			}
			case 8: {
				return new IngressXMPBursterL8(guid, owner);
			}
		}
		return null;
	}

	IngressXMPBursters(String guid, IngressAgent owner, int level, int damage, int radius) {
		super(guid, owner, "XMP Burster", level, damage, radius, 50 * level);
	}
}
