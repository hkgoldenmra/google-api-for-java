package mra.google.ingress;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import mra.json.JsonArray;
import mra.json.JsonNumber;
import mra.json.JsonObject;
import mra.json.JsonString;
import mra.json.JsonValue;

public class IngressInventoryContainer {

	static IngressMods parseIngressMods(String guid, IngressAgent owner, JsonValue jvModResource) {
		JsonObject joModResource = ((JsonObject) jvModResource);
		String resourceType = null;
		if (resourceType == null) {
			try {
				resourceType = ((JsonString) joModResource.getValue("resourceType")).getValue();
			} catch (Exception ex) {
			}
		}
		if (resourceType == null) {
			try {
				resourceType = ((JsonString) joModResource.getValue("type")).getValue();
			} catch (Exception ex) {
			}
		}
		String rarity = ((JsonString) joModResource.getValue("rarity")).getValue();
		int removalStickiness = Integer.parseInt(((JsonString) ((JsonObject) joModResource.getValue("stats")).getValue("REMOVAL_STICKINESS")).getValue());
		if (resourceType.equals("RES_SHIELD")) {
			return IngressShields.createIngressShields(guid, owner, rarity, removalStickiness);
		} else if (resourceType.equals("EXTRA_SHIELD")) {
			return new IngressAXAShield(guid, owner, removalStickiness);
		} else if (resourceType.equals("LINK_AMPLIFIER")) {
			return IngressLinkAmps.createIngressLinkAmps(guid, owner, rarity, removalStickiness);
		} else if (resourceType.equals("ULTRA_LINK_AMP")) {
			return new IngressSoftBankUltraLink(guid, owner, removalStickiness);
		} else if (resourceType.equals("HEATSINK")) {
			return IngressHeatSinks.createIngressHeatSinks(guid, owner, rarity, removalStickiness);
		} else if (resourceType.equals("MULTIHACK")) {
			return IngressMultihacks.createIngressMultihacks(guid, owner, rarity, removalStickiness);
		} else if (resourceType.equals("FORCE_AMP")) {
			return new IngressForceAmp(guid, owner, removalStickiness);
		} else if (resourceType.equals("TURRET")) {
			return new IngressTurret(guid, owner, removalStickiness);
		}
		return null;
	}

	static IngressInventory parseIngressInventory(String guid, IngressAgent owner, JsonValue jvInventoryInfo) throws MalformedURLException {
		JsonObject joInventoryInfo = ((JsonObject) jvInventoryInfo);
		JsonValue jvResourceWithLevels = joInventoryInfo.getValue("resourceWithLevels");
		if (jvResourceWithLevels != null) {
			JsonObject joResourceWithLevels = ((JsonObject) jvResourceWithLevels);
			int level = ((JsonNumber) joResourceWithLevels.getValue("level")).getValue().intValue();
			JsonValue jvResourceType = joResourceWithLevels.getValue("resourceType");
			if (jvResourceType != null) {
				String resourceType = ((JsonString) jvResourceType).getValue();
				if (resourceType.equals("MEDIA")) {
					JsonValue jvStoryItem = joInventoryInfo.getValue("storyItem");
					JsonObject joStoryItem = ((JsonObject) jvStoryItem);
					String title = ((JsonString) joStoryItem.getValue("shortDescription")).getValue();
					String url = ((JsonString) joStoryItem.getValue("primaryUrl")).getValue();
					return new IngressMedia(guid, owner, title, new URL(url));
				} else if (resourceType.equals("EMITTER_A")) {
					return IngressResonators.createIngressResonators(guid, owner, level);
				} else if (resourceType.equals("POWER_CUBE")) {
					return IngressPowerCubes.createIngressPowerCubes(guid, owner, level);
				} else if (resourceType.equals("EMP_BURSTER")) {
					return IngressXMPBursters.createIngressXMPBursters(guid, owner, level);
				} else if (resourceType.equals("ULTRA_STRIKE")) {
					return IngressUltraStrikes.createIngressUltraStrikes(guid, owner, level);
				}
			}
		}
		JsonValue jvModResource = joInventoryInfo.getValue("modResource");
		if (jvModResource != null) {
			return IngressInventoryContainer.parseIngressMods(guid, owner, jvModResource);
		}
		JsonValue jvFlipCard = joInventoryInfo.getValue("flipCard");
		if (jvFlipCard != null) {
			JsonObject joFlipCard = ((JsonObject) jvFlipCard);
			String flipCardType = ((JsonString) joFlipCard.getValue("flipCardType")).getValue();
			if (flipCardType.equals("ADA")) {
				return new IngressAdaRefactor(guid, owner);
			} else if (flipCardType.equals("JARVIS")) {
				return new IngressJarvisVirus(guid, owner);
			}
		}
		JsonValue jvPortalCoupler = joInventoryInfo.getValue("portalCoupler");
		if (jvPortalCoupler != null) {
			JsonObject joPortalCoupler = ((JsonObject) jvPortalCoupler);
			String ingressPortalGuid = ((JsonString) joPortalCoupler.getValue("portalGuid")).getValue();
			String location = ((JsonString) joPortalCoupler.getValue("portalLocation")).getValue();
			String[] fields = location.split(",");
			long latitude = 0;
			try {
				Long.parseLong(fields[0]);
			} catch (Exception ex) {
			}
			long longitude = 0;
			try {
				longitude = Long.parseLong(fields[1]);
			} catch (Exception ex) {
			}
			String coverUrl = ((JsonString) joPortalCoupler.getValue("portalImageUrl")).getValue();
			String title = ((JsonString) joPortalCoupler.getValue("portalTitle")).getValue();
			String address = ((JsonString) joPortalCoupler.getValue("portalAddress")).getValue();
			return new IngressPortalKey(guid, owner, ingressPortalGuid, latitude, longitude, title, address, coverUrl);
		}
		{
			JsonValue jvResource = joInventoryInfo.getValue("resource");
			if (jvResource != null) {
				JsonObject joResource = ((JsonObject) jvResource);
				String resourceType = ((JsonString) joResource.getValue("resourceType")).getValue();
				if (resourceType.equals("BOOSTED_POWER_CUBE")) {
					return new IngressLawsonPowerCube(guid, owner);
				}
			}
		}
		JsonValue jvContainer = joInventoryInfo.getValue("container");
		if (jvContainer != null) {
			IngressCapsules ingressCapsules = null;
			JsonValue jvResource = joInventoryInfo.getValue("resource");
			JsonObject joResource = ((JsonObject) jvResource);
			String resourceType = ((JsonString) joResource.getValue("resourceType")).getValue();
			if (resourceType.equals("CAPSULE")) {
				ingressCapsules = new IngressCapsule(guid, owner);
			} else if (resourceType.equals("INTEREST_CAPSULE")) {
				ingressCapsules = new IngressMUFGCapsule(guid, owner);
			}
			if (ingressCapsules != null) {
				JsonObject joContainer = ((JsonObject) jvContainer);
				JsonValue jvStackableItems = joContainer.getValue("stackableItems");
				JsonArray jaStackableItems = ((JsonArray) jvStackableItems);
				for (JsonValue jvStackableItem : jaStackableItems.getValue()) {
					JsonObject joStackableItem = ((JsonObject) jvStackableItem);
					JsonValue jvItemGuids = joStackableItem.getValue("itemGuids");
					JsonArray jaItemGuids = ((JsonArray) jvItemGuids);
					JsonValue jvExampleGameEntity = joStackableItem.getValue("exampleGameEntity");
					JsonArray jaExampleGameEntity = ((JsonArray) jvExampleGameEntity);
					JsonValue jvCapsuleInventoryInfo = jaExampleGameEntity.getValue(2);
					for (JsonValue jvItemGuid : jaItemGuids.getValue()) {
						String itemGuid = ((JsonString) jvItemGuid).getValue();
						IngressStorableInventory ingressStorableInventory = ((IngressStorableInventory) IngressInventoryContainer.parseIngressInventory(itemGuid, owner, jvCapsuleInventoryInfo));
						ingressCapsules.addIngressInventory(ingressStorableInventory);
					}
				}
				return ingressCapsules;
			}
		}
		return null;
	}

	private final IngressGenericContainer<IngressInventory> ingressInventoryContainer = new IngressGenericContainer<>();

	IngressInventoryContainer(IngressInventory[] ingressInventories) throws IOException {
		for (IngressInventory ingressInventory : ingressInventories) {
			this.ingressInventoryContainer.add(ingressInventory);
		}
	}

	private int countIngressInventory(Class clazz, boolean includeInsideIngressCapules) {
		int count = 0;
		for (IngressInventory ingressInventory : this.ingressInventoryContainer.getAll()) {
			if (clazz.isInstance(ingressInventory)) {
				count++;
			}
			if (includeInsideIngressCapules && IngressCapsules.class.isInstance(ingressInventory)) {
				IngressCapsules ingressCapsules = ((IngressCapsules) ingressInventory);
				for (IngressStorableInventory ingressStorableInventory : ingressCapsules.getAll()) {
					if (clazz.isInstance(ingressStorableInventory)) {
						count++;
					}
				}
			}
		}
		return count;
	}

	public int countAllIngressInventories(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressInventory.class, includeInsideIngressCapsules);
	}

	public int countAllIngressCapsules() {
		return this.countIngressInventory(IngressCapsules.class, false);
	}

	public int countIngressMUFGCapsule() {
		return this.countIngressInventory(IngressMUFGCapsule.class, false);
	}

	public int countIngressCapsule() {
		return this.countIngressInventory(IngressCapsule.class, false);
	}

	public int countIngressMedia(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressMedia.class, includeInsideIngressCapsules);
	}

	public int countAllIngressMods(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressMods.class, includeInsideIngressCapsules);
	}

	public int countAllIngressShields(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressShields.class, includeInsideIngressCapsules);
	}

	public int countIngressShieldCommon(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressShieldCommon.class, includeInsideIngressCapsules);
	}

	public int countIngressShieldRare(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressShieldRare.class, includeInsideIngressCapsules);
	}

	public int countIngressShieldVeryRare(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressShieldVeryRare.class, includeInsideIngressCapsules);
	}

	public int countIngressAXAShield(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressAXAShield.class, includeInsideIngressCapsules);
	}

	public int countAllIngressLinkAmps(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressLinkAmps.class, includeInsideIngressCapsules);
	}

	public int countIngressLinkAmpRare(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressLinkAmpRare.class, includeInsideIngressCapsules);
	}

	public int countIngressLinkAmpVeryRare(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressLinkAmpVeryRare.class, includeInsideIngressCapsules);
	}

	public int countIngressSoftBankUltraLink(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressSoftBankUltraLink.class, includeInsideIngressCapsules);
	}

	public int countAllIngressHeatSinks(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressHeatSinks.class, includeInsideIngressCapsules);
	}

	public int countIngressHeatSinkCommon(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressHeatSinkCommon.class, includeInsideIngressCapsules);
	}

	public int countIngressHeatSinkRare(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressHeatSinkRare.class, includeInsideIngressCapsules);
	}

	public int countIngressHeatSinkVeryRare(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressHeatSinkVeryRare.class, includeInsideIngressCapsules);
	}

	public int countAllIngressMultihacks(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressMultihacks.class, includeInsideIngressCapsules);
	}

	public int countIngressMultihackCommon(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressMultihackCommon.class, includeInsideIngressCapsules);
	}

	public int countIngressMultihackRare(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressMultihackRare.class, includeInsideIngressCapsules);
	}

	public int countIngressMultihackVeryRare(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressMultihackVeryRare.class, includeInsideIngressCapsules);
	}

	public int countIngressForceAmp(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressForceAmp.class, includeInsideIngressCapsules);
	}

	public int countIngressTurret(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressTurret.class, includeInsideIngressCapsules);
	}

	public int countIngressPortalKey(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressPortalKey.class, includeInsideIngressCapsules);
	}

	public int countIngressLawsonPowerCube(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressLawsonPowerCube.class, includeInsideIngressCapsules);
	}

	public int countAllIngressPowerCubes(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressPowerCubes.class, includeInsideIngressCapsules);
	}

	public int countIngressPowerCubeL1(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressPowerCubeL1.class, includeInsideIngressCapsules);
	}

	public int countIngressPowerCubeL2(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressPowerCubeL2.class, includeInsideIngressCapsules);
	}

	public int countIngressPowerCubeL3(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressPowerCubeL3.class, includeInsideIngressCapsules);
	}

	public int countIngressPowerCubeL4(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressPowerCubeL4.class, includeInsideIngressCapsules);
	}

	public int countIngressPowerCubeL5(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressPowerCubeL5.class, includeInsideIngressCapsules);
	}

	public int countIngressPowerCubeL6(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressPowerCubeL6.class, includeInsideIngressCapsules);
	}

	public int countIngressPowerCubeL7(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressPowerCubeL7.class, includeInsideIngressCapsules);
	}

	public int countIngressPowerCubeL8(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressPowerCubeL8.class, includeInsideIngressCapsules);
	}

	public int countAllIngressResonators(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressResonators.class, includeInsideIngressCapsules);
	}

	public int countIngressResonatorL1(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressResonatorL1.class, includeInsideIngressCapsules);
	}

	public int countIngressResonatorL2(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressResonatorL2.class, includeInsideIngressCapsules);
	}

	public int countIngressResonatorL3(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressResonatorL3.class, includeInsideIngressCapsules);
	}

	public int countIngressResonatorL4(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressResonatorL4.class, includeInsideIngressCapsules);
	}

	public int countIngressResonatorL5(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressResonatorL5.class, includeInsideIngressCapsules);
	}

	public int countIngressResonatorL6(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressResonatorL6.class, includeInsideIngressCapsules);
	}

	public int countIngressResonatorL7(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressResonatorL7.class, includeInsideIngressCapsules);
	}

	public int countIngressResonatorL8(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressResonatorL8.class, includeInsideIngressCapsules);
	}

	public int countAllIngressWeapons(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressWeapons.class, includeInsideIngressCapsules) + this.countIngressAXAShield(includeInsideIngressCapsules) + this.countIngressJarvisVirus(includeInsideIngressCapsules);
	}

	public int countAllIngressXMPBursters(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressXMPBursters.class, includeInsideIngressCapsules);
	}

	public int countIngressXMPBursterL1(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressXMPBursterL1.class, includeInsideIngressCapsules);
	}

	public int countIngressXMPBursterL2(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressXMPBursterL2.class, includeInsideIngressCapsules);
	}

	public int countIngressXMPBursterL3(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressXMPBursterL3.class, includeInsideIngressCapsules);
	}

	public int countIngressXMPBursterL4(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressXMPBursterL4.class, includeInsideIngressCapsules);
	}

	public int countIngressXMPBursterL5(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressXMPBursterL5.class, includeInsideIngressCapsules);
	}

	public int countIngressXMPBursterL6(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressXMPBursterL6.class, includeInsideIngressCapsules);
	}

	public int countIngressXMPBursterL7(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressXMPBursterL7.class, includeInsideIngressCapsules);
	}

	public int countIngressXMPBursterL8(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressXMPBursterL8.class, includeInsideIngressCapsules);
	}

	public int countAllIngressUltraStrikes(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressUltraStrikes.class, includeInsideIngressCapsules);
	}

	public int countIngressUltraStrikeL1(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressUltraStrikeL1.class, includeInsideIngressCapsules);
	}

	public int countIngressUltraStrikeL2(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressUltraStrikeL2.class, includeInsideIngressCapsules);
	}

	public int countIngressUltraStrikeL3(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressUltraStrikeL3.class, includeInsideIngressCapsules);
	}

	public int countIngressUltraStrikeL4(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressUltraStrikeL4.class, includeInsideIngressCapsules);
	}

	public int countIngressUltraStrikeL5(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressUltraStrikeL5.class, includeInsideIngressCapsules);
	}

	public int countIngressUltraStrikeL6(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressUltraStrikeL6.class, includeInsideIngressCapsules);
	}

	public int countIngressUltraStrikeL7(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressUltraStrikeL7.class, includeInsideIngressCapsules);
	}

	public int countIngressUltraStrikeL8(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressUltraStrikeL8.class, includeInsideIngressCapsules);
	}

	public int countIngressAdaRefactor(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressAdaRefactor.class, includeInsideIngressCapsules);
	}

	public int countIngressJarvisVirus(boolean includeInsideIngressCapsules) {
		return this.countIngressInventory(IngressJarvisVirus.class, includeInsideIngressCapsules);
	}

	public int countAllIngressInventories() {
		return this.countIngressInventory(IngressInventory.class, true);
	}

	public int countIngressMedia() {
		return this.countIngressInventory(IngressMedia.class, true);
	}

	public int countAllIngressMods() {
		return this.countIngressInventory(IngressMods.class, true);
	}

	public int countAllIngressShields() {
		return this.countIngressInventory(IngressShields.class, true);
	}

	public int countIngressShieldCommon() {
		return this.countIngressInventory(IngressShieldCommon.class, true);
	}

	public int countIngressShieldRare() {
		return this.countIngressInventory(IngressShieldRare.class, true);
	}

	public int countIngressShieldVeryRare() {
		return this.countIngressInventory(IngressShieldVeryRare.class, true);
	}

	public int countIngressAXAShield() {
		return this.countIngressInventory(IngressAXAShield.class, true);
	}

	public int countAllIngressLinkAmps() {
		return this.countIngressInventory(IngressLinkAmps.class, true);
	}

	public int countIngressLinkAmpRare() {
		return this.countIngressInventory(IngressLinkAmpRare.class, true);
	}

	public int countIngressLinkAmpVeryRare() {
		return this.countIngressInventory(IngressLinkAmpVeryRare.class, true);
	}

	public int countIngressSoftBankUltraLink() {
		return this.countIngressInventory(IngressSoftBankUltraLink.class, true);
	}

	public int countAllIngressHeatSinks() {
		return this.countIngressInventory(IngressHeatSinks.class, true);
	}

	public int countIngressHeatSinkCommon() {
		return this.countIngressInventory(IngressHeatSinkCommon.class, true);
	}

	public int countIngressHeatSinkRare() {
		return this.countIngressInventory(IngressHeatSinkRare.class, true);
	}

	public int countIngressHeatSinkVeryRare() {
		return this.countIngressInventory(IngressHeatSinkVeryRare.class, true);
	}

	public int countAllIngressMultihacks() {
		return this.countIngressInventory(IngressMultihacks.class, true);
	}

	public int countIngressMultihackCommon() {
		return this.countIngressInventory(IngressMultihackCommon.class, true);
	}

	public int countIngressMultihackRare() {
		return this.countIngressInventory(IngressMultihackRare.class, true);
	}

	public int countIngressMultihackVeryRare() {
		return this.countIngressInventory(IngressMultihackVeryRare.class, true);
	}

	public int countIngressForceAmp() {
		return this.countIngressInventory(IngressForceAmp.class, true);
	}

	public int countIngressTurret() {
		return this.countIngressInventory(IngressTurret.class, true);
	}

	public int countIngressPortalKey() {
		return this.countIngressInventory(IngressPortalKey.class, true);
	}

	public int countIngressLawsonPowerCube() {
		return this.countIngressInventory(IngressLawsonPowerCube.class, true);
	}

	public int countAllIngressPowerCubes() {
		return this.countIngressInventory(IngressPowerCubes.class, true);
	}

	public int countIngressPowerCubeL1() {
		return this.countIngressInventory(IngressPowerCubeL1.class, true);
	}

	public int countIngressPowerCubeL2() {
		return this.countIngressInventory(IngressPowerCubeL2.class, true);
	}

	public int countIngressPowerCubeL3() {
		return this.countIngressInventory(IngressPowerCubeL3.class, true);
	}

	public int countIngressPowerCubeL4() {
		return this.countIngressInventory(IngressPowerCubeL4.class, true);
	}

	public int countIngressPowerCubeL5() {
		return this.countIngressInventory(IngressPowerCubeL5.class, true);
	}

	public int countIngressPowerCubeL6() {
		return this.countIngressInventory(IngressPowerCubeL6.class, true);
	}

	public int countIngressPowerCubeL7() {
		return this.countIngressInventory(IngressPowerCubeL7.class, true);
	}

	public int countIngressPowerCubeL8() {
		return this.countIngressInventory(IngressPowerCubeL8.class, true);
	}

	public int countAllIngressResonators() {
		return this.countIngressInventory(IngressResonators.class, true);
	}

	public int countIngressResonatorL1() {
		return this.countIngressInventory(IngressResonatorL1.class, true);
	}

	public int countIngressResonatorL2() {
		return this.countIngressInventory(IngressResonatorL2.class, true);
	}

	public int countIngressResonatorL3() {
		return this.countIngressInventory(IngressResonatorL3.class, true);
	}

	public int countIngressResonatorL4() {
		return this.countIngressInventory(IngressResonatorL4.class, true);
	}

	public int countIngressResonatorL5() {
		return this.countIngressInventory(IngressResonatorL5.class, true);
	}

	public int countIngressResonatorL6() {
		return this.countIngressInventory(IngressResonatorL6.class, true);
	}

	public int countIngressResonatorL7() {
		return this.countIngressInventory(IngressResonatorL7.class, true);
	}

	public int countIngressResonatorL8() {
		return this.countIngressInventory(IngressResonatorL8.class, true);
	}

	public int countAllIngressWeapons() {
		return this.countIngressInventory(IngressWeapons.class, true) + this.countIngressAXAShield(true) + this.countIngressJarvisVirus(true);
	}

	public int countAllIngressXMPBursters() {
		return this.countIngressInventory(IngressXMPBursters.class, true);
	}

	public int countIngressXMPBursterL1() {
		return this.countIngressInventory(IngressXMPBursterL1.class, true);
	}

	public int countIngressXMPBursterL2() {
		return this.countIngressInventory(IngressXMPBursterL2.class, true);
	}

	public int countIngressXMPBursterL3() {
		return this.countIngressInventory(IngressXMPBursterL3.class, true);
	}

	public int countIngressXMPBursterL4() {
		return this.countIngressInventory(IngressXMPBursterL4.class, true);
	}

	public int countIngressXMPBursterL5() {
		return this.countIngressInventory(IngressXMPBursterL5.class, true);
	}

	public int countIngressXMPBursterL6() {
		return this.countIngressInventory(IngressXMPBursterL6.class, true);
	}

	public int countIngressXMPBursterL7() {
		return this.countIngressInventory(IngressXMPBursterL7.class, true);
	}

	public int countIngressXMPBursterL8() {
		return this.countIngressInventory(IngressXMPBursterL8.class, true);
	}

	public int countAllIngressUltraStrikes() {
		return this.countIngressInventory(IngressUltraStrikes.class, true);
	}

	public int countIngressUltraStrikeL1() {
		return this.countIngressInventory(IngressUltraStrikeL1.class, true);
	}

	public int countIngressUltraStrikeL2() {
		return this.countIngressInventory(IngressUltraStrikeL2.class, true);
	}

	public int countIngressUltraStrikeL3() {
		return this.countIngressInventory(IngressUltraStrikeL3.class, true);
	}

	public int countIngressUltraStrikeL4() {
		return this.countIngressInventory(IngressUltraStrikeL4.class, true);
	}

	public int countIngressUltraStrikeL5() {
		return this.countIngressInventory(IngressUltraStrikeL5.class, true);
	}

	public int countIngressUltraStrikeL6() {
		return this.countIngressInventory(IngressUltraStrikeL6.class, true);
	}

	public int countIngressUltraStrikeL7() {
		return this.countIngressInventory(IngressUltraStrikeL7.class, true);
	}

	public int countIngressUltraStrikeL8() {
		return this.countIngressInventory(IngressUltraStrikeL8.class, true);
	}

	public int countIngressAdaRefactor() {
		return this.countIngressInventory(IngressAdaRefactor.class, true);
	}

	public int countIngressJarvisVirus() {
		return this.countIngressInventory(IngressJarvisVirus.class, true);
	}

	public IngressInventory[] listIngressInventories() {
		ArrayList<IngressInventory> ingressInventories = this.ingressInventoryContainer.getAll();
		return ingressInventories.toArray(new IngressInventory[ingressInventories.size()]);
	}

	private IngressInventory[] listIngressInventories(Class clazz) {
		ArrayList<IngressInventory> ingressInventories = new ArrayList<>();
		for (IngressInventory ingressInventory : this.ingressInventoryContainer.getAll()) {
			if (clazz.isInstance(ingressInventory)) {
				ingressInventories.add(ingressInventory);
			}
		}
		return ingressInventories.toArray(new IngressInventory[ingressInventories.size()]);
	}

	public IngressMUFGCapsule[] listIngressMUFGCapsule() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressMUFGCapsule.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressMUFGCapsule[].class);
	}

	public IngressCapsule[] listIngressCapsule() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressCapsule.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressCapsule[].class);
	}

	public IngressMedia[] listIngressMedia() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressMedia.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressMedia[].class);
	}

	public IngressShieldCommon[] listIngressShieldCommon() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressShieldCommon.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressShieldCommon[].class);
	}

	public IngressShieldRare[] listIngressShieldRare() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressShieldRare.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressShieldRare[].class);
	}

	public IngressShieldVeryRare[] listIngressShieldVeryRare() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressShieldVeryRare.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressShieldVeryRare[].class);
	}

	public IngressLinkAmpRare[] listIngressLinkAmpRare() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressLinkAmpRare.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressLinkAmpRare[].class);
	}

	public IngressLinkAmpVeryRare[] listIngressLinkAmpVeryRare() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressLinkAmpVeryRare.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressLinkAmpVeryRare[].class);
	}

	public IngressSoftBankUltraLink[] listIngressSoftBankUltraLink() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressSoftBankUltraLink.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressSoftBankUltraLink[].class);
	}

	public IngressHeatSinkCommon[] listIngressHeatSinkCommon() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressHeatSinkCommon.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressHeatSinkCommon[].class);
	}

	public IngressHeatSinkRare[] listIngressHeatSinkRare() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressHeatSinkRare.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressHeatSinkRare[].class);
	}

	public IngressHeatSinkVeryRare[] listIngressHeatSinkVeryRare() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressHeatSinkVeryRare.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressHeatSinkVeryRare[].class);
	}

	public IngressMultihackCommon[] listIngressMultihackCommon() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressMultihackCommon.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressMultihackCommon[].class);
	}

	public IngressForceAmp[] listIngressForceAmp() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressForceAmp.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressForceAmp[].class);
	}

	public IngressTurret[] listIngressTurret() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressTurret.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressTurret[].class);
	}

	public IngressPortalKey[] listIngressPortalKey() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressPortalKey.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressPortalKey[].class);
	}

	public IngressLawsonPowerCube[] listIngressLawsonPowerCubeL1() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressLawsonPowerCube.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressLawsonPowerCube[].class);
	}

	public IngressPowerCubeL1[] listIngressPowerCubeL1() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressPowerCubeL1.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressPowerCubeL1[].class);
	}

	public IngressPowerCubeL2[] listIngressPowerCubeL2() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressPowerCubeL2.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressPowerCubeL2[].class);
	}

	public IngressPowerCubeL3[] listIngressPowerCubeL3() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressPowerCubeL3.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressPowerCubeL3[].class);
	}

	public IngressPowerCubeL4[] listIngressPowerCubeL4() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressPowerCubeL4.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressPowerCubeL4[].class);
	}

	public IngressPowerCubeL5[] listIngressPowerCubeL5() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressPowerCubeL5.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressPowerCubeL5[].class);
	}

	public IngressPowerCubeL6[] listIngressPowerCubeL6() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressPowerCubeL6.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressPowerCubeL6[].class);
	}

	public IngressPowerCubeL7[] listIngressPowerCubeL7() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressPowerCubeL7.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressPowerCubeL7[].class);
	}

	public IngressPowerCubeL8[] listIngressPowerCubeL8() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressPowerCubeL8.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressPowerCubeL8[].class);
	}

	public IngressResonatorL1[] listIngressResonatorL1() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressResonatorL1.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressResonatorL1[].class);
	}

	public IngressResonatorL2[] listIngressResonatorL2() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressResonatorL2.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressResonatorL2[].class);
	}

	public IngressResonatorL3[] listIngressResonatorL3() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressResonatorL3.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressResonatorL3[].class);
	}

	public IngressResonatorL4[] listIngressResonatorL4() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressResonatorL4.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressResonatorL4[].class);
	}

	public IngressResonatorL5[] listIngressResonatorL5() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressResonatorL5.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressResonatorL5[].class);
	}

	public IngressResonatorL6[] listIngressResonatorL6() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressResonatorL6.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressResonatorL6[].class);
	}

	public IngressResonatorL7[] listIngressResonatorL7() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressResonatorL7.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressResonatorL7[].class);
	}

	public IngressResonatorL8[] listIngressResonatorL8() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressResonatorL8.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressResonatorL8[].class);
	}

	public IngressXMPBursterL1[] listIngressXMPBursterL1() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressXMPBursterL1.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressXMPBursterL1[].class);
	}

	public IngressXMPBursterL2[] listIngressXMPBursterL2() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressXMPBursterL2.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressXMPBursterL2[].class);
	}

	public IngressXMPBursterL3[] listIngressXMPBursterL3() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressXMPBursterL3.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressXMPBursterL3[].class);
	}

	public IngressXMPBursterL4[] listIngressXMPBursterL4() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressXMPBursterL4.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressXMPBursterL4[].class);
	}

	public IngressXMPBursterL5[] listIngressXMPBursterL5() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressXMPBursterL5.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressXMPBursterL5[].class);
	}

	public IngressXMPBursterL6[] listIngressXMPBursterL6() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressXMPBursterL6.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressXMPBursterL6[].class);
	}

	public IngressXMPBursterL7[] listIngressXMPBursterL7() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressXMPBursterL7.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressXMPBursterL7[].class);
	}

	public IngressXMPBursterL8[] listIngressXMPBursterL8() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressXMPBursterL8.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressXMPBursterL8[].class);
	}

	public IngressUltraStrikeL1[] listIngressUltraStrikeL1() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressUltraStrikeL1.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressUltraStrikeL1[].class);
	}

	public IngressUltraStrikeL2[] listIngressUltraStrikeL2() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressUltraStrikeL2.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressUltraStrikeL2[].class);
	}

	public IngressUltraStrikeL3[] listIngressUltraStrikeL3() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressUltraStrikeL3.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressUltraStrikeL3[].class);
	}

	public IngressUltraStrikeL4[] listIngressUltraStrikeL4() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressUltraStrikeL4.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressUltraStrikeL4[].class);
	}

	public IngressUltraStrikeL5[] listIngressUltraStrikeL5() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressUltraStrikeL5.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressUltraStrikeL5[].class);
	}

	public IngressUltraStrikeL6[] listIngressUltraStrikeL6() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressUltraStrikeL6.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressUltraStrikeL6[].class);
	}

	public IngressUltraStrikeL7[] listIngressUltraStrikeL7() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressUltraStrikeL7.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressUltraStrikeL7[].class);
	}

	public IngressUltraStrikeL8[] listIngressUltraStrikeL8() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressUltraStrikeL8.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressUltraStrikeL8[].class);
	}

	public IngressAdaRefactor[] listIngressAdaRefactor() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressAdaRefactor.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressAdaRefactor[].class);
	}

	public IngressJarvisVirus[] listIngressJarvisVirus() {
		IngressInventory[] ingressInventories = this.listIngressInventories(IngressJarvisVirus.class);
		return Arrays.copyOf(ingressInventories, ingressInventories.length, IngressJarvisVirus[].class);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("MUFG Capsule = ");
		sb.append(this.countIngressMUFGCapsule());
		sb.append("\nCapsule = ");
		sb.append(this.countIngressCapsule());
		sb.append("\nMedia = ");
		sb.append(this.countIngressMedia());
		sb.append("\nShield Common = ");
		sb.append(this.countIngressShieldCommon());
		sb.append("\nShield Rare = ");
		sb.append(this.countIngressShieldRare());
		sb.append("\nShield Very Rare = ");
		sb.append(this.countIngressShieldVeryRare());
		sb.append("\nLink Amp Rare = ");
		sb.append(this.countIngressLinkAmpRare());
		//sb.append("\nLink Amp Very Rare = ");
		//sb.append(this.countIngressLinkAmpVeryRare());
		sb.append("\nSoftBank Ultra Link = ");
		sb.append(this.countIngressSoftBankUltraLink());
		sb.append("\nHeat Sink Common = ");
		sb.append(this.countIngressHeatSinkCommon());
		sb.append("\nHeat Sink Rare = ");
		sb.append(this.countIngressHeatSinkRare());
		sb.append("\nHeat Sink Very Rare = ");
		sb.append(this.countIngressHeatSinkVeryRare());
		sb.append("\nMulti-hack Common = ");
		sb.append(this.countIngressMultihackCommon());
		sb.append("\nMulti-hack Rare = ");
		sb.append(this.countIngressMultihackRare());
		sb.append("\nMulti-hack Very Rare = ");
		sb.append(this.countIngressMultihackVeryRare());
		sb.append("\nForce Amp = ");
		sb.append(this.countIngressForceAmp());
		sb.append("\nTurret = ");
		sb.append(this.countIngressTurret());
		sb.append("\nPortal Key = ");
		sb.append(this.countIngressPortalKey());
		sb.append("\nPower Cube L1 = ");
		sb.append(this.countIngressPowerCubeL1());
		sb.append("\nPower Cube L2 = ");
		sb.append(this.countIngressPowerCubeL2());
		sb.append("\nPower Cube L3 = ");
		sb.append(this.countIngressPowerCubeL3());
		sb.append("\nPower Cube L4 = ");
		sb.append(this.countIngressPowerCubeL4());
		sb.append("\nPower Cube L5 = ");
		sb.append(this.countIngressPowerCubeL5());
		sb.append("\nPower Cube L6 = ");
		sb.append(this.countIngressPowerCubeL6());
		sb.append("\nPower Cube L7 = ");
		sb.append(this.countIngressPowerCubeL7());
		sb.append("\nPower Cube L8 = ");
		sb.append(this.countIngressPowerCubeL8());
		sb.append("\nResonator L1 = ");
		sb.append(this.countIngressResonatorL1());
		sb.append("\nResonator L2 = ");
		sb.append(this.countIngressResonatorL2());
		sb.append("\nResonator L3 = ");
		sb.append(this.countIngressResonatorL3());
		sb.append("\nResonator L4 = ");
		sb.append(this.countIngressResonatorL4());
		sb.append("\nResonator L5 = ");
		sb.append(this.countIngressResonatorL5());
		sb.append("\nResonator L6 = ");
		sb.append(this.countIngressResonatorL6());
		sb.append("\nResonator L7 = ");
		sb.append(this.countIngressResonatorL7());
		sb.append("\nResonator L8 = ");
		sb.append(this.countIngressResonatorL8());
		sb.append("\nXMP Burster L1 = ");
		sb.append(this.countIngressXMPBursterL1());
		sb.append("\nXMP Burster L2 = ");
		sb.append(this.countIngressXMPBursterL2());
		sb.append("\nXMP Burster L3 = ");
		sb.append(this.countIngressXMPBursterL3());
		sb.append("\nXMP Burster L4 = ");
		sb.append(this.countIngressXMPBursterL4());
		sb.append("\nXMP Burster L5 = ");
		sb.append(this.countIngressXMPBursterL5());
		sb.append("\nXMP Burster L6 = ");
		sb.append(this.countIngressXMPBursterL6());
		sb.append("\nXMP Burster L7 = ");
		sb.append(this.countIngressXMPBursterL7());
		sb.append("\nXMP Burster L8 = ");
		sb.append(this.countIngressXMPBursterL8());
		sb.append("\nUltra Strike L1 = ");
		sb.append(this.countIngressUltraStrikeL1());
		sb.append("\nUltra Strike L2 = ");
		sb.append(this.countIngressUltraStrikeL2());
		sb.append("\nUltra Strike L3 = ");
		sb.append(this.countIngressUltraStrikeL3());
		sb.append("\nUltra Strike L4 = ");
		sb.append(this.countIngressUltraStrikeL4());
		sb.append("\nUltra Strike L5 = ");
		sb.append(this.countIngressUltraStrikeL5());
		sb.append("\nUltra Strike L6 = ");
		sb.append(this.countIngressUltraStrikeL6());
		sb.append("\nUltra Strike L7 = ");
		sb.append(this.countIngressUltraStrikeL7());
		sb.append("\nUltra Strike L8 = ");
		sb.append(this.countIngressUltraStrikeL8());
		sb.append("\nAda Refactor = ");
		sb.append(this.countIngressAdaRefactor());
		sb.append("\nJarvis Virus = ");
		sb.append(this.countIngressJarvisVirus());
		return sb.toString();
	}
}
