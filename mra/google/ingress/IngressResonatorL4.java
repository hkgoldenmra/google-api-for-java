package mra.google.ingress;

public class IngressResonatorL4 extends IngressResonators {

	IngressResonatorL4(String guid, IngressAgent owner) {
		this(guid, owner, -1, -1);
	}

	IngressResonatorL4(String guid, IngressAgent owner, int remainedEnergy, int distance) {
		super(guid, owner, 4, 2500, remainedEnergy, distance);
	}
}
