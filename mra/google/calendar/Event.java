package mra.google.calendar;

import java.util.Date;
import mra.google.Google;

public class Event {

	private final String id;
	private final String title;
	private final String location;
	private final Date start;
	private final Date end;

	public Event(String title, String location, Date start, Date end) {
		this(null, title, location, start, end);
	}

	Event(String id, String title, String location, Date start, Date end) {
		this.id = id;
		this.title = title;
		this.location = location;
		this.start = start;
		this.end = end;
	}

	public String getId() {
		return this.id;
	}

	public String getTitle() {
		return this.title;
	}

	public String getLocation() {
		return this.location;
	}

	public Date getStart() {
		return this.start;
	}

	public Date getEnd() {
		return this.end;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("ID = ");
		sb.append(this.getId());
		sb.append("\nTitle = ");
		sb.append(this.getTitle());
		sb.append("\nLocation = ");
		sb.append(this.getLocation());
		sb.append("\nStart = ");
		sb.append(this.getStart() == null ? "<null>" : Google.toHumanDateTimeFormat(this.getStart()));
		sb.append("\nEnd = ");
		sb.append(this.getEnd() == null ? "<null>" : Google.toHumanDateTimeFormat(this.getEnd()));
		return sb.toString();
	}
}
