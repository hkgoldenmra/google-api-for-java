package mra.google.contact;

import java.util.Comparator;

class ContactEntryComparator implements Comparator<ContactEntry> {

	public static final char SORT_BY_FULL_NAME = 'N';
	public static final char SORT_BY_NAME_PREFIX = 'P';
	public static final char SORT_BY_GIVEN_NAME = 'G';
	public static final char SORT_BY_ADDITIONAL_NAME = 'A';
	public static final char SORT_BY_FAMILY_NAME = 'F';
	public static final char SORT_BY_NAME_SUFFIX = 'S';
	public static final char ORDER_BY_ASCENDING = 'A';
	public static final char ORDER_BY_DESCENDING = 'D';
	private char sortBy;
	private char orderBy;

	public ContactEntryComparator(char sortBy, char orderBy) {
		switch (sortBy) {
			default: {
				this.sortBy = ContactEntryComparator.SORT_BY_FULL_NAME;
			}
		}
		switch (orderBy) {
			case ContactEntryComparator.ORDER_BY_DESCENDING: {
				this.orderBy = orderBy;
			}
			break;
			default: {
				this.orderBy = ContactEntryComparator.ORDER_BY_ASCENDING;
			}
		}
	}

	public ContactEntryComparator() {
		this(ContactEntryComparator.SORT_BY_FULL_NAME, ContactEntryComparator.ORDER_BY_ASCENDING);
	}

	private int nameCompare(String name1, String name2) {
		if (name1 == null) {
			name1 = "";
		}
		if (name2 == null) {
			name2 = "";
		}
		switch (this.orderBy) {
			case ContactEntryComparator.ORDER_BY_DESCENDING: {
				return name2.compareTo(name1);
			}
			default: {
				return name1.compareTo(name2);
			}
		}
	}

	@Override
	public int compare(ContactEntry t1, ContactEntry t2) {
		switch (this.sortBy) {
			case ContactEntryComparator.SORT_BY_NAME_PREFIX: {
				return this.nameCompare(t1.getNamePrefix(), t2.getNamePrefix());
			}
			case ContactEntryComparator.SORT_BY_GIVEN_NAME: {
				return this.nameCompare(t1.getGivenName(), t2.getGivenName());
			}
			case ContactEntryComparator.SORT_BY_ADDITIONAL_NAME: {
				return this.nameCompare(t1.getAdditionalName(), t2.getAdditionalName());
			}
			case ContactEntryComparator.SORT_BY_FAMILY_NAME: {
				return this.nameCompare(t1.getFamilyName(), t2.getFamilyName());
			}
			case ContactEntryComparator.SORT_BY_NAME_SUFFIX: {
				return this.nameCompare(t1.getNameSuffix(), t2.getNameSuffix());
			}
			default: {
				return this.nameCompare(t1.getFullName(), t2.getFullName());
			}
		}
	}
}
