package mra.google.contact;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import mra.google.GoogleAuthorization;
import mra.http.HttpRequest;
import mra.xml.XmlParser;
import mra.xml.XmlString;
import mra.xml.XmlTag;

public class ContactGroup {

	private ContactBook contactBook;
	private String eTag;
	private String id;
	private String title;

	static String createContactGroupXML(String title) {
		XmlTag xmlRoot = new XmlTag("atom:entry");
		xmlRoot.setXmlAttribute("xmlns:atom", "http://www.w3.org/2005/Atom");
		xmlRoot.setXmlAttribute("xmlns:gd", "http://schemas.google.com/g/2005");
		{
			XmlTag xmlChild = new XmlTag("atom:title");
			xmlChild.setXmlAttribute("type", "text");
			xmlChild.addXmlObject(new XmlString(title));
			xmlRoot.addXmlObject(xmlChild);
		}
		{
			XmlTag xmlChild = new XmlTag("atom:category");
			xmlChild.setXmlAttribute("scheme", "http://schemas.google.com/g/2005#kind");
			xmlChild.setXmlAttribute("term", "http://schemas.google.com/contact/2008#group");
			xmlRoot.addXmlObject(xmlChild);
		}
		return xmlRoot.toString();
	}

	ContactGroup(ContactBook contactBook, XmlTag xmlTag) {
		this.contactBook = contactBook;
		try {
			this.eTag = xmlTag.getXmlAttribute("gd:etag");
		} catch (Exception ex) {
		}
		try {
			String id = xmlTag.getXmlTags("id")[0].getInnerString();
			int index = id.lastIndexOf('/');
			this.id = id.substring(index + 1);
		} catch (Exception ex) {
		}
		try {
			this.title = xmlTag.getXmlTags("title")[0].getInnerString();
		} catch (Exception ex) {
		}
	}

	ContactBook getContactBook() {
		return this.contactBook;
	}

	public String getId() {
		return this.id;
	}

	public String getTitle() {
		return this.title;
	}

	public ContactEntry[] listContactEntries() throws IOException {
		return this.listContactEntries(ContactEntryComparator.SORT_BY_FULL_NAME, ContactEntryComparator.ORDER_BY_ASCENDING);
	}

	public ContactEntry[] listContactEntries(char sortBy, char orderBy) throws IOException {
		ArrayList<ContactEntry> contactEntries = new ArrayList<>();
		ContactBook contactBook = this.getContactBook();
		GoogleAuthorization googleAuthorization = contactBook.getGoogleAuthorization();
		URL url = new URL(String.format("%1$s/contacts/%2$s/full?max-results=%3$d&group=%1$s/groups/%2$s/base/%4$s", "https://www.google.com/m8/feeds", this.getContactBook().getUserEmail(), Integer.MAX_VALUE, this.getId()));
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		if (googleAuthorization != null) {
			Map<String, String> requestHeaders = new LinkedHashMap<>();
			requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_WWW_FORM + "; charset=" + HttpRequest.CHARSET_UTF8);
			requestHeaders.put("Authorization", googleAuthorization.getToken());
			requestHeaders.put("GData-Version", "3");
			httpRequest.setRequestHeaders(requestHeaders);
		}
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		XmlTag[] entries = xmlTag.getXmlTags("entry");
		for (XmlTag entry : entries) {
			contactEntries.add(new ContactEntry(contactBook, entry));
		}
		ContactEntry[] ces = contactEntries.toArray(new ContactEntry[contactEntries.size()]);
		Arrays.sort(ces, new ContactEntryComparator(sortBy, orderBy));
		return ces;
	}

	public void update(String title) throws IOException {
		ContactBook contactBook = this.getContactBook();
		String userEmail = contactBook.getUserEmail();
		GoogleAuthorization googleAuthorization = contactBook.getGoogleAuthorization();
		URL url = new URL("https://www.google.com/m8/feeds/groups/" + userEmail + "/full/" + this.getId());
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_PUT);
		if (googleAuthorization != null) {
			Map<String, String> requestHeaders = new LinkedHashMap<>();
			requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_ATOM_XML + "; charset=" + HttpRequest.CHARSET_UTF8);
			requestHeaders.put("Authorization", googleAuthorization.getToken());
			requestHeaders.put("GData-Version", "3");
			requestHeaders.put("If-Match", this.eTag);
			httpRequest.setRequestHeaders(requestHeaders);
		}
		if (title != null) {
			this.title = title;
		}
		String requestData = ContactGroup.createContactGroupXML(this.getTitle());
		httpRequest.setRequestData(requestData);
		httpRequest.send();
	}

	public void updateTitle(String title) throws IOException {
		this.update(title);
	}

	public void delete() throws IOException {
		ContactBook contactBook = this.getContactBook();
		String userEmail = contactBook.getUserEmail();
		GoogleAuthorization googleAuthorization = contactBook.getGoogleAuthorization();
		URL url = new URL("https://www.google.com/m8/feeds/groups/" + userEmail + "/full/" + this.getId());
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_DELETE);
		if (googleAuthorization != null) {
			Map<String, String> requestHeaders = new LinkedHashMap<>();
			requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_ATOM_XML + "; charset=" + HttpRequest.CHARSET_UTF8);
			requestHeaders.put("Authorization", googleAuthorization.getToken());
			requestHeaders.put("GData-Version", "3");
			requestHeaders.put("If-Match", this.eTag);
			httpRequest.setRequestHeaders(requestHeaders);
		}
		httpRequest.send();
		this.contactBook = null;
		this.id = null;
		this.title = null;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("ID = ");
		sb.append(this.getId());
		sb.append("\nTitle = ");
		sb.append(this.getTitle());
		return sb.toString();
	}
}
