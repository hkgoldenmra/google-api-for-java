package mra.google.contact;

public class ContactPhone {

	private final boolean isPrimary;
	private final String type;
	private final String number;

	public ContactPhone(boolean isPrimary, String type, String number) {
		this.isPrimary = isPrimary;
		this.type = type;
		this.number = number;
	}

	public boolean isPrimary() {
		return this.isPrimary;
	}

	public String getType() {
		return this.type;
	}

	public String getNumber() {
		return this.number;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (this.isPrimary()) {
			sb.append("*");
		}
		sb.append("[");
		sb.append(this.getType());
		sb.append("]");
		sb.append(this.getNumber());
		return sb.toString();
	}
}
