package mra.google.contact;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import mra.google.GoogleAuthorization;
import mra.http.HttpRequest;
import mra.xml.XmlString;
import mra.xml.XmlTag;

public class ContactEntry {

	public static final char SORT_BY_FULL_NAME = 'N';
	public static final char SORT_BY_NAME_PREFIX = 'P';
	public static final char SORT_BY_GIVEN_NAME = 'G';
	public static final char SORT_BY_ADDITIONAL_NAME = 'A';
	public static final char SORT_BY_FAMILY_NAME = 'F';
	public static final char SORT_BY_NAME_SUFFIX = 'S';
	public static final char ORDER_BY_ASCENDING = 'A';
	public static final char ORDER_BY_DESCENDING = 'D';

	public static final String TYPE_HOME = "http://schemas.google.com/g/2005#home";
	public static final String TYPE_WORK = "http://schemas.google.com/g/2005#work";
	public static final String TYPE_OTHER = "http://schemas.google.com/g/2005#other";
	public static final String TYPE_MOBILE = "http://schemas.google.com/g/2005#mobile";
	public static final String TYPE_MAIN = "http://schemas.google.com/g/2005#main";
	public static final String TYPE_HOME_FAX = "http://schemas.google.com/g/2005#home_fax";
	public static final String TYPE_WORK_FAX = "http://schemas.google.com/g/2005#work_fax";
	public static final String TYPE_PAGER = "http://schemas.google.com/g/2005#pager";

	static String createContactEntryXML(String namePrefix, String givenName, String additionalName, String familyName, String nameSuffix, ContactEmail[] contactEmails, ContactPhone[] contactPhones, ContactAddress[] contactAddresses, ContactGroup[] contactGroups) {
		XmlTag xmlRoot = new XmlTag("atom:entry");
		xmlRoot.setXmlAttribute("xmlns:atom", "http://www.w3.org/2005/Atom");
		xmlRoot.setXmlAttribute("xmlns:gd", "http://schemas.google.com/g/2005");
		xmlRoot.setXmlAttribute("xmlns:gContact", "http://schemas.google.com/contact/2008");
		{
			XmlTag xmlChild = new XmlTag("gd:name");
			if (namePrefix != null) {
				XmlTag xmlSubChild = new XmlTag("gd:namePrefix");
				xmlSubChild.addXmlObject(new XmlString(namePrefix));
				xmlChild.addXmlObject(xmlSubChild);
			}
			if (givenName != null) {
				XmlTag xmlSubChild = new XmlTag("gd:givenName");
				xmlSubChild.addXmlObject(new XmlString(givenName));
				xmlChild.addXmlObject(xmlSubChild);
			}
			if (additionalName != null) {
				XmlTag xmlSubChild = new XmlTag("gd:additionalName");
				xmlSubChild.addXmlObject(new XmlString(additionalName));
				xmlChild.addXmlObject(xmlSubChild);
			}
			if (familyName != null) {
				XmlTag xmlSubChild = new XmlTag("gd:familyName");
				xmlSubChild.addXmlObject(new XmlString(familyName));
				xmlChild.addXmlObject(xmlSubChild);
			}
			if (nameSuffix != null) {
				XmlTag xmlSubChild = new XmlTag("gd:nameSuffix");
				xmlSubChild.addXmlObject(new XmlString(nameSuffix));
				xmlChild.addXmlObject(xmlSubChild);
			}
			xmlRoot.addXmlObject(xmlChild);
		}
		if (contactEmails != null) {
			for (ContactEmail contactEmail : contactEmails) {
				if (contactEmail != null) {
					XmlTag xmlChild = new XmlTag("gd:email");
					xmlChild.setXmlAttribute("rel", contactEmail.getType());
					if (contactEmail.isPrimary()) {
						xmlChild.setXmlAttribute("primary", "true");
					}
					xmlChild.setXmlAttribute("address", contactEmail.getEmail());
					xmlRoot.addXmlObject(xmlChild);
				}
			}
		}
		if (contactPhones != null) {
			for (ContactPhone contactPhone : contactPhones) {
				if (contactPhone != null) {
					XmlTag xmlChild = new XmlTag("gd:phoneNumber");
					xmlChild.setXmlAttribute("rel", contactPhone.getType());
					if (contactPhone.isPrimary()) {
						xmlChild.setXmlAttribute("primary", "true");
					}
					xmlChild.addXmlObject(new XmlString(contactPhone.getNumber()));
					xmlRoot.addXmlObject(xmlChild);
				}
			}
		}
		if (contactAddresses != null) {
			for (ContactAddress contactAddress : contactAddresses) {
				if (contactAddress != null) {
					XmlTag xmlChild = new XmlTag("gd:structuredPostalAddress");
					if (contactAddress.isPrimary()) {
						xmlChild.setXmlAttribute("primary", "true");
					}
					if (namePrefix != null) {
						XmlTag xmlSubChild = new XmlTag("gd:street");
						xmlSubChild.addXmlObject(new XmlString(contactAddress.getStreet()));
						xmlChild.addXmlObject(xmlSubChild);
					}
					if (givenName != null) {
						XmlTag xmlSubChild = new XmlTag("gd:city");
						xmlSubChild.addXmlObject(new XmlString(contactAddress.getCity()));
						xmlChild.addXmlObject(xmlSubChild);
					}
					if (additionalName != null) {
						XmlTag xmlSubChild = new XmlTag("gd:region");
						xmlSubChild.addXmlObject(new XmlString(contactAddress.getRegion()));
						xmlChild.addXmlObject(xmlSubChild);
					}
					if (familyName != null) {
						XmlTag xmlSubChild = new XmlTag("gd:postcode");
						xmlSubChild.addXmlObject(new XmlString(contactAddress.getPostcode()));
						xmlChild.addXmlObject(xmlSubChild);
					}
					if (nameSuffix != null) {
						XmlTag xmlSubChild = new XmlTag("gd:country");
						xmlSubChild.addXmlObject(new XmlString(contactAddress.getCountry()));
						xmlChild.addXmlObject(xmlSubChild);
					}
					xmlRoot.addXmlObject(xmlChild);
				}
			}
		}
		if (contactGroups != null) {
			for (ContactGroup contactGroup : contactGroups) {
				if (contactGroup != null) {
					XmlTag xmlChild = new XmlTag("gContact:groupMembershipInfo");
					xmlChild.setXmlAttribute("deleted", "false");
					xmlChild.setXmlAttribute("href", String.format("http://www.google.com/m8/feeds/groups/%s/base/%s", contactGroup.getContactBook().getUserEmail(), contactGroup.getId()));
					xmlRoot.addXmlObject(xmlChild);
				}
			}
		}
		{
			XmlTag xmlChild = new XmlTag("atom:category");
			xmlChild.setXmlAttribute("scheme", "http://schemas.google.com/g/2005#kind");
			xmlChild.setXmlAttribute("term", "http://schemas.google.com/contact/2008#contact");
			xmlRoot.addXmlObject(xmlChild);
		}
		return xmlRoot.toString();
	}

	private ContactBook contactBook;
	private String eTag;
	private String id;
	private String fullName;
	private String namePrefix;
	private String givenName;
	private String additionalName;
	private String familyName;
	private String nameSuffix;
	private ArrayList<ContactPhone> contactPhones = new ArrayList<>();
	private ArrayList<ContactEmail> contactEmails = new ArrayList<>();
	private ArrayList<ContactAddress> contactAddresses = new ArrayList<>();
	private ArrayList<ContactGroup> contactGroups = new ArrayList<>();

	ContactBook getContactBook() {
		return this.contactBook;
	}

	public String getId() {
		return this.id;
	}

	public String getFullName() {
		return this.fullName;
	}

	public String getNamePrefix() {
		return this.namePrefix;
	}

	public String getGivenName() {
		return this.givenName;
	}

	public String getAdditionalName() {
		return this.additionalName;
	}

	public String getFamilyName() {
		return this.familyName;
	}

	public String getNameSuffix() {
		return this.nameSuffix;
	}

	public ContactPhone[] getContactPhones() {
		return this.contactPhones.toArray(new ContactPhone[this.contactPhones.size()]);
	}

	public ContactEmail[] getContactEmails() {
		return this.contactEmails.toArray(new ContactEmail[this.contactEmails.size()]);
	}

	public ContactAddress[] getContactAddresses() {
		return this.contactAddresses.toArray(new ContactAddress[this.contactAddresses.size()]);
	}

	public ContactGroup[] getContactGroups() {
		return this.contactGroups.toArray(new ContactGroup[this.contactGroups.size()]);
	}

	ContactEntry(ContactBook contactBook, XmlTag xmlTag) {
		this.contactBook = contactBook;
		try {
			this.eTag = xmlTag.getXmlAttribute("gd:etag");
		} catch (Exception ex) {
		}
		try {
			String id = xmlTag.getXmlTags("id")[0].getInnerString();
			int index = id.lastIndexOf('/');
			this.id = id.substring(index + 1);
		} catch (Exception ex) {
		}
		try {
			this.fullName = xmlTag.getXmlTags("gd:name")[0].getXmlTags("gd:fullName")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			this.namePrefix = xmlTag.getXmlTags("gd:name")[0].getXmlTags("gd:namePrefix")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			this.givenName = xmlTag.getXmlTags("gd:name")[0].getXmlTags("gd:givenName")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			this.additionalName = xmlTag.getXmlTags("gd:name")[0].getXmlTags("gd:additionalName")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			this.familyName = xmlTag.getXmlTags("gd:name")[0].getXmlTags("gd:familyName")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			this.nameSuffix = xmlTag.getXmlTags("gd:name")[0].getXmlTags("gd:nameSuffix")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			XmlTag[] phones = xmlTag.getXmlTags("gd:phoneNumber");
			for (XmlTag phone : phones) {
				String type = phone.getXmlAttribute("label");
				if (type == null) {
					type = phone.getXmlAttribute("rel");
				}
				String number = phone.getInnerString();
				String primary = phone.getXmlAttribute("primary");
				boolean isPrimary = (primary != null && primary.equals("true"));
				this.contactPhones.add(new ContactPhone(isPrimary, type, number));
			}
		} catch (Exception ex) {
		}
		try {
			XmlTag[] emails = xmlTag.getXmlTags("gd:email");
			for (XmlTag email : emails) {
				String type = email.getXmlAttribute("rel");
				String em = email.getXmlAttribute("address");
				String primary = email.getXmlAttribute("primary");
				boolean isPrimary = (primary != null && primary.equals("true"));
				this.contactEmails.add(new ContactEmail(isPrimary, type, em));
			}
		} catch (Exception ex) {
		}
		try {
			XmlTag[] addresss = xmlTag.getXmlTags("gd:structuredPostalAddress");
			for (XmlTag address : addresss) {
				String type = address.getXmlAttribute("rel");
				String formattedAddress = address.getXmlTags("gd:formattedAddress")[0].getInnerString().replaceAll("\n", "");
				String street = address.getXmlTags("gd:street")[0].getInnerString();
				String city = address.getXmlTags("gd:city")[0].getInnerString();
				String region = address.getXmlTags("gd:region")[0].getInnerString();
				String postcode = address.getXmlTags("gd:postcode")[0].getInnerString();
				String country = address.getXmlTags("gd:country")[0].getInnerString();
				String primary = address.getXmlAttribute("primary");
				boolean isPrimary = (primary != null && primary.equals("true"));
				this.contactAddresses.add(new ContactAddress(isPrimary, type, formattedAddress, street, city, region, postcode, country));
			}
		} catch (Exception ex) {
		}
		try {
			XmlTag[] groups = xmlTag.getXmlTags("gContact:groupMembershipInfo");
			for (XmlTag group : groups) {
				String href = group.getXmlAttribute("href");
				int index = href.lastIndexOf('/');
				String id = href.substring(index + 1);
				this.contactGroups.add(this.getContactBook().getContactGroup(id));
			}
		} catch (Exception ex) {
		}
	}

	public void update(String namePrefix, String givenName, String additionalName, String familyName, String nameSuffix, ContactEmail[] contactEmails, ContactPhone[] contactPhones, ContactAddress[] contactAddresses, ContactGroup[] contactGroups) throws IOException {
		ContactBook contactBook = this.getContactBook();
		String userEmail = contactBook.getUserEmail();
		GoogleAuthorization googleAuthorization = contactBook.getGoogleAuthorization();
		URL url = new URL("https://www.google.com/m8/feeds/contacts/" + userEmail + "/full/" + this.getId());
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_PUT);
		if (googleAuthorization != null) {
			Map<String, String> requestHeaders = new LinkedHashMap<>();
			requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_ATOM_XML + "; charset=" + HttpRequest.CHARSET_UTF8);
			requestHeaders.put("Authorization", googleAuthorization.getToken());
			requestHeaders.put("GData-Version", "3");
			requestHeaders.put("If-Match", this.eTag);
			httpRequest.setRequestHeaders(requestHeaders);
		}
		if (namePrefix != null) {
			this.namePrefix = namePrefix;
		}
		if (givenName != null) {
			this.givenName = givenName;
		}
		if (additionalName != null) {
			this.additionalName = additionalName;
		}
		if (familyName != null) {
			this.familyName = familyName;
		}
		if (nameSuffix != null) {
			this.nameSuffix = nameSuffix;
		}
		if (contactEmails != null) {
			this.contactEmails = new ArrayList<>(Arrays.asList(contactEmails));
		}
		if (contactPhones != null) {
			this.contactPhones = new ArrayList<>(Arrays.asList(contactPhones));
		}
		if (contactAddresses != null) {
			this.contactAddresses = new ArrayList<>(Arrays.asList(contactAddresses));
		}
		if (contactGroups != null) {
			this.contactGroups = new ArrayList<>(Arrays.asList(contactGroups));
		}
		String requestData = ContactEntry.createContactEntryXML(this.getNamePrefix(), this.getGivenName(), this.getAdditionalName(), this.getFamilyName(), this.getNameSuffix(), this.getContactEmails(), this.getContactPhones(), this.getContactAddresses(), this.getContactGroups());
		httpRequest.setRequestData(requestData);
		httpRequest.send();
	}

	public void updateNamePrefix(String namePrefix) throws IOException {
		this.update(namePrefix, null, null, null, null, null, null, null, null);
	}

	public void updateGivenName(String givenName) throws IOException {
		this.update(null, givenName, null, null, null, null, null, null, null);
	}

	public void updateAdditionalName(String additionalName) throws IOException {
		this.update(null, null, additionalName, null, null, null, null, null, null);
	}

	public void updateFamilyName(String familyName) throws IOException {
		this.update(null, null, null, familyName, null, null, null, null, null);
	}

	public void updateNameSuffix(String nameSuffix) throws IOException {
		this.update(null, null, null, null, nameSuffix, null, null, null, null);
	}

	public void updateContactEmails(ContactEmail[] contactEmails) throws IOException {
		this.update(null, null, null, null, null, contactEmails, null, null, null);
	}

	public void updateContactPhones(ContactPhone[] contactPhones) throws IOException {
		this.update(null, null, null, null, null, null, contactPhones, null, null);
	}

	public void updateContactAddresses(ContactAddress[] contactAddresses) throws IOException {
		this.update(null, null, null, null, null, null, null, contactAddresses, null);
	}

	public void updateContactGroup(ContactGroup[] contactGroups) throws IOException {
		this.update(null, null, null, null, null, null, null, null, contactGroups);
	}

	public void delete() throws IOException {
		ContactBook contactBook = this.getContactBook();
		String userEmail = contactBook.getUserEmail();
		GoogleAuthorization googleAuthorization = contactBook.getGoogleAuthorization();
		URL url = new URL("https://www.google.com/m8/feeds/contacts/" + userEmail + "/full/" + this.getId());
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_DELETE);
		if (googleAuthorization != null) {
			Map<String, String> requestHeaders = new LinkedHashMap<>();
			requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_ATOM_XML + "; charset=" + HttpRequest.CHARSET_UTF8);
			requestHeaders.put("Authorization", googleAuthorization.getToken());
			requestHeaders.put("GData-Version", "3");
			requestHeaders.put("If-Match", this.eTag);
			httpRequest.setRequestHeaders(requestHeaders);
		}
		httpRequest.send();
		this.contactBook = null;
		this.namePrefix = null;
		this.givenName = null;
		this.additionalName = null;
		this.familyName = null;
		this.nameSuffix = null;
		this.contactEmails = null;
		this.contactPhones = null;
		this.contactAddresses = null;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("ID = ");
		sb.append(this.getId());
		sb.append("\nFull Name = ");
		sb.append(this.getFullName());
		sb.append("\nTelephone = ");
		ContactPhone[] contactPhones = this.getContactPhones();
		for (int i = 0; i < contactPhones.length; i++) {
			if (i > 0) {
				sb.append(", ");
			}
			sb.append(contactPhones[i].toString());
		}
		sb.append("\nE-Mail = ");
		ContactEmail[] contactEmails = this.getContactEmails();
		for (int i = 0; i < contactEmails.length; i++) {
			if (i > 0) {
				sb.append(", ");
			}
			sb.append(contactEmails[i].toString());
		}
		sb.append("\nAddress = ");
		ContactAddress[] contactAddresses = this.getContactAddresses();
		for (int i = 0; i < contactAddresses.length; i++) {
			if (i > 0) {
				sb.append(", ");
			}
			sb.append(contactAddresses[i].toString());
		}
		sb.append("\nGroup = ");
		ContactGroup[] contactGroups = this.getContactGroups();
		for (int i = 0; i < contactGroups.length; i++) {
			if (i > 0) {
				sb.append(", ");
			}
			sb.append(contactGroups[i].toString());
		}
		return sb.toString();
	}
}
