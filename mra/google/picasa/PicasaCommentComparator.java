package mra.google.picasa;

import java.util.Comparator;

public class PicasaCommentComparator implements Comparator<PicasaComment> {

	public static final char SORT_BY_LAST_MODIFIED = 'M';
	public static final char ORDER_BY_ASCENDING = 'A';
	public static final char ORDER_BY_DESCENDING = 'D';
	private char sortBy;
	private char orderBy;

	public PicasaCommentComparator(char sortBy, char orderBy) {
		switch (sortBy) {
			default: {
				this.sortBy = PicasaCommentComparator.SORT_BY_LAST_MODIFIED;
			}
		}
		switch (orderBy) {
			case PicasaCommentComparator.ORDER_BY_DESCENDING: {
				this.orderBy = orderBy;
			}
			break;
			default: {
				this.orderBy = PicasaCommentComparator.ORDER_BY_ASCENDING;
			}
		}
	}

	public PicasaCommentComparator() {
		this(PicasaCommentComparator.SORT_BY_LAST_MODIFIED, PicasaCommentComparator.ORDER_BY_DESCENDING);
	}

	@Override
	public int compare(PicasaComment t1, PicasaComment t2) {
		switch (this.sortBy) {
			default: {
				switch (this.orderBy) {
					case PicasaCommentComparator.ORDER_BY_DESCENDING: {
						return t2.getLastModified().compareTo(t1.getLastModified());
					}
					default: {
						return t1.getLastModified().compareTo(t2.getLastModified());
					}
				}
			}
		}
	}
}
