package mra.google.picasa;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import mra.http.HttpRequest;
import mra.google.Google;
import mra.google.GoogleAuthorization;
import mra.xml.XmlParser;
import mra.xml.XmlString;
import mra.xml.XmlTag;

public class PicasaAlbum {

	public static final String ACCESS_PUBLIC = "public";
	public static final String ACCESS_PROTECTED = "private";
	public static final String ACCESS_PRIVATE = "protected";

	static String createPicasaAlbumXML(String title, String description, String access, Date lastModified, URL cover, String albumId) {
		XmlTag xmlRoot = new XmlTag("entry");
		xmlRoot.setXmlAttribute("xmlns", "http://www.w3.org/2005/Atom");
		xmlRoot.setXmlAttribute("xmlns:gphoto", "http://schemas.google.com/photos/2007");
		{
			XmlTag xmlChild = new XmlTag("title");
			xmlChild.setXmlAttribute("type", "text");
			xmlChild.addXmlObject(new XmlString(title));
			xmlRoot.addXmlObject(xmlChild);
		}
		if (description != null) {
			XmlTag xmlChild = new XmlTag("summary");
			xmlChild.setXmlAttribute("type", "text");
			xmlChild.addXmlObject(new XmlString(description));
			xmlRoot.addXmlObject(xmlChild);
		}
		if (cover != null) {
			XmlTag xmlChild = new XmlTag("icon");
			xmlChild.addXmlObject(new XmlString(cover.toString()));
			xmlRoot.addXmlObject(xmlChild);
		}
		if (albumId != null) {
			XmlTag xmlChild = new XmlTag("gphoto:id");
			xmlChild.addXmlObject(new XmlString(albumId));
			xmlRoot.addXmlObject(xmlChild);
		}
		{
			XmlTag xmlChild = new XmlTag("gphoto:timestamp");
			xmlChild.addXmlObject(new XmlString(Long.toString(lastModified != null ? lastModified.getTime() : System.currentTimeMillis())));
			xmlRoot.addXmlObject(xmlChild);
		}
		if (access != null) {
			access = access.toLowerCase();
			if (!access.equals(PicasaAlbum.ACCESS_PUBLIC) && !access.equals(PicasaAlbum.ACCESS_PROTECTED)) {
				access = PicasaAlbum.ACCESS_PRIVATE;
			}
			XmlTag xmlChild = new XmlTag("gphoto:access");
			xmlChild.addXmlObject(new XmlString(access));
			xmlRoot.addXmlObject(xmlChild);
		}
		{
			XmlTag xmlChild = new XmlTag("category");
			xmlChild.setXmlAttribute("scheme", "http://schemas.google.com/g/2005#kind");
			xmlChild.setXmlAttribute("term", "http://schemas.google.com/photos/2007#album");
			xmlRoot.addXmlObject(xmlChild);
		}
		return xmlRoot.toString();
	}
	private PicasaWeb picasaWeb;
	private String albumId;
	private String title;
	private String description;
	private String access;
	private Date published;
	private Date lastModified;
	private int count;
	private URL cover;
	private URL webURL;
	private URL feedURL;
	private URL entryURL;
	private URL editURL;

	PicasaWeb getPicasaWeb() {
		return this.picasaWeb;
	}

	public String getAlbumId() {
		return this.albumId;
	}

	public String getTitle() {
		return this.title;
	}

	public String getDescription() {
		return this.description;
	}

	public String getAccess() {
		return this.access;
	}

	public Date getPublished() {
		return this.published;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public int getPhotoCount() {
		return this.count;
	}

	public URL getCover() {
		return this.cover;
	}

	public URL getWebURL() {
		return this.webURL;
	}

	URL getFeedURL() {
		return this.feedURL;
	}

	URL getEntryURL() {
		return this.entryURL;
	}

	URL getEditURL() {
		return this.editURL;
	}

	PicasaAlbum(PicasaWeb picasaWeb, XmlTag xmlTag) {
		this.picasaWeb = picasaWeb;
		try {
			String id = xmlTag.getXmlTags("id")[0].getInnerString();
			int index = id.lastIndexOf('/');
			this.albumId = id.substring(index + 1);
		} catch (Exception ex) {
		}
		try {
			this.title = xmlTag.getXmlTags("title")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			this.description = xmlTag.getXmlTags("summary")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			this.access = xmlTag.getXmlTags("rights")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			this.published = Google.datetimeStringToDate(xmlTag.getXmlTags("published")[0].getInnerString());
		} catch (Exception ex) {
		}
		try {
			this.lastModified = Google.datetimeStringToDate(xmlTag.getXmlTags("updated")[0].getInnerString());
		} catch (Exception ex) {
		}
		try {
			this.count = Integer.parseInt(xmlTag.getXmlTags("gphoto:numphotos")[0].getInnerString());
		} catch (Exception ex) {
		}
		try {
			this.cover = new URL(xmlTag.getXmlTags("icon")[0].getInnerString());
		} catch (Exception ex) {
		}
		try {
			XmlTag[] links = xmlTag.getXmlTags("link");
			for (XmlTag link : links) {
				String rel = link.getXmlAttribute("rel");
				URL href = new URL(link.getXmlAttribute("href"));
				if (rel.equals("http://schemas.google.com/g/2005#feed")) {
					this.feedURL = href;
				} else if (rel.equals("self")) {
					this.entryURL = href;
				} else if (rel.equals("alternate")) {
					this.webURL = href;
				} else if (rel.equals("edit")) {
					this.editURL = href;
				}
			}
		} catch (Exception ex) {
		}
	}

	public PicasaAlbum(String userId, String albumId) throws IOException {
		this(userId, albumId, null);
	}

	public PicasaAlbum(String userId, String albumId, GoogleAuthorization googleAuthorization) throws IOException {
		PicasaAlbum picasaAlbum = new PicasaWeb(userId, googleAuthorization).getPicasaAlbum(albumId);
		this.picasaWeb = picasaAlbum.getPicasaWeb();
		this.albumId = picasaAlbum.getAlbumId();
		this.title = picasaAlbum.getTitle();
		this.description = picasaAlbum.getDescription();
		this.access = picasaAlbum.getAccess();
		this.lastModified = picasaAlbum.getLastModified();
		this.count = picasaAlbum.getPhotoCount();
		this.cover = picasaAlbum.getCover();
		this.webURL = picasaAlbum.getWebURL();
		this.feedURL = picasaAlbum.getFeedURL();
		this.entryURL = picasaAlbum.getEntryURL();
		this.editURL = picasaAlbum.getEditURL();
	}

	public void update(String title, String description, String access, Date lastModified, URL cover) throws IOException {
		GoogleAuthorization googleAuthorization = this.getPicasaWeb().getGoogleAuthorization();
		URL url = this.getEditURL();
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_PUT);
		httpRequest.setRequestHeaders(PicasaWeb.createPicasaGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_ATOM_XML, googleAuthorization));
		if (title != null) {
			this.title = title;
		}
		if (description != null) {
			this.description = description;
		}
		if (access != null) {
			this.access = access;
		}
		if (lastModified != null) {
			this.lastModified = lastModified;
		}
		if (cover != null) {
			this.cover = cover;
		}
		String requestData = PicasaAlbum.createPicasaAlbumXML(this.getTitle(), this.getDescription(), this.getAccess(), this.getLastModified(), this.getCover(), this.getAlbumId());
		httpRequest.setRequestData(requestData);
		httpRequest.send();
	}

	public void updateTitle(String title) throws IOException {
		this.update(title, null, null, null, null);
	}

	public void updateDescription(String description) throws IOException {
		this.update(null, description, null, null, null);
	}

	public void updateAccess(String access) throws IOException {
		this.update(null, null, access, null, null);
	}

	public void updateLastModified(Date lastModified) throws IOException {
		this.update(null, null, null, lastModified, null);
	}

	public void updateCover(URL cover) throws IOException {
		this.update(null, null, null, null, cover);
	}

	public void delete() throws IOException {
		GoogleAuthorization googleAuthorization = this.getPicasaWeb().getGoogleAuthorization();
		URL url = this.getEditURL();
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_DELETE);
		httpRequest.setRequestHeaders(PicasaWeb.createPicasaGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_ATOM_XML, googleAuthorization));
		httpRequest.send();
		this.picasaWeb = null;
		this.albumId = null;
		this.title = null;
		this.description = null;
		this.access = null;
		this.lastModified = null;
		this.cover = null;
		this.webURL = null;
		this.feedURL = null;
		this.entryURL = null;
		this.editURL = null;
	}

	public PicasaPhoto uploadPicasaPhoto(File file, String title, String description, Date lastModified, String keywords, Boolean canComment) throws IOException {
		GoogleAuthorization googleAuthorization = this.getPicasaWeb().getGoogleAuthorization();
		HttpRequest httpRequest = new HttpRequest(this.getFeedURL());
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		httpRequest.setRequestHeaders(PicasaWeb.createPicasaGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_MULTIPART_RELATED, googleAuthorization));
		if (title == null || title.length() == 0) {
			title = file.getName();
		}
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		this.writeString(baos, "Media multipart posting");
		this.writeString(baos, "\n--END_OF_PART");
		this.writeString(baos, "\nContent-Type: ");
		this.writeString(baos, HttpRequest.CONTENT_TYPE_ATOM_XML);
		this.writeString(baos, "\n");
		this.writeString(baos, "\n");
		this.writeString(baos, PicasaPhoto.createPicasaPhotoXML(title, description, lastModified, keywords, canComment));
		this.writeString(baos, "\n--END_OF_PART");
		this.writeString(baos, "\nContent-Type: ");
		this.writeString(baos, URLConnection.guessContentTypeFromName(file.getAbsolutePath()));
		this.writeString(baos, "\n");
		this.writeString(baos, "\n");
		InputStream is = new FileInputStream(file);
		byte buffer[] = new byte[HttpRequest.DEFAULT_BUFFER_SIZE];
		for (int length; (length = is.read(buffer, 0, buffer.length)) > 0;) {
			baos.write(buffer, 0, length);
		}
		is.close();
		this.writeString(baos, "\n--END_OF_PART--");
		baos.flush();
		baos.close();
		httpRequest.setRequestData(baos.toByteArray());
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		return new PicasaPhoto(this, xmlTag);
	}

	private void writeString(OutputStream os, String string) throws IOException {
		os.write(string.getBytes(HttpRequest.CHARSET_UTF8));
	}

	public PicasaPhoto getPicasaPhoto(String id) throws IOException {
		GoogleAuthorization googleAuthorization = this.getPicasaWeb().getGoogleAuthorization();
		URL url = new URL(this.getEntryURL().toString() + "/photoid/" + id);
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		httpRequest.setRequestHeaders(PicasaWeb.createPicasaGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		return new PicasaPhoto(this, xmlTag);
	}

	public PicasaPhoto[] listPicasaPhotos() throws IOException {
		return this.listPicasaPhotos(PicasaPhotoComparator.SORT_BY_TITLE, PicasaPhotoComparator.ORDER_BY_ASCENDING);
	}

	public PicasaPhoto[] listPicasaPhotos(char sortBy, char orderBy) throws IOException {
		GoogleAuthorization googleAuthorization = this.getPicasaWeb().getGoogleAuthorization();
		URL url = this.getFeedURL();
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		httpRequest.setRequestHeaders(PicasaWeb.createPicasaGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		ArrayList<PicasaPhoto> picasaPhotos = new ArrayList<>();
		XmlTag[] entries = xmlTag.getXmlTags("entry");
		for (XmlTag entry : entries) {
			picasaPhotos.add(new PicasaPhoto(this, entry));
		}
		PicasaPhoto[] pps = picasaPhotos.toArray(new PicasaPhoto[picasaPhotos.size()]);
		Arrays.sort(pps, new PicasaPhotoComparator(sortBy, orderBy));
		return pps;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Album ID = ");
		sb.append(this.getAlbumId());
		sb.append("\nTitle = ");
		sb.append(this.getTitle());
		sb.append("\nDescription = ");
		sb.append(this.getDescription());
		sb.append("\nAccess = ");
		sb.append(this.getAccess());
		sb.append("\nPublished = ");
		sb.append(this.getPublished() == null ? "<null>" : Google.toHumanDateTimeFormat(this.getPublished()));
		sb.append("\nLast Modified = ");
		sb.append(this.getLastModified() == null ? "<null>" : Google.toHumanDateTimeFormat(this.getLastModified()));
		sb.append("\nCover = ");
		sb.append(this.getCover() == null ? "<null>" : this.getCover().toString());
		sb.append("\nWeb URL = ");
		sb.append(this.getWebURL() == null ? "<null>" : this.getWebURL().toString());
		return sb.toString();
	}
}
