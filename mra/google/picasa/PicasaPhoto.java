package mra.google.picasa;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import mra.http.HttpRequest;
import mra.google.Google;
import mra.google.GoogleAuthorization;
import mra.xml.XmlObject;
import mra.xml.XmlParser;
import mra.xml.XmlString;
import mra.xml.XmlTag;

public class PicasaPhoto {

	static String createPicasaPhotoXML(String title, String description, Date lastModified, String keywords, Boolean canComment) {
		XmlTag xmlRoot = new XmlTag("entry");
		xmlRoot.setXmlAttribute("xmlns", "http://www.w3.org/2005/Atom");
		xmlRoot.setXmlAttribute("xmlns:media", "http://search.yahoo.com/mrss");
		xmlRoot.setXmlAttribute("xmlns:gphoto", "http://schemas.google.com/photos/2007");
		{
			XmlTag xmlChild = new XmlTag("title");
			xmlChild.addXmlObject(new XmlString(title));
			xmlRoot.addXmlObject(xmlChild);
		}
		if (description != null) {
			XmlTag xmlChild = new XmlTag("summary");
			xmlChild.addXmlObject(new XmlString(description));
			xmlRoot.addXmlObject(xmlChild);
		}
		if (canComment != null) {
			XmlTag xmlChild = new XmlTag("gphoto:commentingEnabled");
			xmlChild.addXmlObject(new XmlString(canComment ? "true" : "false"));
			xmlRoot.addXmlObject(xmlChild);
		}
		{
			XmlTag xmlChild = new XmlTag("gphoto:timestamp");
			xmlChild.addXmlObject(new XmlString(Long.toString(lastModified != null ? lastModified.getTime() : System.currentTimeMillis())));
			xmlRoot.addXmlObject(xmlChild);
		}
		if (keywords != null) {
			XmlTag xmlChild = new XmlTag("media:group", new XmlObject[]{
				new XmlTag("media:keywords", new XmlObject[]{
					new XmlString(keywords)
				})
			});
			xmlRoot.addXmlObject(xmlChild);
		}
		{
			XmlTag xmlChild = new XmlTag("category");
			xmlChild.setXmlAttribute("scheme", "http://schemas.google.com/g/2005#kind");
			xmlChild.setXmlAttribute("term", "http://schemas.google.com/photos/2007#photo");
			xmlRoot.addXmlObject(xmlChild);
		}
		return xmlRoot.toString();
	}
	private PicasaAlbum picasaAlbum;
	private String photoId;
	private String title;
	private String description;
	private Date published;
	private Date lastModified;
	private String keywords;
	private boolean canComment;
	private int width;
	private int height;
	private int length;
	private String mime;
	private String head;
	private URL webURL;
	private URL feedURL;
	private URL entryURL;
	private URL editURL;
	private URL mediaURL;

	PicasaAlbum getPicasaAlbum() {
		return this.picasaAlbum;
	}

	public String getTitle() {
		return this.title;
	}

	public String getPhotoId() {
		return this.photoId;
	}

	public String getDescription() {
		return this.description;
	}

	public Date getPublished() {
		return this.published;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public String getKeywords() {
		return this.keywords;
	}

	public boolean isCanComment() {
		return this.canComment;
	}

	public int getWidth() {
		return this.width;
	}

	public int getHeight() {
		return this.height;
	}

	public int getLength() {
		return this.length;
	}

	public String getMime() {
		return this.mime;
	}

	public String getHead() {
		return this.head;
	}

	URL getWebURL() {
		return this.webURL;
	}

	URL getFeedURL() {
		return this.feedURL;
	}

	URL getEntryURL() {
		return this.entryURL;
	}

	URL getEditURL() {
		return this.editURL;
	}

	public URL getMediaURL() {
		return mediaURL;
	}

	PicasaPhoto(PicasaAlbum picasaAlbum, XmlTag xmlTag) {
		this.picasaAlbum = picasaAlbum;
		try {
			this.title = xmlTag.getXmlTags("title")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			this.photoId = xmlTag.getXmlTags("gphoto:id")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			this.description = xmlTag.getXmlTags("summary")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			this.published = Google.datetimeStringToDate(xmlTag.getXmlTags("published")[0].getInnerString());
		} catch (Exception ex) {
		}
		try {
			this.lastModified = Google.datetimeStringToDate(xmlTag.getXmlTags("updated")[0].getInnerString());
		} catch (Exception ex) {
		}
		try {
			this.keywords = xmlTag.getXmlTags("media:group")[0].getXmlTags("media:keywords")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			this.canComment = Boolean.parseBoolean(xmlTag.getXmlTags("gphoto:commentingEnabled")[0].getInnerString());
		} catch (Exception ex) {
		}
		try {
			this.width = Integer.parseInt(xmlTag.getXmlTags("gphoto:width")[0].getInnerString());
		} catch (Exception ex) {
		}
		try {
			this.height = Integer.parseInt(xmlTag.getXmlTags("gphoto:height")[0].getInnerString());
		} catch (Exception ex) {
		}
		try {
			this.length = Integer.parseInt(xmlTag.getXmlTags("gphoto:size")[0].getInnerString());
		} catch (Exception ex) {
		}
		try {
			XmlTag content = xmlTag.getXmlTags("content")[0];
			this.mime = content.getXmlAttribute("type");
			String src = content.getXmlAttribute("src");
			int index = src.lastIndexOf('/');
			this.head = src.substring(0, index);
		} catch (Exception ex) {
		}
		try {
			XmlTag[] links = xmlTag.getXmlTags("link");
			for (XmlTag link : links) {
				String rel = link.getXmlAttribute("rel");
				URL href = new URL(link.getXmlAttribute("href"));
				if (rel.equals("http://schemas.google.com/g/2005#feed")) {
					this.feedURL = href;
				} else if (rel.equals("self")) {
					this.entryURL = href;
				} else if (rel.equals("alternate")) {
					this.webURL = href;
				} else if (rel.equals("edit")) {
					this.editURL = href;
				} else if (rel.equals("edit-media")) {
					this.mediaURL = href;
				}
			}
		} catch (Exception ex) {
		}
	}

	public PicasaPhoto(String userId, String albumId, String photoId) throws IOException {
		this(userId, albumId, photoId, null);
	}

	public PicasaPhoto(String userId, String albumId, String photoId, GoogleAuthorization googleAuthorization) throws IOException {
		PicasaPhoto picasaPhoto = new PicasaWeb(userId, googleAuthorization).getPicasaAlbum(albumId).getPicasaPhoto(photoId);
		this.picasaAlbum = picasaPhoto.getPicasaAlbum();
		this.photoId = picasaPhoto.getPhotoId();
		this.title = picasaPhoto.getTitle();
		this.description = picasaPhoto.getDescription();
		this.lastModified = picasaPhoto.getLastModified();
		this.keywords = picasaPhoto.getKeywords();
		this.canComment = picasaPhoto.isCanComment();
		this.width = picasaPhoto.getWidth();
		this.height = picasaPhoto.getHeight();
		this.length = picasaPhoto.getLength();
		this.mime = picasaPhoto.getMime();
		this.head = picasaPhoto.getHead();
		this.webURL = picasaPhoto.getWebURL();
		this.feedURL = picasaPhoto.getFeedURL();
		this.entryURL = picasaPhoto.getEntryURL();
		this.editURL = picasaPhoto.getEditURL();
		this.mediaURL = picasaPhoto.getMediaURL();
	}

	public void update(String title, String description, Date lastModified, String keywords, Boolean canComment) throws IOException {
		GoogleAuthorization googleAuthorization = this.getPicasaAlbum().getPicasaWeb().getGoogleAuthorization();
		URL url = this.getEditURL();
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_PUT);
		httpRequest.setRequestHeaders(PicasaWeb.createPicasaGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_ATOM_XML, googleAuthorization));
		if (title != null) {
			this.title = title;
		}
		if (description != null) {
			this.description = description;
		}
		if (lastModified != null) {
			this.lastModified = lastModified;
		}
		if (keywords != null) {
			this.keywords = keywords;
		}
		if (canComment != null) {
			this.canComment = canComment;
		}
		String requestData = PicasaPhoto.createPicasaPhotoXML(this.getTitle(), this.getDescription(), this.getLastModified(), this.getKeywords(), this.isCanComment());
		httpRequest.setRequestData(requestData);
		httpRequest.send();
	}

	public void updateTitle(String title) throws IOException {
		this.update(title, null, null, null, null);
	}

	public void updateDescription(String description) throws IOException {
		this.update(null, description, null, null, null);
	}

	public void updateLastModified(Date lastModified) throws IOException {
		this.update(null, null, lastModified, null, null);
	}

	public void updateKeywords(String keywords) throws IOException {
		this.update(null, null, null, keywords, null);
	}

	public void updateCover(boolean canComment) throws IOException {
		this.update(null, null, null, null, canComment);
	}

	public void delete() throws IOException {
		GoogleAuthorization googleAuthorization = this.getPicasaAlbum().getPicasaWeb().getGoogleAuthorization();
		URL url = this.getEditURL();
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_DELETE);
		httpRequest.setRequestHeaders(PicasaWeb.createPicasaGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_ATOM_XML, googleAuthorization));
		httpRequest.send();
		this.picasaAlbum = null;
		this.photoId = null;
		this.title = null;
		this.description = null;
		this.lastModified = null;
		this.keywords = null;
		this.canComment = false;
		this.width = 0;
		this.height = 0;
		this.length = 0;
		this.mime = null;
		this.head = null;
		this.webURL = null;
		this.feedURL = null;
		this.entryURL = null;
		this.editURL = null;
		this.mediaURL = null;
	}

	public PicasaComment addPicasaComment(String comment) throws IOException {
		GoogleAuthorization googleAuthorization = this.getPicasaAlbum().getPicasaWeb().getGoogleAuthorization();
		URL url = this.getFeedURL();
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		httpRequest.setRequestHeaders(PicasaWeb.createPicasaGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_ATOM_XML, googleAuthorization));
		String requestData = PicasaComment.createPicasaCommentXML(comment);
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		return new PicasaComment(this, xmlTag);
	}

	public PicasaComment getPicasaComment(String commentId) throws IOException {
		GoogleAuthorization googleAuthorization = this.getPicasaAlbum().getPicasaWeb().getGoogleAuthorization();
		URL url = new URL(this.getEntryURL().toString() + "/commentid/" + commentId);
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		httpRequest.setRequestHeaders(PicasaWeb.createPicasaGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		return new PicasaComment(this, xmlTag);
	}

	public PicasaComment[] listPicasaComments() throws IOException {
		return this.listPicasaComments(PicasaCommentComparator.SORT_BY_LAST_MODIFIED, PicasaCommentComparator.ORDER_BY_DESCENDING);
	}

	public PicasaComment[] listPicasaComments(char sortBy, char orderBy) throws IOException {
		GoogleAuthorization googleAuthorization = this.getPicasaAlbum().getPicasaWeb().getGoogleAuthorization();
		URL url = this.getFeedURL();
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		httpRequest.setRequestHeaders(PicasaWeb.createPicasaGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		ArrayList<PicasaComment> picasaComments = new ArrayList<>();
		XmlTag[] entries = xmlTag.getXmlTags("entry");
		for (XmlTag entry : entries) {
			picasaComments.add(new PicasaComment(this, entry));
		}
		PicasaComment[] pcs = picasaComments.toArray(new PicasaComment[picasaComments.size()]);
		Arrays.sort(pcs, new PicasaCommentComparator(sortBy, orderBy));
		return pcs;
	}

	public PicasaTag addPicasaTag(String tag) throws IOException {
		GoogleAuthorization googleAuthorization = this.getPicasaAlbum().getPicasaWeb().getGoogleAuthorization();
		URL url = this.getFeedURL();
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		httpRequest.setRequestHeaders(PicasaWeb.createPicasaGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_ATOM_XML, googleAuthorization));
		String requestData = PicasaTag.createPicasaTagXML(tag);
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		return new PicasaTag(this, xmlTag);
	}

	public PicasaTag getPicasaTag(String tag) throws IOException {
		GoogleAuthorization googleAuthorization = this.getPicasaAlbum().getPicasaWeb().getGoogleAuthorization();
		URL url = new URL(this.getEntryURL().toString() + "/tag/" + tag);
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		httpRequest.setRequestHeaders(PicasaWeb.createPicasaGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		return new PicasaTag(this, xmlTag);
	}

	public PicasaTag[] listPicasaTags() throws IOException {
		return this.listPicasaTags(PicasaTagComparator.SORT_BY_LAST_MODIFIED, PicasaCommentComparator.ORDER_BY_DESCENDING);
	}

	public PicasaTag[] listPicasaTags(char sortBy, char orderBy) throws IOException {
		GoogleAuthorization googleAuthorization = this.getPicasaAlbum().getPicasaWeb().getGoogleAuthorization();
		URL url = this.getFeedURL();
		String query = url.getQuery();
		String path = String.format("%s://%s%s", url.getProtocol(), url.getHost(), url.getPath());
		url = new URL(path + ((query == null) ? "?" : "&") + "kind=tag");
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		httpRequest.setRequestHeaders(PicasaWeb.createPicasaGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		ArrayList<PicasaTag> picasaTags = new ArrayList<>();
		XmlTag[] entries = xmlTag.getXmlTags("entry");
		for (XmlTag entry : entries) {
			picasaTags.add(new PicasaTag(this, entry));
		}
		PicasaTag[] pts = picasaTags.toArray(new PicasaTag[picasaTags.size()]);
		Arrays.sort(pts, new PicasaTagComparator(sortBy, orderBy));
		return pts;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Photo ID = ");
		sb.append(this.getPhotoId());
		sb.append("\nTitle = ");
		sb.append(this.getTitle());
		sb.append("\nDescription = ");
		sb.append(this.getDescription());
		sb.append("\nPublished = ");
		sb.append(this.getPublished() == null ? "<null>" : Google.toHumanDateTimeFormat(this.getPublished()));
		sb.append("\nLast Modified = ");
		sb.append(this.getLastModified() == null ? "<null>" : Google.toHumanDateTimeFormat(this.getLastModified()));
		sb.append("\nKeywords = ");
		sb.append(this.getKeywords());
		sb.append("\nCan Comment = ");
		sb.append(this.isCanComment() ? "true" : "false");
		sb.append("\nWidth = ");
		sb.append(this.getWidth());
		sb.append("\nHeight = ");
		sb.append(this.getHeight());
		sb.append("\nLength = ");
		sb.append(this.getLength());
		sb.append("\nMime = ");
		sb.append(this.getMime());
		sb.append("\nHead = ");
		sb.append(this.getHead());
		sb.append("\nWeb URL = ");
		sb.append(((this.getWebURL() == null) ? "<null>" : this.getWebURL().toString()));
		return sb.toString();
	}
}
