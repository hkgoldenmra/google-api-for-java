package mra.google.picasa;

import java.util.Comparator;

public class PicasaTagComparator implements Comparator<PicasaTag> {

	public static final char SORT_BY_TAG = 'T';
	public static final char SORT_BY_LAST_MODIFIED = 'M';
	public static final char ORDER_BY_ASCENDING = 'A';
	public static final char ORDER_BY_DESCENDING = 'D';
	private char sortBy;
	private char orderBy;

	public PicasaTagComparator(char sortBy, char orderBy) {
		switch (sortBy) {
			case PicasaTagComparator.SORT_BY_TAG: {
				this.sortBy = PicasaTagComparator.SORT_BY_TAG;
			}
			break;
			default: {
				this.sortBy = PicasaTagComparator.SORT_BY_LAST_MODIFIED;
			}
		}
		switch (orderBy) {
			case PicasaTagComparator.ORDER_BY_DESCENDING: {
				this.orderBy = orderBy;
			}
			break;
			default: {
				this.orderBy = PicasaTagComparator.ORDER_BY_ASCENDING;
			}
		}
	}

	public PicasaTagComparator() {
		this(PicasaTagComparator.SORT_BY_LAST_MODIFIED, PicasaTagComparator.ORDER_BY_DESCENDING);
	}

	@Override
	public int compare(PicasaTag t1, PicasaTag t2) {
		switch (this.sortBy) {
			case PicasaTagComparator.SORT_BY_TAG: {
				switch (this.orderBy) {
					case PicasaTagComparator.ORDER_BY_DESCENDING: {
						return t2.getTag().compareTo(t1.getTag());
					}
					default: {
						return t1.getTag().compareTo(t2.getTag());
					}
				}
			}
			default: {
				switch (this.orderBy) {
					case PicasaTagComparator.ORDER_BY_DESCENDING: {
						return t2.getLastModified().compareTo(t1.getLastModified());
					}
					default: {
						return t1.getLastModified().compareTo(t2.getLastModified());
					}
				}
			}
		}
	}
}
