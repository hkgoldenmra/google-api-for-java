package mra.google.picasa;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import mra.http.HttpRequest;
import mra.google.Google;
import mra.google.GoogleAuthorization;
import mra.xml.XmlString;
import mra.xml.XmlTag;

public class PicasaTag {

	static String createPicasaTagXML(String tag) {
		XmlTag xmlRoot = new XmlTag("entry");
		xmlRoot.setXmlAttribute("xmlns", "http://www.w3.org/2005/Atom");
		xmlRoot.setXmlAttribute("xmlns:gphoto", "http://schemas.google.com/photos/2007");
		{
			XmlTag xmlChild = new XmlTag("title");
			xmlChild.addXmlObject(new XmlString(tag));
			xmlRoot.addXmlObject(xmlChild);
		}
		{
			XmlTag xmlChild = new XmlTag("category");
			xmlChild.setXmlAttribute("scheme", "http://schemas.google.com/g/2005#kind");
			xmlChild.setXmlAttribute("term", "http://schemas.google.com/photos/2007#tag");
			xmlRoot.addXmlObject(xmlChild);
		}
		return xmlRoot.toString();
	}
	private PicasaPhoto picasaPhoto;
	private String tag;
	private Date published;
	private Date lastModified;
	private URL editURL;

	PicasaPhoto getPicasaPhoto() {
		return this.picasaPhoto;
	}

	public String getTag() {
		return this.tag;
	}

	public Date getPublished() {
		return this.published;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	URL getEditURL() {
		return this.editURL;
	}

	PicasaTag(PicasaPhoto picasaPhoto, XmlTag xmlTag) {
		this.picasaPhoto = picasaPhoto;
		try {
			this.tag = xmlTag.getXmlTags("title")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			this.published = Google.datetimeStringToDate(xmlTag.getXmlTags("published")[0].getInnerString());
		} catch (Exception ex) {
		}
		try {
			this.lastModified = Google.datetimeStringToDate(xmlTag.getXmlTags("updated")[0].getInnerString());
		} catch (Exception ex) {
		}
		try {
			XmlTag[] links = xmlTag.getXmlTags("link");
			for (XmlTag link : links) {
				String rel = link.getXmlAttribute("rel");
				URL href = new URL(link.getXmlAttribute("href"));
				if (rel.equals("edit")) {
					this.editURL = href;
				}
			}
		} catch (Exception ex) {
		}
	}

	public void delete() throws IOException {
		GoogleAuthorization googleAuthorization = this.getPicasaPhoto().getPicasaAlbum().getPicasaWeb().getGoogleAuthorization();
		URL url = this.getEditURL();
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_DELETE);
		httpRequest.setRequestHeaders(PicasaWeb.createPicasaGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_ATOM_XML, googleAuthorization));
		httpRequest.send();
		this.picasaPhoto = null;
		this.tag = null;
		this.editURL = null;
		this.lastModified = null;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Tag = ");
		sb.append(this.getTag());
		sb.append("\nPublished = ");
		sb.append(this.getPublished() == null ? "<null>" : Google.toHumanDateTimeFormat(this.getPublished()));
		sb.append("\nLast Modified = ");
		sb.append(this.getLastModified() == null ? "<null>" : Google.toHumanDateTimeFormat(this.getLastModified()));
		return sb.toString();
	}
}
