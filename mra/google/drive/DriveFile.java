package mra.google.drive;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import mra.google.Google;
import mra.google.GoogleAuthorization;
import mra.http.HttpRequest;
import mra.json.JsonArray;
import mra.json.JsonBoolean;
import mra.json.JsonObject;
import mra.json.JsonParser;
import mra.json.JsonString;
import mra.json.JsonValue;

public class DriveFile {

	static Map<String, String> createDriveGoogleAuthorizationHeaders(String contentType, GoogleAuthorization googleAuthorization) {
		Map<String, String> requestHeaders = new LinkedHashMap<>();
		requestHeaders.put("Content-Type", contentType + "; charset=" + HttpRequest.CHARSET_UTF8);
		if (googleAuthorization != null) {
			requestHeaders.put("Authorization", googleAuthorization.getToken());
		}
		return requestHeaders;
	}

	static String createFolderJSON(String title, DriveFile folder) {
		JsonObject jsonRoot = new JsonObject();
		if (folder != null) {
			JsonObject joId = new JsonObject();
			joId.setValue("id", new JsonString(folder.getFileId()));
			jsonRoot.setValue("parents", joId);
		}
		jsonRoot.setValue("title", new JsonString(title));
		jsonRoot.setValue("mimeType", new JsonString("application/vnd.google-apps.folder"));
		return jsonRoot.toString();
	}

	static String createDriveFileJSON(String title, DriveFile folder) {
		JsonObject jsonRoot = new JsonObject();
		if (folder != null) {
			JsonObject joId = new JsonObject();
			joId.setValue("id", new JsonString(folder.getFileId()));
			JsonArray jaParents = new JsonArray();
			jaParents.addValue(joId);
			jsonRoot.setValue("parents", jaParents);
		}
		jsonRoot.setValue("title", new JsonString(title));
		return jsonRoot.toString();
	}

	private GoogleAuthorization googleAuthorization;
	private String fileId;
	private String title;
	private String fileName;
	private String extension;
	private String mimeType;
	private DriveFile parent;
	private long size;
	private Date created;
	private Date lastModified;
	private boolean shared;
	private URL webURL;
	private URL sourceURL;
	private URL feedURL;

	GoogleAuthorization getGoogleAuthorization() {
		return this.googleAuthorization;
	}

	public String getFileId() {
		return this.fileId;
	}

	public String getTitle() {
		return this.title;
	}

	public String getFileName() {
		return this.fileName;
	}

	public boolean isFile() {
		return this.getFileName() != null;
	}

	public String getExtension() {
		return this.extension;
	}

	public String getMimeType() {
		return this.mimeType;
	}

	public DriveFile getParent() {
		return this.parent;
	}

	public boolean isRoot() {
		return this.getParent() == null;
	}

	public long getSize() {
		return this.size;
	}

	public Date getCreated() {
		return this.created;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public boolean isShared() {
		return this.shared;
	}

	public URL getWebURL() {
		return this.webURL;
	}

	public URL getSourceURL() {
		return this.sourceURL;
	}

	URL getFeedURL() {
		return this.feedURL;
	}

	private void _initial(JsonObject jsonObject) {
		try {
			this.fileId = ((JsonString) jsonObject.getValue("id")).getValue();
		} catch (Exception ex) {
		}
		try {
			this.title = ((JsonString) jsonObject.getValue("title")).getValue();
		} catch (Exception ex) {
		}
		try {
			this.fileName = ((JsonString) jsonObject.getValue("originalFilename")).getValue();
		} catch (Exception ex) {
		}
		try {
			this.extension = ((JsonString) jsonObject.getValue("fileExtension")).getValue();
		} catch (Exception ex) {
		}
		try {
			this.mimeType = ((JsonString) jsonObject.getValue("mimeType")).getValue();
		} catch (Exception ex) {
		}
		try {
			this.size = Long.parseLong(((JsonString) jsonObject.getValue("fileSize")).getValue());
		} catch (Exception ex) {
		}
		try {
			this.created = Google.datetimeStringToDate(((JsonString) jsonObject.getValue("createdDate")).getValue());
		} catch (Exception ex) {
		}
		try {
			this.lastModified = Google.datetimeStringToDate(((JsonString) jsonObject.getValue("modifiedDate")).getValue());
		} catch (Exception ex) {
		}
		try {
			this.shared = ((JsonBoolean) jsonObject.getValue("shared")).getValue();
		} catch (Exception ex) {
		}
		try {
			this.webURL = new URL(((JsonString) jsonObject.getValue("alternateLink")).getValue());
		} catch (Exception ex) {
		}
		try {
			this.sourceURL = new URL(((JsonString) jsonObject.getValue("downloadUrl")).getValue());
		} catch (Exception ex) {
		}
		try {
			String parentId = ((JsonString) ((JsonObject) ((JsonArray) jsonObject.getValue("parents")).getValue(0)).getValue("id")).getValue();
			this.parent = new DriveFile(parentId, this.getGoogleAuthorization());
		} catch (Exception ex) {
		}
	}

	public DriveFile(GoogleAuthorization googleAuthorization) throws IOException {
		this(new DriveInfo(googleAuthorization).getRootDir().getFileId(), googleAuthorization);
	}

	public DriveFile(String fileId, GoogleAuthorization googleAuthorization) throws IOException {
		this.googleAuthorization = googleAuthorization;
		this.feedURL = new URL("https://www.googleapis.com/drive/v2/files/" + fileId);
		HttpRequest httpRequest = new HttpRequest(this.getFeedURL());
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		httpRequest.setRequestHeaders(DriveFile.createDriveGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		httpRequest.send();
		JsonObject jsonObject = (JsonObject) JsonParser.parse(httpRequest.getResponseString());
		this._initial(jsonObject);
	}

	DriveFile(JsonObject jsonObject) {
		this._initial(jsonObject);
	}

	public DriveFile[] listDriveFiles() throws IOException {
		return this.listDriveFiles(DriveFileComparator.SORT_BY_TITLE, DriveFileComparator.ORDER_BY_ASCENDING);
	}

	public DriveFile[] listDriveFiles(char sortBy, char orderBy) throws IOException {
		GoogleAuthorization googleAuthorization = this.getGoogleAuthorization();
		URL url = new URL(this.getFeedURL().toString() + "/children");
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		httpRequest.setRequestHeaders(DriveFile.createDriveGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonObject jsonObject = (JsonObject) JsonParser.parse(responseString);
		ArrayList<DriveFile> driveFiles = new ArrayList<>();
		JsonArray jsonArray = (JsonArray) jsonObject.getValue("items");
		JsonValue[] jsonValues = jsonArray.getValue();
		for (JsonValue jsonValue : jsonValues) {
			String id = ((JsonString) ((JsonObject) jsonValue).getValue("id")).getValue();
			driveFiles.add(new DriveFile(id, this.getGoogleAuthorization()));
		}
		DriveFile[] dfs = driveFiles.toArray(new DriveFile[driveFiles.size()]);
		Arrays.sort(dfs, new DriveFileComparator(sortBy, orderBy));
		return dfs;
	}

	public DriveFile createFolder(String title) throws IOException {
		GoogleAuthorization googleAuthorization = this.getGoogleAuthorization();
		URL url = new URL("https://www.googleapis.com/drive/v2/files");
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		httpRequest.setRequestHeaders(DriveFile.createDriveGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_JSON, googleAuthorization));
		String requestData = DriveFile.createFolderJSON(title, this);
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonObject jsonObject = (JsonObject) JsonParser.parse(responseString);
		return new DriveFile(jsonObject);
	}

	public DriveFile uploadDriveFile(File file) throws IOException {
		return this.uploadDriveFile(file, null);
	}

	public DriveFile uploadDriveFile(File file, String title) throws IOException {
		GoogleAuthorization googleAuthorization = this.getGoogleAuthorization();
		URL url = new URL("https://www.googleapis.com/upload/drive/v2/files?uploadType=multipart");
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		httpRequest.setRequestHeaders(DriveFile.createDriveGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_MULTIPART_RELATED, googleAuthorization));
		if (title == null || title.length() == 0) {
			title = file.getName();
		}
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		this.writeString(baos, "Media multipart posting");
		this.writeString(baos, "\n--END_OF_PART");
		this.writeString(baos, "\nContent-Type: ");
		this.writeString(baos, HttpRequest.CONTENT_TYPE_JSON);
		this.writeString(baos, "\n");
		this.writeString(baos, "\n");
		this.writeString(baos, DriveFile.createDriveFileJSON(title, this));
		this.writeString(baos, "\n--END_OF_PART");
		this.writeString(baos, "\nContent-Type: ");
		this.writeString(baos, URLConnection.guessContentTypeFromName(file.getAbsolutePath()));
		this.writeString(baos, "\n");
		this.writeString(baos, "\n");
		InputStream is = new FileInputStream(file);
		byte buffer[] = new byte[HttpRequest.DEFAULT_BUFFER_SIZE];
		for (int length; (length = is.read(buffer, 0, buffer.length)) > 0;) {
			baos.write(buffer, 0, length);
		}
		is.close();
		this.writeString(baos, "\n--END_OF_PART--");
		baos.flush();
		baos.close();
		httpRequest.setRequestData(baos.toByteArray());
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonObject jsonObject = (JsonObject) JsonParser.parse(responseString);
		return new DriveFile(jsonObject);
	}

	private void writeString(OutputStream os, String string) throws IOException {
		os.write(string.getBytes(HttpRequest.CHARSET_UTF8));
	}

	public void trash() throws IOException {
		GoogleAuthorization googleAuthorization = this.getGoogleAuthorization();
		URL url = new URL(this.getFeedURL().toString() + "/trash");
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		httpRequest.setRequestHeaders(DriveFile.createDriveGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		httpRequest.send();
	}

	public void delete() throws IOException {
		GoogleAuthorization googleAuthorization = this.getGoogleAuthorization();
		URL url = this.getFeedURL();
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_DELETE);
		httpRequest.setRequestHeaders(DriveFile.createDriveGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		httpRequest.send();
		this.fileId = null;
		this.title = null;
		this.fileName = null;
		this.extension = null;
		this.mimeType = null;
		this.parent = null;
		this.size = 0L;
		this.created = null;
		this.lastModified = null;
		this.shared = false;
		this.webURL = null;
		this.sourceURL = null;
		this.feedURL = null;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("File ID = ");
		sb.append(this.getFileId());
		sb.append("\nTitle = ");
		sb.append(this.getTitle());
		sb.append("\nFile Name = ");
		sb.append(this.getFileName());
		sb.append("\nIs File = ");
		sb.append(this.isFile() ? "true" : "false");
		sb.append("\nExtension = ");
		sb.append(this.getExtension());
		sb.append("\nMime Type = ");
		sb.append(this.getMimeType());
		sb.append("\nFile Size = ");
		sb.append(this.getSize());
		sb.append("\nCreated = ");
		sb.append(this.getCreated() == null ? "<null>" : Google.toHumanDateTimeFormat(this.getCreated()));
		sb.append("\nLast Modified = ");
		sb.append(this.getLastModified() == null ? "<null>" : Google.toHumanDateTimeFormat(this.getLastModified()));
		sb.append("\nShared = ");
		sb.append(this.getLastModified() == null ? "<null>" : this.isShared());
		sb.append("\nWeb URL = ");
		sb.append(this.getWebURL() == null ? "<null>" : this.getWebURL().toString());
		sb.append("\nSource URL = ");
		sb.append(this.getSourceURL() == null ? "<null>" : this.getSourceURL().toString());
		sb.append("\nIs Root = ");
		sb.append(this.isRoot() ? "true" : "false");
		sb.append("\nParent ID = ");
		sb.append(this.getParent() == null ? "<null>" : this.getParent().getFileId());
		return sb.toString();
	}
}
