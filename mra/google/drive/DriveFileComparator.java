package mra.google.drive;

import java.util.Comparator;

public class DriveFileComparator implements Comparator<DriveFile> {

	public static final char SORT_BY_TITLE = 'T';
	public static final char SORT_BY_FILENAME = 'F';
	public static final char SORT_BY_EXTENSION = 'E';
	public static final char SORT_BY_MIME_TYPE = 'P';
	public static final char SORT_BY_SIZE = 'S';
	public static final char SORT_BY_CREATED = 'C';
	public static final char SORT_BY_LAST_MODIFIED = 'M';
	public static final char ORDER_BY_ASCENDING = 'A';
	public static final char ORDER_BY_DESCENDING = 'D';
	private char sortBy;
	private char orderBy;

	public DriveFileComparator(char sortBy, char orderBy) {
		switch (sortBy) {
			case DriveFileComparator.SORT_BY_FILENAME:
			case DriveFileComparator.SORT_BY_EXTENSION:
			case DriveFileComparator.SORT_BY_MIME_TYPE:
			case DriveFileComparator.SORT_BY_SIZE:
			case DriveFileComparator.SORT_BY_CREATED:
			case DriveFileComparator.SORT_BY_LAST_MODIFIED: {
				this.sortBy = sortBy;
			}
			break;
			default: {
				this.sortBy = DriveFileComparator.SORT_BY_TITLE;
			}
		}
		switch (orderBy) {
			case DriveFileComparator.ORDER_BY_DESCENDING: {
				this.orderBy = orderBy;
			}
			break;
			default: {
				this.orderBy = DriveFileComparator.ORDER_BY_ASCENDING;
			}
		}
	}

	public DriveFileComparator() {
		this(DriveFileComparator.SORT_BY_TITLE, DriveFileComparator.ORDER_BY_ASCENDING);
	}

	@Override
	public int compare(DriveFile t1, DriveFile t2) {
		switch (this.sortBy) {
			case DriveFileComparator.SORT_BY_FILENAME: {
				switch (this.orderBy) {
					case DriveFileComparator.ORDER_BY_DESCENDING: {
						return t2.getFileName().compareTo(t1.getFileName());
					}
					default: {
						return t1.getFileName().compareTo(t2.getFileName());
					}
				}
			}
			case DriveFileComparator.SORT_BY_EXTENSION: {
				switch (this.orderBy) {
					case DriveFileComparator.ORDER_BY_DESCENDING: {
						return t2.getExtension().compareTo(t1.getExtension());
					}
					default: {
						return t1.getExtension().compareTo(t2.getExtension());
					}
				}
			}
			case DriveFileComparator.SORT_BY_MIME_TYPE: {
				switch (this.orderBy) {
					case DriveFileComparator.ORDER_BY_DESCENDING: {
						return t2.getMimeType().compareTo(t1.getMimeType());
					}
					default: {
						return t1.getMimeType().compareTo(t2.getMimeType());
					}
				}
			}
			case DriveFileComparator.SORT_BY_SIZE: {
				String t1s = String.format("%030d", t1.getSize());
				String t2s = String.format("%030d", t2.getSize());
				switch (this.orderBy) {
					case DriveFileComparator.ORDER_BY_DESCENDING: {
						return t2s.compareTo(t1s);
					}
					default: {
						return t1s.compareTo(t2s);
					}
				}
			}
			case DriveFileComparator.SORT_BY_CREATED: {
				switch (this.orderBy) {
					case DriveFileComparator.ORDER_BY_DESCENDING: {
						return t2.getCreated().compareTo(t1.getCreated());
					}
					default: {
						return t1.getCreated().compareTo(t2.getCreated());
					}
				}
			}
			case DriveFileComparator.SORT_BY_LAST_MODIFIED: {
				switch (this.orderBy) {
					case DriveFileComparator.ORDER_BY_DESCENDING: {
						return t2.getLastModified().compareTo(t1.getLastModified());
					}
					default: {
						return t1.getLastModified().compareTo(t2.getLastModified());
					}
				}
			}
			default: {
				switch (this.orderBy) {
					case DriveFileComparator.ORDER_BY_DESCENDING: {
						return t2.getTitle().compareTo(t1.getTitle());
					}
					default: {
						return t1.getTitle().compareTo(t2.getTitle());
					}
				}
			}
		}
	}
}
