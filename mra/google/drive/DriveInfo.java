package mra.google.drive;

import java.io.IOException;
import java.net.URL;
import mra.google.GoogleAuthorization;
import mra.http.HttpRequest;
import mra.json.JsonObject;
import mra.json.JsonParser;
import mra.json.JsonString;

public class DriveInfo {

	private GoogleAuthorization googleAuthorization;
	private DriveFile rootDir;
	private String author;
	private long totalSpace;
	private long totalUsedSpace;
	private long totalTrashSpace;
	private long totalFreeSpace;

	GoogleAuthorization getGoogleAuthorization() {
		return this.googleAuthorization;
	}

	public DriveFile getRootDir() {
		return this.rootDir;
	}

	public String getAuthor() {
		return this.author;
	}

	public long getTotalSpace() {
		return this.totalSpace;
	}

	public long getTotalUsedSpace() {
		return this.totalUsedSpace;
	}

	public long getTotalTrashSpace() {
		return this.totalTrashSpace;
	}

	public long getTotalFreeSpace() {
		return this.totalFreeSpace;
	}

	private void _initial(JsonObject jsonObject) {
		try {
			this.rootDir = new DriveFile(((JsonString) jsonObject.getValue("rootFolderId")).getValue(), this.getGoogleAuthorization());
		} catch (Exception ex) {
		}
		try {
			this.author = ((JsonString) jsonObject.getValue("name")).getValue();
		} catch (Exception ex) {
		}
		try {
			this.totalSpace = Long.parseLong(((JsonString) jsonObject.getValue("quotaBytesTotal")).getValue());
		} catch (Exception ex) {
		}
		try {
			this.totalUsedSpace = Long.parseLong(((JsonString) jsonObject.getValue("quotaBytesUsed")).getValue());
		} catch (Exception ex) {
		}
		try {
			this.totalFreeSpace = Long.parseLong(((JsonString) jsonObject.getValue("quotaBytesUsedAggregate")).getValue());
		} catch (Exception ex) {
		}
		try {
			this.totalTrashSpace = Long.parseLong(((JsonString) jsonObject.getValue("quotaBytesUsedInTrash")).getValue());
		} catch (Exception ex) {
		}
	}

	public DriveInfo(GoogleAuthorization googleAuthorization) throws IOException {
		this.googleAuthorization = googleAuthorization;
		URL url = new URL("https://www.googleapis.com/drive/v2/about");
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		httpRequest.setRequestHeaders(DriveFile.createDriveGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		httpRequest.send();
		JsonObject jsonObject = (JsonObject) JsonParser.parse(httpRequest.getResponseString());
		this._initial(jsonObject);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("File ID = ");
		sb.append(this.getRootDir().getFileId());
		sb.append("\nAuthor = ");
		sb.append(this.getAuthor());
		sb.append("\nTotal Space = ");
		sb.append(this.getTotalSpace());
		sb.append("\nTotal Used Space = ");
		sb.append(this.getTotalUsedSpace());
		sb.append("\nTotal Trash Space = ");
		sb.append(this.getTotalTrashSpace());
		sb.append("\nTotal Free Space = ");
		sb.append(this.getTotalFreeSpace());
		return sb.toString();
	}
}
