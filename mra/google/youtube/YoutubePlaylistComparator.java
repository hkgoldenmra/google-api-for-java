package mra.google.youtube;

import java.util.Comparator;

public class YoutubePlaylistComparator implements Comparator<YoutubePlaylist> {

	public static final char SORT_BY_TITLE = 'T';
	public static final char ORDER_BY_ASCENDING = 'A';
	public static final char ORDER_BY_DESCENDING = 'D';
	private char sortBy;
	private char orderBy;

	public YoutubePlaylistComparator(char sortBy, char orderBy) {
		switch (sortBy) {
			default: {
				this.sortBy = YoutubePlaylistComparator.SORT_BY_TITLE;
			}
		}
		switch (orderBy) {
			case YoutubePlaylistComparator.ORDER_BY_DESCENDING: {
				this.orderBy = orderBy;
			}
			break;
			default: {
				this.orderBy = YoutubePlaylistComparator.ORDER_BY_ASCENDING;
			}
		}
	}

	public YoutubePlaylistComparator() {
		this(YoutubePlaylistComparator.SORT_BY_TITLE, YoutubePlaylistComparator.ORDER_BY_ASCENDING);
	}

	@Override
	public int compare(YoutubePlaylist t1, YoutubePlaylist t2) {
		switch (this.sortBy) {
			default: {
				switch (this.orderBy) {
					case YoutubeVideoComparator.ORDER_BY_DESCENDING: {
						return t2.getTitle().compareTo(t1.getTitle());
					}
					default: {
						return t1.getTitle().compareTo(t2.getTitle());
					}
				}
			}
		}
	}
}
