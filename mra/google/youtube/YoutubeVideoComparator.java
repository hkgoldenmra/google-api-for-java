package mra.google.youtube;

import java.util.Comparator;

public class YoutubeVideoComparator implements Comparator<YoutubeVideo> {

	public static final char SORT_BY_TITLE = 'T';
	public static final char SORT_BY_PUBLISHED = 'P';
	public static final char SORT_BY_LAST_MODIFIED = 'M';
	public static final char SORT_BY_DURATION = 'D';
	public static final char SORT_BY_VIEW_COUNT = 'V';
	public static final char ORDER_BY_ASCENDING = 'A';
	public static final char ORDER_BY_DESCENDING = 'D';
	private char sortBy;
	private char orderBy;

	public YoutubeVideoComparator(char sortBy, char orderBy) {
		switch (sortBy) {
			case YoutubeVideoComparator.SORT_BY_PUBLISHED:
			case YoutubeVideoComparator.SORT_BY_LAST_MODIFIED:
			case YoutubeVideoComparator.SORT_BY_DURATION:
			case YoutubeVideoComparator.SORT_BY_VIEW_COUNT: {
				this.sortBy = sortBy;
			}
			break;
			default: {
				this.sortBy = YoutubeVideoComparator.SORT_BY_TITLE;
			}
		}
		switch (orderBy) {
			case YoutubeVideoComparator.ORDER_BY_DESCENDING: {
				this.orderBy = orderBy;
			}
			break;
			default: {
				this.orderBy = YoutubeVideoComparator.ORDER_BY_ASCENDING;
			}
		}
	}

	public YoutubeVideoComparator() {
		this(YoutubeVideoComparator.SORT_BY_TITLE, YoutubeVideoComparator.ORDER_BY_ASCENDING);
	}

	@Override
	public int compare(YoutubeVideo t1, YoutubeVideo t2) {
		switch (this.sortBy) {
			case YoutubeVideoComparator.SORT_BY_PUBLISHED: {
				switch (this.orderBy) {
					case YoutubeVideoComparator.ORDER_BY_DESCENDING: {
						return t2.getPublished().compareTo(t1.getPublished());
					}
					default: {
						return t1.getPublished().compareTo(t2.getPublished());
					}
				}
			}
			case YoutubeVideoComparator.SORT_BY_LAST_MODIFIED: {
				switch (this.orderBy) {
					case YoutubeVideoComparator.ORDER_BY_DESCENDING: {
						return t2.getLastModified().compareTo(t1.getLastModified());
					}
					default: {
						return t1.getLastModified().compareTo(t2.getLastModified());
					}
				}
			}
			case YoutubeVideoComparator.SORT_BY_DURATION: {
				switch (this.orderBy) {
					case YoutubeVideoComparator.ORDER_BY_DESCENDING: {
						return t2.getDuration() - t1.getDuration();
					}
					default: {
						return t1.getDuration() - t2.getDuration();
					}
				}
			}
			case YoutubeVideoComparator.SORT_BY_VIEW_COUNT: {
				switch (this.orderBy) {
					case YoutubeVideoComparator.ORDER_BY_DESCENDING: {
						return t2.getViewCount() - t1.getViewCount();
					}
					default: {
						return t1.getViewCount() - t2.getViewCount();
					}
				}
			}
			default: {
				switch (this.orderBy) {
					case YoutubeVideoComparator.ORDER_BY_DESCENDING: {
						return t2.getTitle().compareTo(t1.getTitle());
					}
					default: {
						return t1.getTitle().compareTo(t2.getTitle());
					}
				}
			}
		}
	}
}
