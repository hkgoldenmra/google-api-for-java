package mra.google.youtube;

import java.io.IOException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import mra.http.HttpRequest;
import mra.google.Google;
import mra.google.GoogleAuthorization;
import mra.xml.XmlParser;
import mra.xml.XmlString;
import mra.xml.XmlTag;

public class YoutubeVideo {

	public static final String CATEGORY_PEOPLE_AND_BLOGS = "People";
	public static final String CATEGORY_AUTOS_AND_VEHICLES = "Autos";
	public static final String CATEGORY_NONPROFIT_AND_ACTIVISM = "Nonprofit";
	public static final String CATEGORY_SCIENCE_AND_TECHNOLOGY = "Tech";
	public static final String CATEGORY_MUSIC = "Music";
	public static final String CATEGORY_ENTERTAINMENT = "Entertainment";
	public static final String CATEGORY_TRAVEL_AND_EVENTS = "Travel";
	public static final String CATEGORY_EDUCATION = "Education";
	public static final String CATEGORY_COMEDY = "Comedy";
	public static final String CATEGORY_NEWS_AND_POLITIES = "News";
	public static final String CATEGORY_GAMING = "Games";
	public static final String CATEGORY_SPORTS = "Sports";
	public static final String CATEGORY_FILM_AND_ANIMATION = "Film";
	public static final String CATEGORY_PETS_AND_ANIMALS = "Animals";
	public static final String CATEGORY_HOWTO_AND_STYLE = "Howto";

	static String createYoutubeVideoXML(String title, String description, String category, String keywords, boolean canAccess, boolean canEmbed) {
		XmlTag xmlRoot = new XmlTag("entry");
		xmlRoot.setXmlAttribute("xmlns", "http://www.w3.org/2005/Atom");
		xmlRoot.setXmlAttribute("xmlns:media", "http://search.yahoo.com/mrss");
		xmlRoot.setXmlAttribute("xmlns:gphoto", "http://gdata.youtube.com/schemas/2007");
		{
			XmlTag xmlMedia = new XmlTag("media:group");
			if (title != null) {
				XmlTag xmlChild = new XmlTag("media:title");
				xmlChild.setXmlAttribute("type", "plain");
				xmlChild.addXmlObject(new XmlString(title));
				xmlMedia.addXmlObject(xmlChild);
			}
			if (description != null) {
				XmlTag xmlChild = new XmlTag("media:description");
				xmlChild.setXmlAttribute("type", "plain");
				xmlChild.addXmlObject(new XmlString(description));
				xmlMedia.addXmlObject(xmlChild);
			}
			if (category != null) {
				XmlTag xmlChild = new XmlTag("media:category");
				xmlChild.setXmlAttribute("scheme", "http://gdata.youtube.com/schemas/2007/categories.cat");
				xmlChild.addXmlObject(new XmlString(category));
				xmlMedia.addXmlObject(xmlChild);
			}
			if (keywords != null) {
				XmlTag xmlChild = new XmlTag("media:keywords");
				xmlChild.addXmlObject(new XmlString(keywords));
				xmlMedia.addXmlObject(xmlChild);
			}
			xmlRoot.addXmlObject(xmlMedia);

		}
		if (!canAccess) {
			XmlTag xmlChild = new XmlTag("yt:private");
			xmlRoot.addXmlObject(xmlChild);
		}
		if (!canAccess) {
			XmlTag xmlChild = new XmlTag("yt:noembed");
			xmlRoot.addXmlObject(xmlChild);
		}
		return xmlRoot.toString();
	}
	private YoutubeChannel youtubeChannel;
	private String videoId;
	private String title;
	private String description;
	private Date published;
	private Date lastModified;
	private String category;
	private String keywords;
	private int duration;
	private int viewCount;
	private boolean canAccess;
	private boolean canEmbed;
	private URL webURL;

	YoutubeChannel getYoutubeChannel() {
		return this.youtubeChannel;
	}

	public String getVideoId() {
		return this.videoId;
	}

	public String getTitle() {
		return this.title;
	}

	public String getDescription() {
		return this.description;
	}

	public Date getPublished() {
		return this.published;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public String getCategory() {
		return this.category;
	}

	public String getKeywords() {
		return this.keywords;
	}

	public int getDuration() {
		return this.duration;
	}

	public int getViewCount() {
		return this.viewCount;
	}

	public boolean isCanAccess() {
		return this.canAccess;
	}

	public boolean isCanEmbed() {
		return this.canEmbed;
	}

	public URL getWebURL() {
		return this.webURL;
	}

	private Date datetimeStringToDate(String datetimeString) {
		String datetime[] = datetimeString.split("\\.")[0].split("T");
		String date[] = datetime[0].split("-");
		String time[] = datetime[1].split(":");
		int year = Integer.parseInt(date[0]);
		int month = Integer.parseInt(date[1]);
		int day = Integer.parseInt(date[2]);
		int hour = Integer.parseInt(time[0]);
		int minute = Integer.parseInt(time[1]);
		int second = Integer.parseInt(time[2]);
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month, day, hour, minute, second);
		return calendar.getTime();
	}

	private void _initial(XmlTag xmlTag) {
		try {
			String id = xmlTag.getXmlTags("media:group")[0].getXmlTags("media:player")[0].getXmlAttribute("url");
			int start = id.indexOf("v=");
			int end = id.indexOf("&", start);
			this.videoId = id.substring(start + 2, end);
			this.webURL = new URL("https://www.youtube.com/watch?v=" + this.getVideoId());
		} catch (Exception ex) {
		}
		try {
			this.title = xmlTag.getXmlTags("title")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			this.description = xmlTag.getXmlTags("content")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			this.published = this.datetimeStringToDate(xmlTag.getXmlTags("published")[0].getInnerString());
		} catch (Exception ex) {
		}
		try {
			this.lastModified = this.datetimeStringToDate(xmlTag.getXmlTags("updated")[0].getInnerString());
		} catch (Exception ex) {
		}
		try {
			this.category = xmlTag.getXmlTags("media:group")[0].getXmlTags("media:category")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			this.keywords = xmlTag.getXmlTags("media:group")[0].getXmlTags("media:keywords")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			this.duration = Integer.parseInt(xmlTag.getXmlTags("media:group")[0].getXmlTags("yt:duration")[0].getXmlAttribute("seconds"));
		} catch (Exception ex) {
		}
		try {
			this.viewCount = Integer.parseInt(xmlTag.getXmlTags("yt:statistics")[0].getXmlAttribute("viewCount"));
		} catch (Exception ex) {
		}
		try {
			this.canAccess = xmlTag.getXmlTags("yt:private").length == 0;
		} catch (Exception ex) {
		}
		try {
			this.canEmbed = xmlTag.getXmlTags("yt:noembed").length == 0;
		} catch (Exception ex) {
		}
	}

	YoutubeVideo(YoutubeChannel youtubeChannel, XmlTag xmlTag) {
		this.youtubeChannel = youtubeChannel;
		this._initial(xmlTag);
	}

	public YoutubeVideo(String videoId) throws IOException {
		this(videoId, null);
	}

	public YoutubeVideo(String videoId, GoogleAuthorization googleAuthorization) throws IOException {
		URL url = new URL("https://gdata.youtube.com/feeds/api/videos/" + videoId);
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		if (googleAuthorization != null) {
			Map<String, String> requestHeaders = new LinkedHashMap<>();
			requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_WWW_FORM + "; charset=" + HttpRequest.CHARSET_UTF8);
			requestHeaders.put("Authorization", googleAuthorization.getToken());
			requestHeaders.put("X-GData-Client", googleAuthorization.getClientId());
			requestHeaders.put("X-GData-Key", String.format("key=\"%s\"", googleAuthorization.getApiKey()));
			httpRequest.setRequestHeaders(requestHeaders);
		}
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		String author = xmlTag.getXmlTags("author")[0].getXmlTags("name")[0].getInnerString();
		this.youtubeChannel = new YoutubeChannel(author, googleAuthorization);
		this._initial(xmlTag);
	}

	public void update(String title, String description, String category, String keywords, Boolean canAccess, Boolean canEmbed) throws IOException {
		GoogleAuthorization googleAuthorization = this.getYoutubeChannel().getGoogleAuthorization();
		URL url = new URL("https://gdata.youtube.com/feeds/api/users/" + this.getYoutubeChannel().getChannelId() + "/uploads/" + this.getVideoId());
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_PUT);
		if (googleAuthorization != null) {
			Map<String, String> requestHeaders = new LinkedHashMap<>();
			requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_ATOM_XML + "; charset=" + HttpRequest.CHARSET_UTF8);
			requestHeaders.put("Authorization", googleAuthorization.getToken());
			requestHeaders.put("X-GData-Client", googleAuthorization.getClientId());
			requestHeaders.put("X-GData-Key", String.format("key=\"%s\"", googleAuthorization.getApiKey()));
			httpRequest.setRequestHeaders(requestHeaders);
		}
		if (title != null) {
			this.title = title;
		}
		if (description != null) {
			this.description = description;
		}
		if (category != null) {
			this.category = category;
		}
		if (keywords != null) {
			this.keywords = keywords;
		}
		if (canAccess != null) {
			this.canAccess = canAccess;
		}
		if (canEmbed != null) {
			this.canEmbed = canEmbed;
		}
		String requestData = YoutubeVideo.createYoutubeVideoXML(this.getTitle(), this.getDescription(), this.getCategory(), this.getKeywords(), this.isCanAccess(), this.isCanEmbed());
		httpRequest.setRequestData(requestData);
		httpRequest.send();
	}

	public void updateTitle(String title) throws IOException {
		this.update(title, null, null, null, null, null);
	}

	public void updateDescription(String description) throws IOException {
		this.update(null, description, null, null, null, null);
	}

	public void updateCategory(String category) throws IOException {
		this.update(null, null, category, null, null, null);
	}

	public void updateKeywords(String keywords) throws IOException {
		this.update(null, null, null, keywords, null, null);
	}

	public void updateCanAccess(boolean canAccess) throws IOException {
		this.update(null, null, null, null, canAccess, null);
	}

	public void updateCanEmbed(boolean canEmbed) throws IOException {
		this.update(null, null, null, null, null, canEmbed);
	}

	public void delete() throws IOException {
		GoogleAuthorization googleAuthorization = this.getYoutubeChannel().getGoogleAuthorization();
		URL url = new URL("https://gdata.youtube.com/feeds/api/users/" + this.getYoutubeChannel().getChannelId()
			+ "/uploads/" + this.getVideoId());
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_DELETE);
		if (googleAuthorization != null) {
			Map<String, String> requestHeaders = new LinkedHashMap<>();
			requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_ATOM_XML + "; charset=" + HttpRequest.CHARSET_UTF8);
			requestHeaders.put("Authorization", googleAuthorization.getToken());
			requestHeaders.put("X-GData-Client", googleAuthorization.getClientId());
			requestHeaders.put("X-GData-Key", String.format("key=\"%s\"", googleAuthorization.getApiKey()));
			httpRequest.setRequestHeaders(requestHeaders);
		}
		httpRequest.send();
		this.youtubeChannel = null;
		this.videoId = null;
		this.title = null;
		this.description = null;
		this.published = null;
		this.lastModified = null;
		this.category = null;
		this.keywords = null;
		this.duration = 0;
		this.viewCount = 0;
		this.canAccess = false;
		this.canEmbed = false;
		this.webURL = null;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Video ID = ");
		sb.append(this.getVideoId());
		sb.append("\nTitle = ");
		sb.append(this.getTitle());
		sb.append("\nDescription = ");
		sb.append(this.getDescription());
		sb.append("\nPublished = ");
		sb.append(this.getPublished() == null ? "<null>" : Google.toHumanDateTimeFormat(this.getPublished()));
		sb.append("\nLast Modified = ");
		sb.append(this.getLastModified() == null ? "<null>" : Google.toHumanDateTimeFormat(this.getLastModified()));
		sb.append("\nCategory = ");
		sb.append(this.getCategory());
		sb.append("\nKeywords = ");
		sb.append(this.getKeywords());
		sb.append("\nDuration = ");
		sb.append(this.getDuration());
		sb.append("\nView Count = ");
		sb.append(this.getViewCount());
		sb.append("\nCan Access = ");
		sb.append(this.isCanAccess() ? "true" : "false");
		sb.append("\nCan Embed = ");
		sb.append(this.isCanEmbed() ? "true" : "false");
		sb.append("\nWeb URL = ");
		sb.append(this.getWebURL().toString());
		return sb.toString();
	}
}
