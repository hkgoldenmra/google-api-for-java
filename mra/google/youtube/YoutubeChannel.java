package mra.google.youtube;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import mra.http.HttpRequest;
import mra.google.GoogleAuthorization;
import mra.xml.XmlParser;
import mra.xml.XmlTag;

public class YoutubeChannel {

	private GoogleAuthorization googleAuthorization;
	private String channelId;
	private String author;
	private int subscriberCount;
	private int totalUploadViews;
	private URL webURL;

	GoogleAuthorization getGoogleAuthorization() {
		return this.googleAuthorization;
	}

	public String getChannelId() {
		return this.channelId;
	}

	public String getAuthor() {
		return this.author;
	}

	public int getSubscriberCount() {
		return this.subscriberCount;
	}

	public int getTotalUploadViews() {
		return this.totalUploadViews;
	}

	public URL getWebURL() {
		return this.webURL;
	}

	public YoutubeChannel(String channelId) throws IOException {
		this(channelId, null);
	}

	public YoutubeChannel(String channelId, GoogleAuthorization googleAuthorization) throws IOException {
		this.googleAuthorization = googleAuthorization;
		URL url = new URL("https://gdata.youtube.com/feeds/api/users/" + channelId);
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		if (this.getGoogleAuthorization() != null) {
			Map<String, String> requestHeaders = new LinkedHashMap<>();
			requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_WWW_FORM + "; charset=" + HttpRequest.CHARSET_UTF8);
			requestHeaders.put("Authorization", this.getGoogleAuthorization().getToken());
			requestHeaders.put("X-GData-Client", this.getGoogleAuthorization().getClientId());
			requestHeaders.put("X-GData-Key", String.format("key=\"%s\"", this.getGoogleAuthorization().getApiKey()));
			httpRequest.setRequestHeaders(requestHeaders);
		}
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		try {
			this.author = xmlTag.getXmlTags("author")[0].getXmlTags("name")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			this.subscriberCount = Integer.parseInt(xmlTag.getXmlTags("yt:statistics")[0].getXmlAttribute("subscriberCount"));
		} catch (Exception ex) {
		}
		try {
			this.totalUploadViews = Integer.parseInt(xmlTag.getXmlTags("yt:statistics")[0].getXmlAttribute("totalUploadViews"));
		} catch (Exception ex) {
		}
		try {
			XmlTag[] links = xmlTag.getXmlTags("link");
			for (XmlTag link : links) {
				String rel = link.getXmlAttribute("rel");
				String href = link.getXmlAttribute("href");
				if (rel.equals("alternate")) {
					int index = href.lastIndexOf('/');
					this.channelId = href.substring(index + 1);
					this.webURL = new URL("https://www.youtube.com/channel/" + this.getChannelId());
				}
			}
		} catch (Exception ex) {
		}
	}

	public YoutubeVideo uploadYoutubeVideo(File file, String title, String description, String category, String keywords, boolean canAccess, boolean canEmbed) throws IOException {
		URL url = new URL("https://uploads.gdata.youtube.com/feeds/api/users/" + this.getChannelId() + "/uploads");
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		if (this.getGoogleAuthorization() != null) {
			Map<String, String> requestHeaders = new LinkedHashMap<>();
			requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_MULTIPART_RELATED + "; charset=" + HttpRequest.CHARSET_UTF8);
			requestHeaders.put("Authorization", this.getGoogleAuthorization().getToken());
			requestHeaders.put("X-GData-Client", this.getGoogleAuthorization().getClientId());
			requestHeaders.put("X-GData-Key", String.format("key=\"%s\"", this.getGoogleAuthorization().getApiKey()));
			requestHeaders.put("Slug", file.getName());
			httpRequest.setRequestHeaders(requestHeaders);
		}
		{
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			this.writeString(baos, "Media multipart posting");
			this.writeString(baos, "\n--END_OF_PART");
			this.writeString(baos, "\nContent-Type: ");
			this.writeString(baos, HttpRequest.CONTENT_TYPE_ATOM_XML);
			this.writeString(baos, "\n");
			this.writeString(baos, "\n");
			this.writeString(baos, YoutubeVideo.createYoutubeVideoXML(title, description, category, keywords, canAccess, canEmbed));
			this.writeString(baos, "\n--END_OF_PART");
			this.writeString(baos, "\nContent-Type: ");
			this.writeString(baos, URLConnection.guessContentTypeFromName(file.getAbsolutePath()));
			this.writeString(baos, "\n");
			this.writeString(baos, "\n");
			InputStream is = new FileInputStream(file);
			byte buffer[] = new byte[1024 * 1024];
			for (int length; (length = is.read(buffer, 0, buffer.length)) > 0;) {
				baos.write(buffer, 0, length);
			}
			is.close();
			this.writeString(baos, "\n--END_OF_PART--");
			baos.flush();
			baos.close();
			httpRequest.setRequestData(baos.toByteArray());
		}
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		return new YoutubeVideo(this, xmlTag);
	}

	private void writeString(OutputStream os, String string) throws IOException {
		os.write(string.getBytes(HttpRequest.CHARSET_UTF8));
	}

	public YoutubeVideo getYoutubeVideo(String videoId) throws IOException {
		URL url = new URL("https://gdata.youtube.com/feeds/api/videos/" + videoId);
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		if (this.getGoogleAuthorization() != null) {
			Map<String, String> requestHeaders = new LinkedHashMap<>();
			requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_WWW_FORM + "; charset=" + HttpRequest.CHARSET_UTF8);
			requestHeaders.put("Authorization", this.getGoogleAuthorization().getToken());
			requestHeaders.put("X-GData-Client", this.getGoogleAuthorization().getClientId());
			requestHeaders.put("X-GData-Key", String.format("key=\"%s\"", this.getGoogleAuthorization().getApiKey()));
			httpRequest.setRequestHeaders(requestHeaders);
		}
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		return new YoutubeVideo(this, xmlTag);
	}

	public YoutubeVideo[] listYoutubeVideos() throws IOException {
		return this.listYoutubeVideos(YoutubeVideoComparator.SORT_BY_PUBLISHED, YoutubeVideoComparator.ORDER_BY_DESCENDING);
	}

	public YoutubeVideo[] listYoutubeVideos(char sortBy, char orderBy) throws IOException {
		ArrayList<YoutubeVideo> youtubeVideos = new ArrayList<>();
		int startIndex = 1;
		int maxResults = 50;
		while (true) {
			URL url = new URL("https://gdata.youtube.com/feeds/api/users/" + this.getChannelId()
				+ "/uploads?start-index=" + startIndex
				+ "&max-results=" + maxResults);
			HttpRequest httpRequest = new HttpRequest(url);
			httpRequest.setMethod(HttpRequest.METHOD_GET);
			if (this.getGoogleAuthorization() != null) {
				Map<String, String> requestHeaders = new LinkedHashMap<>();
				requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_WWW_FORM + "; charset=" + HttpRequest.CHARSET_UTF8);
				requestHeaders.put("Authorization", this.getGoogleAuthorization().getToken());
				requestHeaders.put("X-GData-Client", this.getGoogleAuthorization().getClientId());
				requestHeaders.put("X-GData-Key", String.format("key=\"%s\"", this.getGoogleAuthorization().getApiKey()));
				httpRequest.setRequestHeaders(requestHeaders);
			}
			httpRequest.send();
			String responseString = httpRequest.getResponseString();
			XmlTag xmlTag = XmlParser.parse(responseString);
			XmlTag[] entries = xmlTag.getXmlTags("entry");
			if (entries.length > 0) {
				for (XmlTag entry : entries) {
					youtubeVideos.add(new YoutubeVideo(this, entry));
				}
				startIndex += maxResults;
			} else {
				break;
			}
		}
		YoutubeVideo[] yvs = youtubeVideos.toArray(new YoutubeVideo[youtubeVideos.size()]);
		Arrays.sort(yvs, new YoutubeVideoComparator(sortBy, orderBy));
		return yvs;
	}

	public YoutubePlaylist createYoutubePlaylist(String title, String description, boolean canAccess) throws IOException {
		URL url = new URL("https://gdata.youtube.com/feeds/api/users/" + this.getChannelId() + "/playlists");
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		if (this.getGoogleAuthorization() != null) {
			Map<String, String> requestHeaders = new LinkedHashMap<>();
			requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_ATOM_XML + "; charset=" + HttpRequest.CHARSET_UTF8);
			requestHeaders.put("Authorization", this.getGoogleAuthorization().getToken());
			requestHeaders.put("X-GData-Client", this.getGoogleAuthorization().getClientId());
			requestHeaders.put("X-GData-Key", String.format("key=\"%s\"", this.getGoogleAuthorization().getApiKey()));
			httpRequest.setRequestHeaders(requestHeaders);
		}
		String data = YoutubePlaylist.createYoutubePlaylistXML(title, description, canAccess, null);
		httpRequest.setRequestData(data);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		return new YoutubePlaylist(this, xmlTag);
	}

	public YoutubePlaylist getYoutubePlaylist(String playlistId) throws IOException {
		URL url = new URL("https://gdata.youtube.com/feeds/api/playlists/" + playlistId + "?max-results=0");
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		if (this.getGoogleAuthorization() != null) {
			Map<String, String> requestHeaders = new LinkedHashMap<>();
			requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_WWW_FORM + "; charset=" + HttpRequest.CHARSET_UTF8);
			requestHeaders.put("Authorization", this.getGoogleAuthorization().getToken());
			requestHeaders.put("X-GData-Client", this.getGoogleAuthorization().getClientId());
			requestHeaders.put("X-GData-Key", String.format("key=\"%s\"", this.getGoogleAuthorization().getApiKey()));
			httpRequest.setRequestHeaders(requestHeaders);
		}
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		return new YoutubePlaylist(this, xmlTag);
	}

	public YoutubePlaylist[] listYoutubePlaylists() throws IOException {
		return this.listYoutubePlaylists(YoutubePlaylistComparator.SORT_BY_TITLE, YoutubePlaylistComparator.ORDER_BY_ASCENDING);
	}

	public YoutubePlaylist[] listYoutubePlaylists(char sortBy, char orderBy) throws IOException {
		ArrayList<YoutubePlaylist> youtubePlaylists = new ArrayList<>();
		int startIndex = 1;
		int maxResults = 50;
		while (true) {
			URL url = new URL("https://gdata.youtube.com/feeds/api/users/" + this.getChannelId()
				+ "/playlists?start-index=" + startIndex
				+ "&max-results=" + maxResults);
			HttpRequest httpRequest = new HttpRequest(url);
			httpRequest.setMethod(HttpRequest.METHOD_GET);
			if (this.getGoogleAuthorization() != null) {
				Map<String, String> requestHeaders = new LinkedHashMap<>();
				requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_WWW_FORM + "; charset=" + HttpRequest.CHARSET_UTF8);
				requestHeaders.put("Authorization", this.getGoogleAuthorization().getToken());
				requestHeaders.put("X-GData-Client", this.getGoogleAuthorization().getClientId());
				requestHeaders.put("X-GData-Key", String.format("key=\"%s\"", this.getGoogleAuthorization().getApiKey()));
				httpRequest.setRequestHeaders(requestHeaders);
			}
			httpRequest.send();
			String responseString = httpRequest.getResponseString();
			XmlTag xmlTag = XmlParser.parse(responseString);
			XmlTag[] entries = xmlTag.getXmlTags("entry");
			if (entries.length > 0) {
				for (XmlTag entry : entries) {
					youtubePlaylists.add(new YoutubePlaylist(this, entry));
				}
				startIndex += maxResults;
			} else {
				break;
			}
		}
		YoutubePlaylist[] yps = youtubePlaylists.toArray(new YoutubePlaylist[youtubePlaylists.size()]);
		Arrays.sort(yps, new YoutubePlaylistComparator(sortBy, orderBy));
		return yps;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Channel ID = ");
		sb.append(this.getChannelId());
		sb.append("\nAuthor = ");
		sb.append(this.getAuthor());
		sb.append("\nSubsciber Count = ");
		sb.append(this.getSubscriberCount());
		sb.append("\nTotal Upload Views = ");
		sb.append(this.getTotalUploadViews());
		sb.append("\nWeb URL = ");
		sb.append(this.getWebURL() == null ? "<null>" : this.getWebURL().toString());
		return sb.toString();
	}
}
