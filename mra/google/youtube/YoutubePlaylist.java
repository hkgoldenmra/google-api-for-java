package mra.google.youtube;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import mra.http.HttpRequest;
import mra.google.GoogleAuthorization;
import mra.xml.XmlParser;
import mra.xml.XmlString;
import mra.xml.XmlTag;

public class YoutubePlaylist {

	static String createYoutubePlaylistXML(String title, String description, boolean canAccess, String playlistId) {
		XmlTag xmlRoot = new XmlTag("entry");
		xmlRoot.setXmlAttribute("xmlns", "http://www.w3.org/2005/Atom");
		xmlRoot.setXmlAttribute("xmlns:yt", "http://gdata.youtube.com/schemas/2007");
		if (playlistId != null) {
			XmlTag xmlChild = new XmlTag("id");
			xmlChild.addXmlObject(new XmlString(playlistId));
			xmlRoot.addXmlObject(xmlChild);
		}
		{
			XmlTag xmlChild = new XmlTag("title");
			xmlChild.setXmlAttribute("type", "text");
			xmlChild.addXmlObject(new XmlString(title));
			xmlRoot.addXmlObject(xmlChild);
		}
		{
			XmlTag xmlChild = new XmlTag("summary");
			xmlChild.addXmlObject(new XmlString(description));
			xmlRoot.addXmlObject(xmlChild);
		}
		if (!canAccess) {
			XmlTag xmlChild = new XmlTag("yt:private");
			xmlRoot.addXmlObject(xmlChild);
		}
		return xmlRoot.toString();
	}
	private YoutubeChannel youtubeChannel;
	private String playlistId;
	private String title;
	private String description;
	private boolean canAccess;
	private URL webURL;

	YoutubeChannel getYoutubeChannel() {
		return this.youtubeChannel;
	}

	public String getPlaylistId() {
		return this.playlistId;
	}

	public String getTitle() {
		return this.title;
	}

	public String getDescription() {
		return this.description;
	}

	public boolean isCanAccess() {
		return this.canAccess;
	}

	public URL getWebURL() {
		return this.webURL;
	}

	private void _initial(XmlTag xmlTag) {
		try {
			this.playlistId = xmlTag.getXmlTags("yt:playlistId")[0].getInnerString();
			this.webURL = new URL("https://www.youtube.com/playlist?list=" + this.getPlaylistId());
		} catch (Exception ex) {
		}
		try {
			this.title = xmlTag.getXmlTags("title")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			this.description = xmlTag.getXmlTags("subtitle")[0].getInnerString();
		} catch (Exception ex1) {
			try {
				this.description = xmlTag.getXmlTags("content")[0].getInnerString();
			} catch (Exception ex2) {
			}
		}
		try {
			this.canAccess = xmlTag.getXmlTags("yt:private").length == 0;
		} catch (Exception ex) {
		}
	}

	YoutubePlaylist(YoutubeChannel youtubeChannel, XmlTag xmlTag) {
		this.youtubeChannel = youtubeChannel;
		this._initial(xmlTag);
	}

	public YoutubePlaylist(String playlistId) throws IOException {
		this(playlistId, null);
	}

	public YoutubePlaylist(String playlistId, GoogleAuthorization googleAuthorization) throws IOException {
		URL url = new URL("https://gdata.youtube.com/feeds/api/playlists/" + playlistId + "?max-results=0");
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		if (googleAuthorization != null) {
			Map<String, String> requestHeaders = new LinkedHashMap<>();
			requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_WWW_FORM + "; charset=" + HttpRequest.CHARSET_UTF8);
			requestHeaders.put("Authorization", googleAuthorization.getToken());
			requestHeaders.put("X-GData-Client", googleAuthorization.getClientId());
			requestHeaders.put("X-GData-Key", String.format("key=\"%s\"", googleAuthorization.getApiKey()));
			httpRequest.setRequestHeaders(requestHeaders);
		}
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		String author = xmlTag.getXmlTags("author")[0].getXmlTags("name")[0].getInnerString();
		this.youtubeChannel = new YoutubeChannel(author, googleAuthorization);
		this._initial(xmlTag);
	}

	public void update(String title, String description, Boolean canAccess) throws IOException {
		GoogleAuthorization googleAuthorization = this.getYoutubeChannel().getGoogleAuthorization();
		URL url = new URL("https://gdata.youtube.com/feeds/api/playlists/" + this.getPlaylistId());
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_PUT);
		if (googleAuthorization != null) {
			Map<String, String> requestHeaders = new LinkedHashMap<>();
			requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_ATOM_XML + "; charset=" + HttpRequest.CHARSET_UTF8);
			requestHeaders.put("Authorization", googleAuthorization.getToken());
			requestHeaders.put("X-GData-Client", googleAuthorization.getClientId());
			requestHeaders.put("X-GData-Key", String.format("key=\"%s\"", googleAuthorization.getApiKey()));
			httpRequest.setRequestHeaders(requestHeaders);
		}
		if (title != null) {
			this.title = title;
		}
		if (description != null) {
			this.description = description;
		}
		if (canAccess != null) {
			this.canAccess = canAccess;
		}
		String requestData = YoutubePlaylist.createYoutubePlaylistXML(this.getTitle(), this.getDescription(), this.isCanAccess(), this.getPlaylistId());
		httpRequest.setRequestData(requestData);
		httpRequest.send();
	}

	public void updateTitle(String title) throws IOException {
		this.update(title, null, null);
	}

	public void updateDescription(String description) throws IOException {
		this.update(null, description, null);
	}

	public void updateAccessible(Boolean accessible) throws IOException {
		this.update(null, null, accessible);
	}

	public void delete() throws IOException {
		GoogleAuthorization googleAuthorization = this.getYoutubeChannel().getGoogleAuthorization();
		URL url = new URL("https://gdata.youtube.com/feeds/api/users/" + this.getYoutubeChannel().getChannelId() + "/playlists/" + this.getPlaylistId());
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_DELETE);
		if (googleAuthorization != null) {
			Map<String, String> requestHeaders = new LinkedHashMap<>();
			requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_ATOM_XML + "; charset=" + HttpRequest.CHARSET_UTF8);
			requestHeaders.put("Authorization", googleAuthorization.getToken());
			requestHeaders.put("X-GData-Client", googleAuthorization.getClientId());
			requestHeaders.put("X-GData-Key", String.format("key=\"%s\"", googleAuthorization.getApiKey()));
			httpRequest.setRequestHeaders(requestHeaders);
		}
		httpRequest.send();
		this.youtubeChannel = null;
		this.playlistId = null;
		this.title = null;
		this.description = null;
		this.canAccess = false;
		this.webURL = null;
	}

	public void addYoutubeVideo(String videoId, Integer position) throws IOException {
		GoogleAuthorization googleAuthorization = this.getYoutubeChannel().getGoogleAuthorization();
		URL url = new URL("https://gdata.youtube.com/feeds/api/playlists/" + this.getPlaylistId());
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		if (googleAuthorization != null) {
			Map<String, String> requestHeaders = new LinkedHashMap<>();
			requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_ATOM_XML + "; charset=" + HttpRequest.CHARSET_UTF8);
			requestHeaders.put("Authorization", googleAuthorization.getToken());
			requestHeaders.put("X-GData-Client", googleAuthorization.getClientId());
			requestHeaders.put("X-GData-Key", String.format("key=\"%s\"", googleAuthorization.getApiKey()));
			httpRequest.setRequestHeaders(requestHeaders);
		}
		String requestData = YoutubeEntry.createYoutubeEntryXML(videoId, position);
		httpRequest.setRequestData(requestData);
		httpRequest.send();
	}

	public void addYoutubeVideo(String videoId) throws IOException {
		this.addYoutubeVideo(videoId, null);
	}

	public YoutubeEntry[] listYoutubeEntries() throws IOException {
		return this.listYoutubeEntries(YoutubeEntryComparator.SORT_BY_POSITION, YoutubeEntryComparator.ORDER_BY_ASCENDING);
	}

	public YoutubeEntry[] listYoutubeEntries(char sortBy, char orderBy) throws IOException {
		GoogleAuthorization googleAuthorization = this.getYoutubeChannel().getGoogleAuthorization();
		URL url = new URL("https://gdata.youtube.com/feeds/api/playlists/" + this.getPlaylistId());
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		if (googleAuthorization != null) {
			Map<String, String> requestHeaders = new LinkedHashMap<>();
			requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_WWW_FORM + "; charset=" + HttpRequest.CHARSET_UTF8);
			requestHeaders.put("Authorization", googleAuthorization.getToken());
			requestHeaders.put("X-GData-Client", googleAuthorization.getClientId());
			requestHeaders.put("X-GData-Key", String.format("key=\"%s\"", googleAuthorization.getApiKey()));
			httpRequest.setRequestHeaders(requestHeaders);
		}
		httpRequest.send();
		XmlTag xmlTag = XmlParser.parse(httpRequest.getResponseString());
		ArrayList<YoutubeEntry> youtubeEntries = new ArrayList<>();
		XmlTag[] entries = xmlTag.getXmlTags("entry");
		for (XmlTag entry : entries) {
			youtubeEntries.add(new YoutubeEntry(this, entry));
		}
		YoutubeEntry[] yes = youtubeEntries.toArray(new YoutubeEntry[youtubeEntries.size()]);
		Arrays.sort(yes, new YoutubeEntryComparator(sortBy, orderBy));
		return yes;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Playlist ID = ");
		sb.append(this.getPlaylistId());
		sb.append("\nTitle = ");
		sb.append(this.getTitle());
		sb.append("\nDescription = ");
		sb.append(this.getDescription());
		sb.append("\nCan Access = ");
		sb.append(this.isCanAccess() ? "true" : "false");
		sb.append("\nWeb URL = ");
		sb.append(this.getWebURL() == null ? "<null>" : this.getWebURL().toString());
		return sb.toString();
	}
}
