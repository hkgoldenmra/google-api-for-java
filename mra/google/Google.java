package mra.google;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Google {

	static final Pattern fullDateTimePattern = Pattern.compile("^([0-9]{4})-([0-9]{2})-([0-9]{2})(.([0-9]{2}):([0-9]{2}):([0-9]{2})(.([0-9]{2}):([0-9]{2}))?)?");
	static final SimpleDateFormat humanDateTimeFormat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");

	public static Date datetimeStringToDate(String dateTimeString) {
		int year = 1970;
		int month = 0;
		int day = 1;
		int hour = 0;
		int minute = 0;
		int second = 0;
		int utcHour = 0;
		int utcMinute = 0;
		Matcher matcher = Google.fullDateTimePattern.matcher(dateTimeString);
		if (matcher.matches()) {
			int count = matcher.groupCount();
			switch (count) {
				case 3:
				case 7:
				case 10: {
					year = Integer.parseInt(matcher.group(1));
					month = Integer.parseInt(matcher.group(2)) - 1;
					day = Integer.parseInt(matcher.group(3));
				}
			}
			switch (count) {
				case 7:
				case 10: {
					hour = Integer.parseInt(matcher.group(5));
					minute = Integer.parseInt(matcher.group(6));
					second = Integer.parseInt(matcher.group(7));
				}
			}
			if (count == 10) {
				utcHour = Integer.parseInt(matcher.group(9));
				utcMinute = Integer.parseInt(matcher.group(10));
			}
		}
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month, day, hour, minute, second);
		return calendar.getTime();
	}

	public static String toHumanDateTimeFormat(Date date) {
		return date == null ? "<null>" : Google.humanDateTimeFormat.format(date);
	}

	private Google() {
	}
}
