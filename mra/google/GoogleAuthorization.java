package mra.google;

import java.io.Serializable;

public abstract class GoogleAuthorization implements Serializable {

	private final String clientId;
	private final String clientSecert;
	private final String apiKey;

	public String getClientId() {
		return this.clientId;
	}

	String getClientSecert() {
		return this.clientSecert;
	}

	public String getApiKey() {
		return this.apiKey;
	}

	public abstract String getToken();

	public GoogleAuthorization(String clientId, String clientSecert, String apiKey) {
		this.clientId = clientId;
		this.clientSecert = clientSecert;
		this.apiKey = apiKey;
	}
}
