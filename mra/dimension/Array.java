package mra.dimension;

public class Array<T> {

	private T[] data;

	public Array(T[] data) {
		this.data = data;
	}

	public Array(int size) {
		this((T[]) new Object[size]);
	}

	public Array() {
		this(0);
	}

	public int getSize() {
		return this.data.length;
	}

	public void set(int index, T data) {
		if (index >= this.getSize()) {
			this.resize(index + 1, true);
		}
		this.data[index] = data;
	}

	public T get(int index) {
		if (index >= this.getSize()) {
			return null;
		} else {
			return this.data[index];
		}
	}

	public void resize(int size, boolean keepData) {
		if (size < 0) {
			size = this.getSize();
		}
		T[] data = (T[]) new Object[size];
		if (keepData) {
			for (int index = 0; index < size; index++) {
				data[index] = this.get(index);
			}
		}
		this.data = data;
	}

	public void copy(int index1, int index2) {
		int size = Math.max(index1, index2) + 1;
		if (size > this.getSize()) {
			this.resize(size, true);
		}
		this.set(index2, this.get(index1));
	}

	public void copy(int index1, int size, int index2) {
		for (int index = index1 + size - 1, i = size - 1; index >= index1; index--, i--) {
			this.copy(index, index2 + i);
		}
	}

	public void move(int index1, int index2) {
		int size = Math.max(index1, index2) + 1;
		if (size > this.getSize()) {
			this.resize(size, true);
		}
		this.copy(index1, index2);
		this.set(index1, null);
	}

	public void move(int index1, int size, int index2) {
		for (int index = index1 + size - 1, i = size - 1; index >= index1; index--, i--) {
			this.move(index, index2 + i);
		}
	}

	public void swap(int index1, int index2) {
		int size = Math.max(index1, index2) + 1;
		if (size > this.getSize()) {
			this.resize(size, true);
		}
		T data = this.get(index2);
		this.copy(index1, index2);
		this.set(index1, data);
	}

	public void swap(int index1, int size, int index2) {
		for (int index = index1 + size - 1, i = size - 1; index >= index1; index--, i--) {
			this.swap(index, index2 + i);
		}
	}

	public void flip(int index, int size) {
		for (int i = index; i < size / 2; i++) {
			this.swap(i, 1, size - i - 1);
		}
	}

	public void flip() {
		this.flip(0, this.getSize());
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		int size = this.getSize();
		for (int index = 0; index < size; index++) {
			if (index > 0) {
				sb.append(",");
			}
			sb.append(this.get(index));
		}
		return sb.toString();
	}
}
