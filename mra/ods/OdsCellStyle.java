package mra.ods;

import java.awt.Color;
import java.awt.Font;

public class OdsCellStyle {

	public static final int HALIGN_LEFT = 0;
	public static final int HALIGN_CENTER = 1;
	public static final int HALIGN_RIGHT = 2;
	public static final int VALIGN_TOP = 0;
	public static final int VALIGN_MIDDLE = 1;
	public static final int VALIGN_BOTTOM = 2;
	private int hAlign;
	private int vAlign;
	private Color fColor;
	private Color bColor;
	private Font font;
	private String fontUnit;
	private OdsBorderStyle borderTop;
	private OdsBorderStyle borderLeft;
	private OdsBorderStyle borderRight;
	private OdsBorderStyle borderBottom;

	public OdsCellStyle(int hAlign, int vAlign, Color fColor, Color bColor, Font font, String fontUnit, OdsBorderStyle borderTop, OdsBorderStyle borderLeft, OdsBorderStyle borderRight, OdsBorderStyle borderBottom) {
		this.hAlign = hAlign;
		this.vAlign = vAlign;
		this.fColor = fColor;
		this.bColor = bColor;
		this.font = font;
		this.fontUnit = fontUnit;
		this.borderTop = borderTop;
		this.borderLeft = borderLeft;
		this.borderRight = borderRight;
		this.borderBottom = borderBottom;
	}

	public int getHAlign() {
		return this.hAlign;
	}

	public int getVAlign() {
		return this.vAlign;
	}

	public Color getFColor() {
		return this.fColor;
	}

	public Color getBColor() {
		return this.bColor;
	}

	public Font getFont() {
		return this.font;
	}

	public String getFontUnit() {
		return this.fontUnit;
	}

	public OdsBorderStyle getBorderTop() {
		return this.borderTop;
	}

	public OdsBorderStyle getBorderLeft() {
		return this.borderLeft;
	}

	public OdsBorderStyle getBorderRight() {
		return this.borderRight;
	}

	public OdsBorderStyle getBorderBottom() {
		return this.borderBottom;
	}
}
