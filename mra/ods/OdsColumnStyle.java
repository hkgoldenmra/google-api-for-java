package mra.ods;

public class OdsColumnStyle {

	private double width;
	private String unit;

	public OdsColumnStyle(double width) {
		this(width, "");
	}

	public OdsColumnStyle(double width, String unit) {
		this.width = width;
		if (unit == null) {
			unit = "";
		}
		this.unit = unit;
	}

	public double getWidth() {
		return this.width;
	}

	public String getUnit() {
		return this.unit;
	}
}
