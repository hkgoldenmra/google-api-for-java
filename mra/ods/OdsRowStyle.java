package mra.ods;

public class OdsRowStyle {

	private double height;
	private String unit;

	public OdsRowStyle(double height) {
		this(height, "");
	}

	public OdsRowStyle(double height, String unit) {
		this.height = height;
		if (unit == null) {
			unit = "";
		}
		this.unit = unit;
	}

	public double getHeight() {
		return this.height;
	}

	public String getUnit() {
		return this.unit;
	}
}
