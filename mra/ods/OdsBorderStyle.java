package mra.ods;

import java.awt.Color;

public class OdsBorderStyle {

	private double thick;
	private String unit;
	private int style;
	private Color color;

	public OdsBorderStyle(double thick, String unit, int style, Color color) {
		this.thick = thick;
		this.unit = unit;
		this.style = style;
		this.color = color;
	}

	public Color getColor() {
		return this.color;
	}

	public int getStyle() {
		return this.style;
	}

	public String getUnit() {
		return this.unit;
	}

	public double getThick() {
		return this.thick;
	}
}
