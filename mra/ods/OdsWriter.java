package mra.ods;

import java.awt.Font;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import mra.xml.XmlString;
import mra.xml.XmlTag;

public class OdsWriter {

	private int rowCount = 0;
	private int columnCount = 0;
	private String[][] contentss = new String[0][0];
	private OdsRowStyle[] rowStyles = new OdsRowStyle[0];
	private OdsColumnStyle[] columnStyles = new OdsColumnStyle[0];
	private OdsCellStyle[][] cellStyless = new OdsCellStyle[0][0];

	public OdsWriter() {
	}

	public int getRowCount() {
		return this.rowCount;
	}

	public int getColumnCount() {
		return this.columnCount;
	}

	private void resetArrays(int rowIndex, int columnIndex) {
		if (rowIndex >= this.getRowCount() || columnIndex >= this.getColumnCount()) {
			if (rowIndex >= this.getRowCount()) {
				this.rowCount = Math.max(rowIndex + 1, this.getRowCount());
				this.rowStyles = this.extend1DArray(this.rowStyles, this.getRowCount());
			}
			if (columnIndex >= this.getColumnCount()) {
				this.columnCount = Math.max(columnIndex + 1, this.getColumnCount());
				this.columnStyles = this.extend1DArray(this.columnStyles, this.getColumnCount());
			}
			this.contentss = this.extend2DArray(this.contentss, this.getRowCount(), this.getColumnCount());
			this.cellStyless = this.extend2DArray(this.cellStyless, this.getRowCount(), this.getColumnCount());
		}
	}

	private OdsRowStyle[] extend1DArray(OdsRowStyle[] array, int length) {
		OdsRowStyle[] tArray = new OdsRowStyle[length];
		System.arraycopy(array, 0, tArray, 0, array.length);
		return tArray;
	}

	private OdsColumnStyle[] extend1DArray(OdsColumnStyle[] array, int length) {
		OdsColumnStyle[] tArray = new OdsColumnStyle[length];
		System.arraycopy(array, 0, tArray, 0, array.length);
		return tArray;
	}

	private String[][] extend2DArray(String[][] array, int row, int column) {
		String[][] tArray = new String[row][column];
		for (int i = 0; i < array.length; i++) {
			System.arraycopy(array[i], 0, tArray[i], 0, array[i].length);
		}
		return tArray;
	}

	private OdsCellStyle[][] extend2DArray(OdsCellStyle[][] array, int row, int column) {
		OdsCellStyle[][] tArray = new OdsCellStyle[row][column];
		for (int i = 0; i < array.length; i++) {
			System.arraycopy(array[i], 0, tArray[i], 0, array[i].length);
		}
		return tArray;
	}

	public void setContent(int row, int column, String data) {
		this.resetArrays(row, column);
		this.contentss[row][column] = data;
	}

	public void setRowStyle(int rowIndex, OdsRowStyle style) {
		this.resetArrays(rowIndex, -2);
		this.rowStyles[rowIndex] = style;
	}

	public void setColumnStyle(int columnIndex, OdsColumnStyle style) {
		this.resetArrays(-2, columnIndex);
		this.columnStyles[columnIndex] = style;
	}

	public void setCellStyle(int rowIndex, int columnIndex, OdsCellStyle style) {
		this.resetArrays(rowIndex, columnIndex);
		this.cellStyless[rowIndex][columnIndex] = style;
	}

	public void write(File file) throws IOException {
		ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(file));
		this.writeManifest(zos);
		this.writeContent(zos);
		zos.flush();
		zos.close();
	}

	private void writeManifest(ZipOutputStream zos) throws IOException {
		XmlTag xmlManifestManifest = new XmlTag("manifest:manifest");
		xmlManifestManifest.setXmlAttribute("xmlns:manifest", "urn:oasis:names:tc:opendocument:xmlns:manifest:1.0");
		{
			XmlTag xmlManifestFileEntry = new XmlTag("manifest:file-entry");
			xmlManifestFileEntry.setXmlAttribute("manifest:media-type", "application/vnd.oasis.opendocument.spreadsheet");
			xmlManifestFileEntry.setXmlAttribute("manifest:full-path", "/");
			xmlManifestManifest.addXmlObject(xmlManifestFileEntry);
		}
		{
			XmlTag xmlManifestFileEntry = new XmlTag("manifest:file-entry");
			xmlManifestFileEntry.setXmlAttribute("manifest:media-type", "text/xml");
			xmlManifestFileEntry.setXmlAttribute("manifest:full-path", "content.xml");
			xmlManifestManifest.addXmlObject(xmlManifestFileEntry);
		}
		zos.putNextEntry(new ZipEntry("META-INF/manifest.xml"));
		zos.write(xmlManifestManifest.toString(true).getBytes("UTF-8"));
		zos.closeEntry();
	}

	private void writeContent(ZipOutputStream zos) throws IOException {
		XmlTag xmlOfficeDocumentContent = new XmlTag("office:document-content");
		xmlOfficeDocumentContent.setXmlAttribute("xmlns:office", "urn:oasis:names:tc:opendocument:xmlns:office:1.0");
		xmlOfficeDocumentContent.setXmlAttribute("xmlns:style", "urn:oasis:names:tc:opendocument:xmlns:style:1.0");
		xmlOfficeDocumentContent.setXmlAttribute("xmlns:text", "urn:oasis:names:tc:opendocument:xmlns:text:1.0");
		xmlOfficeDocumentContent.setXmlAttribute("xmlns:table", "urn:oasis:names:tc:opendocument:xmlns:table:1.0");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:draw", "urn:oasis:names:tc:opendocument:xmlns:drawing:1.0");
		xmlOfficeDocumentContent.setXmlAttribute("xmlns:fo", "urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:dc", "http://purl.org/dc/elements/1.1");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:meta", "urn:oasis:names:tc:opendocument:xmlns:meta:1.0");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:number", "urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:presentation", "urn:oasis:names:tc:opendocument:xmlns:presentation:1.0");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:svg", "urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:chart", "urn:oasis:names:tc:opendocument:xmlns:chart:1.0");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:dr3d", "urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:math", "http://www.w3.org/1998/Math/MathML");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:form", "urn:oasis:names:tc:opendocument:xmlns:form:1.0");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:script", "urn:oasis:names:tc:opendocument:xmlns:script:1.0");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:ooo", "http://openoffice.org/2004/office");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:ooow", "http://openoffice.org/2004/writer");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:oooc", "http://openoffice.org/2004/calc");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:dom", "http://www.w3.org/2001/xml-events");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:xforms", "http://www.w3.org/2002/xforms");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:rpt", "http://openoffice.org/2005/report");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:of", "urn:oasis:names:tc:opendocument:xmlns:of:1.2");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:xhtml", "http://www.w3.org/1999/xhtml");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:grddl", "http://www.w3.org/2003/g/data-view#");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:tableooo", "http://openoffice.org/2009/table");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:drawooo", "http://openoffice.org/2010/draw");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:calcext", "urn:org:documentfoundation:names:experimental:calc:xmlns:calcext:1.0");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:field", "urn:openoffice:names:experimental:ooo-ms-interop:xmlns:field:1.0");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:formx", "urn:openoffice:names:experimental:ooxml-odf-interop:xmlns:form:1.0");
		//xmlOfficeDocumentContent.setXmlAttribute("xmlns:css3t", "http://www.w3.org/TR/css3-text");
		xmlOfficeDocumentContent.setXmlAttribute("office:version", "1.2");
		{
			XmlTag xmlOfficeAutomaticStyles = new XmlTag("office:automatic-styles");
			{
				for (int i = 0; i < this.getRowCount(); i++) {
					OdsRowStyle rowStyle = this.rowStyles[i];
					if (rowStyle != null) {
						XmlTag xmlStyleStyle = new XmlTag("style:style");
						xmlStyleStyle.setXmlAttribute("style:family", "table-row");
						xmlStyleStyle.setXmlAttribute("style:name", "ro-" + i);
						{
							XmlTag xmlStyleTableRowProperties = new XmlTag("style:table-row-properties");
							xmlStyleTableRowProperties.setXmlAttribute("fo:break-before", "auto");
							xmlStyleTableRowProperties.setXmlAttribute("style:use-optimal-row-height", "false");
							xmlStyleTableRowProperties.setXmlAttribute("style:row-height", rowStyle.getHeight() + rowStyle.getUnit());
							xmlStyleStyle.addXmlObject(xmlStyleTableRowProperties);
						}
						xmlOfficeAutomaticStyles.addXmlObject(xmlStyleStyle);
					}
				}
				for (int j = 0; j < this.getColumnCount(); j++) {
					OdsColumnStyle columnStyle = this.columnStyles[j];
					if (columnStyle != null) {
						XmlTag xmlStyleStyle = new XmlTag("style:style");
						xmlStyleStyle.setXmlAttribute("style:family", "table-column");
						xmlStyleStyle.setXmlAttribute("style:name", "co-" + j);
						{
							XmlTag xmlStyleTableColumnProperties = new XmlTag("style:table-column-properties");
							xmlStyleTableColumnProperties.setXmlAttribute("fo:break-before", "auto");
							xmlStyleTableColumnProperties.setXmlAttribute("style:use-optimal-column-width", "false");
							xmlStyleTableColumnProperties.setXmlAttribute("style:column-width", columnStyle.getWidth() + columnStyle.getUnit());
							xmlStyleStyle.addXmlObject(xmlStyleTableColumnProperties);
						}
						xmlOfficeAutomaticStyles.addXmlObject(xmlStyleStyle);
					}
				}
				for (int i = 0; i < this.getRowCount(); i++) {
					for (int j = 0; j < this.getColumnCount(); j++) {
						OdsCellStyle cellStyle = this.cellStyless[i][j];
						if (cellStyle != null) {
							XmlTag xmlStyleStyle = new XmlTag("style:style");
							xmlStyleStyle.setXmlAttribute("style:family", "table-cell");
							xmlStyleStyle.setXmlAttribute("style:name", String.format("ce-%d-%d", i, j));
							{
								XmlTag xmlStyleTableCellProperties = new XmlTag("style:table-cell-properties");
								xmlStyleTableCellProperties.setXmlAttribute("style:text-align-source", "fix");
								xmlStyleTableCellProperties.setXmlAttribute("style:repeat-content", "false");
								xmlStyleTableCellProperties.setXmlAttribute("style:vertical-align", cellStyle.getVAlign() == 1 ? "middle" : cellStyle.getVAlign() == 2 ? "bottom" : "top");
								if (cellStyle.getBColor() != null) {
									xmlStyleTableCellProperties.setXmlAttribute("fo:background-color", String.format("#%06x", cellStyle.getBColor().getRGB() & 0xffffff));
								}
								{
									OdsBorderStyle borderStyle = cellStyle.getBorderTop();
									if (borderStyle != null) {
										xmlStyleTableCellProperties.setXmlAttribute("fo:border-top", String.format("%s%s %s #%06x", borderStyle.getThick(), borderStyle.getUnit(), borderStyle.getStyle() == 1 ? "dashed" : borderStyle.getStyle() == 2 ? "dotted" : "solid", borderStyle.getColor().getRGB() & 0xffffff));
									}
								}
								{
									OdsBorderStyle borderStyle = cellStyle.getBorderLeft();
									if (borderStyle != null) {
										xmlStyleTableCellProperties.setXmlAttribute("fo:border-left", String.format("%s%s %s #%06x", borderStyle.getThick(), borderStyle.getUnit(), borderStyle.getStyle() == 1 ? "dashed" : borderStyle.getStyle() == 2 ? "dotted" : "solid", borderStyle.getColor().getRGB() & 0xffffff));
									}
								}
								{
									OdsBorderStyle borderStyle = cellStyle.getBorderRight();
									if (borderStyle != null) {
										xmlStyleTableCellProperties.setXmlAttribute("fo:border-right", String.format("%s%s %s #%06x", borderStyle.getThick(), borderStyle.getUnit(), borderStyle.getStyle() == 1 ? "dashed" : borderStyle.getStyle() == 2 ? "dotted" : "solid", borderStyle.getColor().getRGB() & 0xffffff));
									}
								}
								{
									OdsBorderStyle borderStyle = cellStyle.getBorderBottom();
									if (borderStyle != null) {
										xmlStyleTableCellProperties.setXmlAttribute("fo:border-bottom", String.format("%s%s %s #%06x", borderStyle.getThick(), borderStyle.getUnit(), borderStyle.getStyle() == 1 ? "dashed" : borderStyle.getStyle() == 2 ? "dotted" : "solid", borderStyle.getColor().getRGB() & 0xffffff));
									}
								}
								{
									XmlTag xmlStyleParagraphProperties = new XmlTag("style:paragraph-properties");
									xmlStyleParagraphProperties.setXmlAttribute("fo:text-align", cellStyle.getVAlign() == 1 ? "center" : cellStyle.getVAlign() == 2 ? "right" : "left");
									xmlStyleStyle.addXmlObject(xmlStyleParagraphProperties);
								}
								xmlStyleStyle.addXmlObject(xmlStyleTableCellProperties);
							}
							{
								XmlTag xmlStyleTextProperties = new XmlTag("style:text-properties");
								StringBuilder sbr = new StringBuilder();
								Font font = cellStyle.getFont();
								if (font != null) {
									xmlStyleTextProperties.setXmlAttribute("style:font-name", cellStyle.getFont().getFontName());
									if (font.isBold()) {
										xmlStyleTextProperties.setXmlAttribute("fo:font-weight", "bold");
										xmlStyleTextProperties.setXmlAttribute("style:font-weight-asian", "bold");
										xmlStyleTextProperties.setXmlAttribute("style:font-weight-complex", "bold");
									}
									if (font.isItalic()) {
										xmlStyleTextProperties.setXmlAttribute("fo:font-weight", "italic");
										xmlStyleTextProperties.setXmlAttribute("style:font-weight-asian", "italic");
										xmlStyleTextProperties.setXmlAttribute("style:font-weight-complex", "italic");
									}
									sbr.append(font.getSize());
								}
								if (cellStyle.getFontUnit() != null) {
									sbr.append(cellStyle.getFontUnit());
								}
								xmlStyleTextProperties.setXmlAttribute("fo:font-size", sbr.toString());
								xmlStyleTextProperties.setXmlAttribute("style:font-size-asian", sbr.toString());
								xmlStyleTextProperties.setXmlAttribute("style:font-size-complex", sbr.toString());
								if (cellStyle.getFColor() != null) {
									xmlStyleTextProperties.setXmlAttribute("fo:color", String.format("#%06x", cellStyle.getFColor().getRGB() & 0xffffff));
								}
							}
							xmlOfficeAutomaticStyles.addXmlObject(xmlStyleStyle);
						}
					}
				}
			}
			xmlOfficeDocumentContent.addXmlObject(xmlOfficeAutomaticStyles);
		}
		{
			XmlTag xmlOfficeBody = new XmlTag("office:body");
			{
				XmlTag xmlOfficeSpreadsheet = new XmlTag("office:spreadsheet");
				{
					XmlTag xmlTableTable = new XmlTag("table:table");
					xmlTableTable.setXmlAttribute("table:name", "sheet-name");
					{
						for (int j = 0; j < this.getColumnCount(); j++) {
							XmlTag xmlTableTableColumn = new XmlTag("table:table-column");
							if (this.columnStyles[j] != null) {
								xmlTableTableColumn.setXmlAttribute("table:style-name", "co-" + j);
							}
							xmlTableTable.addXmlObject(xmlTableTableColumn);
						}
						for (int i = 0; i < this.getRowCount(); i++) {
							XmlTag xmlTableTableRow = new XmlTag("table:table-row");
							if (this.rowStyles[i] != null) {
								xmlTableTableRow.setXmlAttribute("table:style-name", "ro-" + i);
							}
							for (int j = 0; j < this.getColumnCount(); j++) {
								XmlTag xmlTableTableCell = new XmlTag("table:table-cell");
								if (this.cellStyless[i][j] != null) {
									xmlTableTableCell.setXmlAttribute("table:style-name", String.format("ce-%d-%d", i, j));
								}
								xmlTableTableCell.setXmlAttribute("office:value-type", "string");
								xmlTableTableCell.setXmlAttribute("table:number-rows-spanned", "1");
								xmlTableTableCell.setXmlAttribute("table:number-columns-spanned", "1");
								if (this.contentss[i][j] != null) {
									XmlTag xmlTextP = new XmlTag("text:p");
									xmlTextP.addXmlObject(new XmlString(this.contentss[i][j]));
									xmlTableTableCell.addXmlObject(xmlTextP);
								}
								xmlTableTableRow.addXmlObject(xmlTableTableCell);
							}
							xmlTableTable.addXmlObject(xmlTableTableRow);
						}
					}
					xmlOfficeSpreadsheet.addXmlObject(xmlTableTable);
				}
				xmlOfficeBody.addXmlObject(xmlOfficeSpreadsheet);
			}
			xmlOfficeDocumentContent.addXmlObject(xmlOfficeBody);
		}
		zos.putNextEntry(new ZipEntry("content.xml"));
		zos.write(xmlOfficeDocumentContent.toString(true).getBytes("UTF-8"));
		zos.closeEntry();
	}
}
