package mra.table;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedHashMap;
import java.util.Map;

public class HtmlTable extends MultiStyledTable {

	private static final Map<Integer, String> HORIZONTAL_ALIGNMENTS = new LinkedHashMap<>();
	private static final Map<Integer, String> VERTICAL_ALIGNMENTS = new LinkedHashMap<>();
	private static final Map<Integer, String> BORDER_STYLES = new LinkedHashMap<>();

	static {
		HtmlTable.HORIZONTAL_ALIGNMENTS.put(0, "left");
		HtmlTable.HORIZONTAL_ALIGNMENTS.put(1, "center");
		HtmlTable.HORIZONTAL_ALIGNMENTS.put(2, "right");
		HtmlTable.VERTICAL_ALIGNMENTS.put(0, "top");
		HtmlTable.VERTICAL_ALIGNMENTS.put(1, "middle");
		HtmlTable.VERTICAL_ALIGNMENTS.put(2, "bottom");
		HtmlTable.BORDER_STYLES.put(0, "solid");
		HtmlTable.BORDER_STYLES.put(1, "dashed");
		HtmlTable.BORDER_STYLES.put(2, "dotted");
	}

	@Override
	public void export(File file) throws IOException {
		FileOutputStream fos = new FileOutputStream(file);
		this.writeString(fos, "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n");
		this.writeString(fos, "<html>\n");
		this.writeString(fos, "\t<head>\n");
		this.writeString(fos, "\t\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\n");
		this.writeString(fos, "\t\t<style type=\"text/css\">\n");
		this.writeString(fos, "table { border-collapse: collapse; } \n");
		for (Integer id : this.tableRowStyles.keySet()) {
			TableRowStyle tableRowStyle = this.tableRowStyles.get(id);
			if (tableRowStyle != null) {
				this.writeString(fos, String.format(".ro-%08x { height: %.2f", tableRowStyle.hashCode(), tableRowStyle.getHeight()));
				String unit = tableRowStyle.getUnit();
				if (unit != null) {
					this.writeString(fos, unit);
				}
				this.writeString(fos, "; }\n");
			}
		}
		for (Integer id : this.tableColumnStyles.keySet()) {
			TableColumnStyle tableColumnStyle = this.tableColumnStyles.get(id);
			if (tableColumnStyle != null) {
				this.writeString(fos, String.format(".co-%08x { width: %.2f", tableColumnStyle.hashCode(), tableColumnStyle.getWidth()));
				String unit = tableColumnStyle.getUnit();
				if (unit != null) {
					this.writeString(fos, unit);
				}
				this.writeString(fos, "; }\n");
			}
		}
		for (Integer id : this.tableCellStyles.keySet()) {
			TableCellStyle tableCellStyle = this.tableCellStyles.get(id);
			if (tableCellStyle != null) {
				this.writeString(fos, String.format(".ce-%08x { ", tableCellStyle.hashCode()));
				int horizontalAlignment = tableCellStyle.getHorizontalAlignment();
				int verticalAlignment = tableCellStyle.getVerticalAlignment();
				Color textColor = tableCellStyle.getTextColor();
				Color backgroundColor = tableCellStyle.getBackgroundColor();
				Font font = tableCellStyle.getFont();
				Map<String, TableBorderStyle> tableBorderStyles = new LinkedHashMap<>();
				tableBorderStyles.put("top", tableCellStyle.getBorderTop());
				tableBorderStyles.put("left", tableCellStyle.getBorderLeft());
				tableBorderStyles.put("right", tableCellStyle.getBorderRight());
				tableBorderStyles.put("bottom", tableCellStyle.getBorderBottom());
				switch (horizontalAlignment) {
					case TableCellStyle.HORIZONTAL_ALIGNMENT_CENTER:
					case TableCellStyle.HORIZONTAL_ALIGNMENT_RIGHT: {
					}
					break;
					default: {
						horizontalAlignment = TableCellStyle.HORIZONTAL_ALIGNMENT_LEFT;
					}
				}
				switch (verticalAlignment) {
					case TableCellStyle.VERTICAL_ALIGNMENT_MIDDLE:
					case TableCellStyle.VERTICAL_ALIGNMENT_BOTTOM: {
					}
					break;
					default: {
						verticalAlignment = TableCellStyle.VERTICAL_ALIGNMENT_TOP;
					}
				}
				this.writeString(fos, String.format("text-align: %s; ", HtmlTable.HORIZONTAL_ALIGNMENTS.get(horizontalAlignment)));
				this.writeString(fos, String.format("vertical-align: %s; ", HtmlTable.VERTICAL_ALIGNMENTS.get(verticalAlignment)));
				if (textColor != null) {
					this.writeString(fos, String.format("color: #%06x; ", textColor.getRGB() & 0xffffff));
				}
				if (backgroundColor != null) {
					this.writeString(fos, String.format("background-color: #%06x; ", backgroundColor.getRGB() & 0xffffff));
				}
				if (font != null) {
					String unit = font.getUnit();
					if (unit != null) {
						unit = "";
					}
					this.writeString(fos, String.format("font-family: '%s'; ", font.getName()));
					this.writeString(fos, String.format("font-size: %.2f%s; ", font.getSize(), unit));
				}
				for (String position : tableBorderStyles.keySet()) {
					TableBorderStyle tableBorderStyle = tableBorderStyles.get(position);
					if (tableBorderStyle != null) {
						String unit = tableBorderStyle.getUnit();
						if (unit != null) {
							unit = "";
						}
						int style = tableBorderStyle.getStyle();
						switch (style) {
							case TableBorderStyle.SOLID:
							case TableBorderStyle.DASHED: {
							}
							break;
							default: {
								style = TableBorderStyle.DOTTED;
							}
						}
						this.writeString(fos, String.format("border-%s-width: %.2f%s;", position, tableBorderStyle.getThick(), unit));
						this.writeString(fos, String.format("border-%s-style: %s; ", position, HtmlTable.BORDER_STYLES.get(style)));
						Color color = tableBorderStyle.getColor();
						if (color != null) {
							this.writeString(fos, String.format("border-%s-color: #%06x; ", position, color.getRGB() & 0xffffff));
						}
					}
				}
				this.writeString(fos, "}\n");
			}
		}
		this.writeString(fos, "\t\t</style>\n");
		this.writeString(fos, "\t</head>\n");
		this.writeString(fos, "\t<body>\n");
		for (String name : this.tableData.keySet()) {
			int rows = this.getRowCount(name);
			int columns = this.getColumnCount(name);
			if (rows > 0 && columns > 0) {
				this.writeString(fos, "\t\t<table>\n");
				this.writeString(fos, String.format("\t\t\t<caption>%s</caption>\n", name));
				for (int column = 0; column < columns; column++) {
					this.writeString(fos, "\t\t\t<col");
					Integer id = this.tableColumnStyleIds.get(name).get(column);
					if (id != null) {
						this.writeString(fos, String.format(" class=\"co-%08x\"", id));
					}
					this.writeString(fos, "/>\n");
				}
				for (int row = 0; row < rows; row++) {
					boolean rowSkip = true;
					for (int column = 0; column < columns; column++) {
						Boolean skip = this.tableCellSkip.get(name).get(row, column);
						rowSkip = !(skip == null || skip == false);
					}
					if (!rowSkip) {
						this.writeString(fos, "\t\t\t<tr");
						Integer id = this.tableRowStyleIds.get(name).get(row);
						if (id != null) {
							this.writeString(fos, String.format(" class=\"ro-%08x\"", id));
						}
						this.writeString(fos, ">\n");
						for (int column = 0; column < columns; column++) {
							Boolean skip = this.tableCellSkip.get(name).get(row, column);
							if (skip == null || skip == false) {
								this.writeString(fos, "\t\t\t\t<td");
								id = this.tableCellStyleIds.get(name).get(row, column);
								TableCellStyle tableCellStyle = this.tableCellStyles.get(id);
								if (id != null) {
									this.writeString(fos, String.format(" class=\"ce-%08x\"", id));
								}
								if (tableCellStyle != null) {
									int rowspan = this.tableCellStyles.get(id).getRowspan();
									int colspan = this.tableCellStyles.get(id).getColspan();
									if (rowspan > 1) {
										this.writeString(fos, String.format(" rowspan=\"%d\"", rowspan));
									}
									if (colspan > 1) {
										this.writeString(fos, String.format(" colspan=\"%d\"", colspan));
									}
								}
								this.writeString(fos, ">");
								String data = this.tableData.get(name).get(row, column);
								if (data != null) {
									this.writeString(fos, data);
								}
								this.writeString(fos, "</td>\n");
							}
						}
						this.writeString(fos, "\t\t\t</tr>\n");
					}
				}
				this.writeString(fos, "\t\t</table>\n");
			}
		}
		this.writeString(fos, "\t</body>\n");
		this.writeString(fos, "</html>");
		fos.flush();
		fos.close();
	}

	private void writeString(OutputStream os, String string) throws IOException {
		os.write(string.getBytes("UTF-8"));
	}
}
