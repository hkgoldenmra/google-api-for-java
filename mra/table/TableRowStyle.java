package mra.table;

public class TableRowStyle {

	private final float height;
	private final String unit;

	public TableRowStyle(float height, String unit) {
		this.height = height;
		this.unit = unit;
	}

	public float getHeight() {
		return this.height;
	}

	public String getUnit() {
		return this.unit;
	}
}
