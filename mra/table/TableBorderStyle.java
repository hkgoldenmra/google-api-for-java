package mra.table;

import java.awt.Color;

public class TableBorderStyle {

	public static final int SOLID = 0;
	public static final int DASHED = 1;
	public static final int DOTTED = 2;
	private final float thick;
	private final String unit;
	private final int style;
	private final Color color;

	public TableBorderStyle(float thick, String unit, int style, Color color) {
		this.thick = thick;
		this.unit = unit;
		this.style = style;
		this.color = color;
	}

	public float getThick() {
		return this.thick;
	}

	public String getUnit() {
		return this.unit;
	}

	public int getStyle() {
		return this.style;
	}

	public Color getColor() {
		return this.color;
	}
}
