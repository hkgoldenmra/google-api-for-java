package mra.ini;

public class IniException extends RuntimeException {

	public IniException() {
	}

	public IniException(String message) {
		super(message);
	}

	public IniException(String message, Throwable cause) {
		super(message, cause);
	}

	public IniException(Throwable cause) {
		super(cause);
	}

	public IniException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
