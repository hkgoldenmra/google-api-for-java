package mra.image;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

public class Flip implements Processor {

	public static final int HORIZONTAL = 1;
	public static final int VERTICAL = 2;
	private final int type;

	public Flip(int type) {
		this.type = type;
	}

	@Override
	public BufferedImage process(BufferedImage image) {
		int width = image.getWidth();
		int height = image.getHeight();
		BufferedImage bimage = new BufferedImage(width, height, image.getType());
		Graphics2D g2d = bimage.createGraphics();
		switch (this.type) {
			case Flip.VERTICAL: {
				g2d.drawImage(image, 0, height, width, 0, 0, 0, width, height, null);
			}
			break;
			default: {
				g2d.drawImage(image, width, 0, 0, height, 0, 0, width, height, null);
			}
		}
		g2d.dispose();
		return bimage;
	}
}
