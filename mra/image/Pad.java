package mra.image;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

public class Pad implements Processor {

	private final int top;
	private final int left;
	private final int right;
	private final int bottom;
	private final Color color;

	public Pad(int top, int left, int right, int bottom, Color color) {
		this.top = top;
		this.left = left;
		this.right = right;
		this.bottom = bottom;
		this.color = color;
	}

	@Override
	public BufferedImage process(BufferedImage image) {
		int width = image.getWidth();
		int height = image.getHeight();
		BufferedImage bimage = new BufferedImage(width + this.left + this.right, height + this.top + this.bottom, image.getType());
		Graphics2D g2d = bimage.createGraphics();
		Color color = g2d.getColor();
		g2d.setColor(this.color);
		g2d.fillRect(0, 0, width + this.left + this.right, height + this.top + this.bottom);
		g2d.setColor(color);
		g2d.drawImage(image, this.left, this.top, this.left + width, this.top + height, 0, 0, width, height, null);
		g2d.dispose();
		return bimage;
	}
}
