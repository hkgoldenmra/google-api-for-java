package mra.image;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

public class Resizer implements Processor {

	private final Number width;
	private final Number height;

	public Resizer(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public Resizer(int size) {
		this(size, size);
	}

	public Resizer(double width, double height) {
		this.width = width;
		this.height = height;
	}

	public Resizer(double size) {
		this(size, size);
	}

	public Resizer(int width, double height) {
		this.width = width;
		this.height = height;
	}

	public Resizer(double width, int height) {
		this.width = width;
		this.height = height;
	}

	@Override
	public BufferedImage process(BufferedImage image) {
		int width = this.width.intValue();
		if (this.width instanceof Double) {
			width = new Double(image.getWidth() * this.width.doubleValue()).intValue();
		}
		int height = this.height.intValue();
		if (this.height instanceof Double) {
			height = new Double(image.getHeight() * this.height.doubleValue()).intValue();
		}
		BufferedImage bimage = new BufferedImage(width, height, image.getType());
		Graphics2D g2d = bimage.createGraphics();
		g2d.drawImage(image, 0, 0, width, height, null);
		g2d.dispose();
		return bimage;
	}
}
