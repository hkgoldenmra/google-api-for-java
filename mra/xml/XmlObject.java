package mra.xml;

import mra.json.JsonValue;

public abstract class XmlObject {

	@Override
	public String toString() {
		return this.toString(false);
	}

	public String toString(boolean formatted) {
		return this.toString(formatted, 0, true);
	}

	abstract String toString(boolean formatted, int tags, boolean isFirst);

	public abstract JsonValue toJsonValue();
}
