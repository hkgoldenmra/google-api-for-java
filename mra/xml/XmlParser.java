package mra.xml;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XmlParser {

	public static final char[] xmlSpecialChars = {'&', '"', '\'', '<', '>'};
	public static final String[] xmlEscapedStrings = {"amp", "quot", "apos", "lt", "gt"};

	public static String toEscapedXml(String string) {
		for (int i = 0; i < xmlSpecialChars.length; i++) {
			String find = Character.toString(XmlParser.xmlSpecialChars[i]);
			String replace = "&" + XmlParser.xmlEscapedStrings[i] + ";";
			string = string.replaceAll(find, replace);
		}
		return string;
	}

	public static XmlTag parse(File xmlFile) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(xmlFile));
		StringBuilder sb = new StringBuilder();
		for (String line; (line = br.readLine()) != null;) {
			sb.append("\n");
			sb.append(line);
		}
		sb.delete(0, 1);
		return XmlParser.parse(sb.toString());
	}

	public static XmlTag parse(String xml) throws XmlException {
		try {
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			db.setErrorHandler(null);
			return XmlParser.extract(db.parse(new ByteArrayInputStream(xml.getBytes("UTF-8"))).getDocumentElement());
		} catch (IOException | ParserConfigurationException | SAXException ex) {
			throw new XmlException(ex.toString().substring(ex.getClass().getName().length() + 2));
		}
	}

	private static XmlTag extract(Element element) {
		XmlTag xmlTag = new XmlTag(element.getTagName());
		NamedNodeMap attributes = element.getAttributes();
		int aLength = attributes.getLength();
		for (int i = 0; i < aLength; i++) {
			Node attribute = attributes.item(i);
			xmlTag.setXmlAttribute(attribute.getNodeName(), attribute.getNodeValue());
		}
		NodeList nodes = element.getChildNodes();
		int nLength = nodes.getLength();
		for (int i = 0; i < nLength; i++) {
			Node node = nodes.item(i);
			short nodeType = node.getNodeType();
			switch (nodeType) {
				case Node.ELEMENT_NODE: {
					xmlTag.addXmlObject(XmlParser.extract((Element) node));
				}
				break;
				case Node.CDATA_SECTION_NODE:
				case Node.TEXT_NODE: {
					String string = node.getTextContent().trim();
					if (string.length() > 0) {
						xmlTag.addXmlObject(new XmlString(string));
					}
				}
				break;
			}
		}
		return xmlTag;
	}
}
